> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

#  Perishable Import

## Project description

The Perishable Import component is a SE service of a Silicon Economy Platform.
The documentation contains the description of all Perishable Import  Components, their context, interaction,
constraints and implementation.

The Perishable Import App digitizes the import processes for time sensitive goods (perishables) and
facilitates the data exchange and processing of analog documents. All necessary information is collected,
stored, processed and distributed in a uniform and, if possible, automated manner. All stakeholders involved
in the process have access. The IATA Standard One Record is taken into account.

The aim is to accelerate the information flow and to prevent mistakes that are caused by analog documents.


## Local Couchdb Setup

To start a local couchdb instance to develop with use the docker compose file `docker-compose.dev.yml`

Start it with

> docker-compose -f docker-compose.dev.yml up

or

> docker-compose -f docker-compose.dev.yml up -d

if you want to run it in detached mode.

Attention: if you want do restart the couchdb docker-container and want to fill it with fresh data
make sure to remove the old containers by using

> docker-compose -f docker-compose.dev.yml down

Otherwise the old container(s) could be restarted and you get a conflict message when fill / upload
the inital data.


## One Record

There are two possibilities to use an One Record server

### Local Setup

To start a local One Record server change to the directory docker/one-record and do there a

> docker-compose up --build

This will download the sources from the One Record git repository, build the server and start it
along with a local mongodb instance.

If the server was started you can verify this by visiting http://localhost:8080/swagger-ui.html


### Deployment to k8s

Normally the deployment will happen automatically if something has changed in the helm config or the
docker setup. Though this will only happen on the default branch (master).

You can force a build or deploy by manually run a git pipeline with the following variable(s) set:

> BUILD_ONE_RECORD to "true" for forced building

> DEPLOY_ONE_RECORD to "true" for forced deployment

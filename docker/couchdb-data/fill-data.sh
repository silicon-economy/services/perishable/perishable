#!/bin/sh

# remove comment if you want to debug this script
#set -x



DB_URL=${COUCHDB_CLIENT_URL:-http://localhost:5984}

USER_ADMIN=${COUCHDB_CLIENT_USERNAME}:${COUCHDB_CLIENT_PASSWORD}
USER_IMPORT=import:Zl5Wqq606sg

COUNT=60


echo "environment settings:----------------"
env
echo "-------------------------------------"


echo "Connecting to couchdb at ${DB_URL}"

for i in $(seq 1 ${COUNT}) ; do
    echo "waiting for couchdb to come up... "
    if curl -s ${DB_URL} > /dev/null ; then
        echo "couchdb is up!"
        break
    fi
    sleep 2
done

if [ "$i" = "${COUNT}" ] ; then
    echo "ERROR: could not connect to to couchdb instance at ${DB_URL}. exiting!"
    exit 1
fi


db_exists() {
    test "$(curl -X GET -u ${USER_ADMIN} -o /dev/null -I -L -s -w "%{http_code}" ${DB_URL}/$1)" = 200
}

#
# creates a database
# $1 database's name
# $4 credentials in format user:password
#
create_db() {
    if db_exists $1 ; then
        echo "database '$1' exists; we don't touch it!"
    else
        echo -n "creating database '$1' ..."
        ret="$(curl -X PUT -u ${USER_ADMIN} -o /dev/null -I -L -s -w "%{http_code}" ${DB_URL}/$1)"
        case $ret in
            201)
                echo "ok!"
                ;;
            *)
                echo "failed! exiting with code $ret"
                exit 1
                ;;
        esac
    fi
}


#
# deletes a database
# $1 database's name
# $4 credentials in format user:password
#
delete_db() {
    if ! db_exists $1 ; then
        echo "database '$1' does not exist. No need to delete"
    else
        echo -n "deleting database '$1' ..."
        ret="$(curl -X DELETE -u ${USER_ADMIN} -o /dev/null -I -L -s -w "%{http_code}" ${DB_URL}/$1)"
        case $ret in
            200)
                echo "ok!"
                ;;
            *)
                echo "failed! exiting with code $ret"
                exit 1
                ;;
        esac
    fi
}


# grants acess to database
# $1 database's name
# $2 roles
# $3 credentials in format user:password
grant_access_to_role() {
    # '{"admins":{"names":["superuser"],"roles":["admins"]},"members":{"names": ["user1","user2"],"roles": ["developers"]}}
    data="{\"admins\":{\"names\":[],\"roles\":[\"$2\"]} }"
    echo ----------------------------------------
    echo "creating document $2 in db $1..."
    curl -X PUT -u $3 -s -H "Content-type: application/json" -d "$data"  ${DB_URL}/$1/_security
    echo ----------------------------------------
}

#
# creates a document
# $1 database's name
# $2 document id
# $3 document data as JSON string
# $4 credentials in format user:password
#
create_doc() {
    echo ----------------------------------------
    echo "creating document $2 in db $1..."
    curl -X POST -u $4 -s -H "Content-type: application/json" -d "$3"  ${DB_URL}/$1
    echo ----------------------------------------
}


#
# fetch the revision of a document
#
# $1 database's name
# $2 document id
# $3 credentials in format user:password
get_doc_rev() {
    curl -s --head  ${DB_URL}/$1/$2 | sed -n '/^ETag/p' | cut -c 8-41
}

#
# creates a document
# $1 database's name
# $2 file name with JSON documents
# $3 credentials in format user:password
#
bulk_upload() {
    echo ----------------------------------------
    echo -n "bulk document upload to db $1..."
    curl -X POST -u $3 -s -H "Content-type: application/json" -d "@$2"  ${DB_URL}/$1/_bulk_docs

}


#
# uploads an attachment to a document
# $1 database's name
# $2 document id
# $3 filename of attachment
# $4 content type (MIME)
# $5 credentials in format user:password
upload_attachment() {
    echo ----------------------------------------
    rev=$(get_doc_rev $1 $2 $5)
    echo -n "upload attachment '$2' for document $1 to db $1"
    curl -X PUT -u $5 -s -H "Content-type: $4" --data-binary "@$3"  "${DB_URL}/$1/$2/$3?rev=${rev}"
}

#
# creates default users
#
create_users() {

    doc=$(
cat <<EOF
    {
      "_id":"org.couchdb.user:api",
      "name":"api",
      "roles":["perishable"],
      "type":"user",
      "password":"Fi0BTiKdocc"
     }
EOF
)

    create_doc _users "org.couchdb.user:api" "$doc" ${USER_ADMIN}

    doc=$(
cat <<EOF
  {
    "_id":"org.couchdb.user:import",
    "name":"import",
    "roles":["perishable"],
    "type":"user",
    "password":"Zl5Wqq606sg"
  }
EOF
)

   create_doc _users "org.couchdb.user:import" "$doc" ${USER_ADMIN}

}




if ! db_exists _users ; then
    create_db _users
    create_users
fi

create_db _replicator
create_db _global_changes

delete_db perishable
create_db perishable

grant_access_to_role perishable perishable ${USER_ADMIN}

ddoc=$(cat <<EOF
{
  "_id": "_design/all",
  "views": {
    "data": {
      "map": "function(doc){emit(null);}",
      "reduce": "_count"
    }
  },
  "language": "javascript"
}
EOF
)

create_doc perishable "_design/all" "$ddoc" ${USER_IMPORT}

ddoc=$(cat <<EOF
{
  "_id": "_design/byType",
  "views": {
    "shipment": {
      "map": "function(doc){if(doc.type == \"shipment\"){emit(null);}}",
      "reduce": "_count"
    },
    "piece": {
      "map": "function(doc){if(doc.type == \"piece\"){emit(null);}}",
      "reduce": "_count"
    },
    "pieceDocument": {
      "map": "function(doc){if(doc.type == \"pieceDocument\"){emit(null);}}",
      "reduce": "_count"
    }
  },
  "language": "javascript"
}
EOF
)

create_doc perishable "_design/byType" "$ddoc" ${USER_IMPORT}

bulk_upload perishable or-pieces.json ${USER_IMPORT}
bulk_upload perishable or-shipments.json ${USER_IMPORT}
bulk_upload perishable or-piece-documents.json ${USER_IMPORT}

upload_attachment perishable "563-73609538_2CD373FB" "Phytosanitary.pdf" "application/pdf" ${USER_IMPORT}
upload_attachment perishable "563-73609538_2CD373FB" "Invoice.pdf" "application/pdf" ${USER_IMPORT}
upload_attachment perishable "563-73609538_2CD373FB" "MAWB.pdf" "application/pdf" ${USER_IMPORT}

upload_attachment perishable "563-73609536_4B9D8F6C" "Phytosanitary.pdf" "application/pdf" ${USER_IMPORT}
upload_attachment perishable "563-73609536_4B9D8F6C" "Invoice.pdf" "application/pdf" ${USER_IMPORT}

upload_attachment perishable "563-73609534_F0324EC4" "Phytosanitary.pdf" "application/pdf" ${USER_IMPORT}
upload_attachment perishable "563-73609534_F0324EC4" "Invoice.pdf" "application/pdf" ${USER_IMPORT}




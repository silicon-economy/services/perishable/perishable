#!/bin/sh

# for setting the one-record server in a cluster environment


set -x

java "-Dspring.data.mongodb.uri=mongodb://${PERISHABLE_USER}:${PERISHABLE_PASSWORD}@mongodb-headless/perishable?retryWrites=true&w=majority"  -jar ./one-record.jar


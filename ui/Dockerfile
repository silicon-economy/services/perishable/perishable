FROM node:12-alpine as builder

COPY package.json package-lock.json ./
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY . .
RUN npm run ng build -- --prod --output-path=dist


FROM nginxinc/nginx-unprivileged
USER root
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/dist /usr/share/nginx/html
# Set appropriate access right to environment so that this can be set by an unprivileged user
RUN touch /usr/share/nginx/html/assets/env.js && chown nginx:nginx /usr/share/nginx/html/assets/env.js && chmod go+rw /usr/share/nginx/html/assets/env.js

USER nginx
EXPOSE 8080:8080
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]

/**
 * Loads environment variables.
 * Needs to be done in assetfolder since environment.ts is barely accessable after build.
 * Attention: New environment variables also need to be added to assets/env.template.js
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

 (function(window){
  window["env"]=window["env"]||{}
  window['env']['production'] = false;
  window["env"]["API_HOST"] = "http://localhost";
  window["env"]["API_PORT"] = "8080";
}(this));

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

// This loads environment variables.
// Following this tutorial: https://pumpingco.de/blog/environment-variables-angular-docker/
// To add an evironment variable you also have to add it to 'env.template.js'.

export const environment = {
  // Check if fields are defined before reading them, they are not in e.g. headless chrome
  production: window["env"] && window["env"]["production"] ? window["env"]["production"] : false,
  // API_HOST has to include the protocol since we can not rely on servers redirecting us to https when hardcoding http
  apiHost: window["env"] && window["env"]["API_HOST"] ? window["env"]["API_HOST"] : 'http://localhost',
  apiPort: window["env"] && window["env"]["API_PORT"] ? window["env"]["API_PORT"] : '8080',
};

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { MatDrawerMode } from "@angular/material/sidenav";
import { ThemeService } from "./services/state/theme.service";
import { SidenavService } from "./services/state/sidenav.service";

/**
 * Provides the base layout of the application.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  breakpointStateLtMd = false;
  themeModeIcon: string;
  footerState: boolean;
  sidenavMode: MatDrawerMode = 'side';
  sidenavState = true;

  constructor(
    public breakpointObserver: BreakpointObserver,
    public themeService: ThemeService,
    public sidenavService: SidenavService)
  {
    breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.XSmall
    ]).subscribe(result => {
      if (result.matches) {
        this.activateHandsetLayout();
        this.breakpointStateLtMd = true;
      } else {
        this.deactivateHandsetLayout();
        this.breakpointStateLtMd = false;
      }
    });
  }

  ngOnInit(): void {
    // load application state from local storage
    // mode
    document.body.classList.add(`blue-${ this.themeService.getModeStr() }`);
    this.themeModeIcon = this.themeService.getNextModeIcon();

    // sidenav
    if (!this.breakpointStateLtMd) {
      this.sidenavState = this.sidenavService.getSidenavState();
    }
  }

  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.sidenavService.getSidenavState();
  }

  /**
   * Toggles between open and closed sidebar.
   */
  sidenavStateToggle() {
    if (!this.breakpointStateLtMd){
      this.sidenavService.toggleSidenavState();
    }
  }

  /**
   * Toggles between dark and light mode of the Material blue theme.
   */
  toggleThemeMode(): void {
    const nextClassname = `blue-${ this.themeService.getNextModeObj().name }`;
    const currentClassname = `blue-${  this.themeService.getModeObj().name }`;

    if (nextClassname !== currentClassname) {
      document.body.classList.add(nextClassname);
      document.body.classList.remove(currentClassname);

      this.themeService.toggleMode();
      this.themeModeIcon = this.themeService.getNextModeIcon();
    }
  }
}

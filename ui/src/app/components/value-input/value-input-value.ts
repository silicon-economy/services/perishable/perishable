// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Value for the {@link ValueInputComponent}.
 *
 * @author Marc Dickmann
 */
export class ValueInputValue {
  constructor(public amount: number, public unit: string) {}
}

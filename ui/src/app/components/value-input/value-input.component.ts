// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { FocusMonitor } from '@angular/cdk/a11y';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, ElementRef, Inject, Input, OnDestroy, Optional, Self, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormGroup, NgControl, Validators } from '@angular/forms';
import { MatFormField, MatFormFieldControl, MAT_FORM_FIELD } from '@angular/material/form-field';
import { Subject } from 'rxjs';
import { ValueInputValue } from 'src/app/components/value-input/value-input-value';

/**
 * Material input group for {@link Value}s.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-value-input',
  templateUrl: './value-input.component.html',
  styleUrls: ['./value-input.component.scss'],
  providers: [
    {provide: MatFormFieldControl, useExisting: ValueInputComponent}
  ],
  host: {
    '[class.value-input-floating]': 'shouldLabelFloat',
    '[id]': 'id',
  }
})
export class ValueInputComponent implements OnDestroy, MatFormFieldControl<ValueInputValue>, ControlValueAccessor {
  static nextId = 0;
  @ViewChild('amount') amountInput: HTMLInputElement;
  @ViewChild('unit') unitInput: HTMLInputElement;

  parts: FormGroup;
  stateChanges = new Subject<void>();
  focused = false;
  touched = false;
  controlType = 'value-input';
  id = `value-input-${ValueInputComponent.nextId++}`;
  onChange = (_: any) => {
    //NOSONAR
  };
  onTouched = () => {
    //NOSONAR
  };

  get empty() {
    const {
      value: { amount, unit }
    } = this.parts;

    return !amount && !unit;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input('aria-describedby') userAriaDescribedBy: string;

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }
  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }
  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this._disabled ? this.parts.disable() : this.parts.enable();
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get value(): ValueInputValue | null {
    if (this.parts.valid) {
      const {
        value: { amount, unit }
      } = this.parts;
      return new ValueInputValue(amount, unit);
    }
    return null;
  }
  set value(tel: ValueInputValue | null) {
    const { amount, unit } = tel || new ValueInputValue(null, '');
    this.parts.setValue({ amount, unit });
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return this.parts.invalid && this.touched;
  }

  constructor(
    formBuilder: FormBuilder,
    private _focusMonitor: FocusMonitor,
    public _elementRef: ElementRef<HTMLElement>,
    @Optional() @Inject(MAT_FORM_FIELD) public _formField: MatFormField,
    @Optional() @Self() public ngControl: NgControl) {

    this.parts = formBuilder.group({
      amount: [
        null,
        [Validators.required, Validators.pattern("^[0-9]*$")]
      ],
      unit: [
        null,
        [Validators.required]
      ],
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  /**
   * Event handler that gets callend when achieving
   * the focus.
   *
   * @param event: Emitting event
   */
  onFocusIn(event: FocusEvent) {
    if (!this.focused) {
      this.focused = true;
      this.stateChanges.next();
    }
  }

  /**
   * Event handler that gets called uppon loosing the
   * focus.
   * @param event FocusEvent
   */
  onFocusOut(event: FocusEvent) {
    if (!this._elementRef.nativeElement.contains(event.relatedTarget as Element)) {
      this.touched = true;
      this.focused = false;
      this.onTouched();
      this.stateChanges.next();
    }
  }

  /**
   * Automaticly focuses the next input element.
   *
   * @param control current control
   * @param nextElement next element to set the focus on
   */
  autoFocusNext(control: AbstractControl, nextElement?: HTMLInputElement): void {
    if (!control.errors && nextElement) {
      this._focusMonitor.focusVia(nextElement, 'program');
    }
  }

  /**
   * Automaticly focuses the previous input element.
   *
   * @param control current control
   * @param prevElement previous element to set the focus on
   */
  autoFocusPrev(control: AbstractControl, prevElement: HTMLInputElement): void {
    if (control.value.length < 1) {
      this._focusMonitor.focusVia(prevElement, 'program');
    }
  }

  /**
   * Builder for an aria describedby id list.
   *
   * @param ids descriptor ids
   */
  setDescribedByIds(ids: string[]) {
    const controlElement = this._elementRef.nativeElement
      .querySelector('.value-input-container')!;
    controlElement.setAttribute('aria-describedby', ids.join(' '));
  }

  /**
   * Event handler that gets called whenever
   * the user clicks on the container.
   */
  onContainerClick() {
    //NOSONAR
  }

  /**
   * Setter for the value.
   *
   * @param tel value to set
   */
  writeValue(tel: ValueInputValue | null): void {
    this.value = tel;
  }

  /**
   * Registers a function that will be called whenver
   * this component changes.
   *
   * @param fn function to call
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Registers a function that will be called when
   * the user touches the input components.
   *
   * @param fn function to call
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Setter for the disabled state.
   *
   * @param isDisabled value
   */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /**
   * Event handler that gets called uppon changing the value
   * of the underlying inputs.
   */
  _handleInput(): void {
    this.onChange(this.value);
  }

  static ngAcceptInputType_disabled: BooleanInput;
  static ngAcceptInputType_required: BooleanInput;
}

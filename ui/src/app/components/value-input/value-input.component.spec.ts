// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { FocusMonitor } from '@angular/cdk/a11y';
import { ElementRef, Injectable } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { ValueInputValue } from 'src/app/components/value-input/value-input-value';
import { ValueInputComponent } from './value-input.component';

@Injectable()
export class FakeElementRef extends ElementRef {
  nativeElement = {
    querySelector: () => {}
  };

  constructor() {
    super(new ElementRef<HTMLElement>({
    } as HTMLElement));
  }
}

describe('ValueInputComponent', () => {
  let component: ValueInputComponent;
  let fixture: ComponentFixture<ValueInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ValueInputComponent,
      ],
      imports: [
        ReactiveFormsModule,
      ],
      providers: [
        { provide: ElementRef, useValue: new FakeElementRef() },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check if its empty', () => {
    // arrange
    component.parts = new FormGroup({
      amount: new FormControl('123'),
      unit: new FormControl('kg'),
    });

    // act & assert
    expect(component.empty).toBeFalse();
  });

  it('should check if the labels should float', () => {
    // arrange
    component.focused = true;

    // act & assert
    expect(component.focused).toBeTrue();
  });

  it('should fire a statechange on changing the placeholder', () => {
    // arrange
    const nextSpy = spyOn(component.stateChanges, 'next');

    // act
    component.placeholder = '123';

    // assert
    expect(component.placeholder).toEqual('123');
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should fire a statechange on changing the required attribute', () => {
    // arrange
    const nextSpy = spyOn(component.stateChanges, 'next');

    // act
    component.required = true;

    // assert
    expect(component.required).toBeTrue();
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should fire a statechange on changing the disabled attribute', () => {
    // arrange
    const nextSpy = spyOn(component.stateChanges, 'next');
    const enableSpy = spyOn(component.parts, 'enable');

    // act
    component.disabled = false;

    // assert
    expect(component.disabled).toBeFalse();
    expect(nextSpy).toHaveBeenCalled();
    expect(enableSpy).toHaveBeenCalled();
  });

  it('should fire a statechange on changing the value', () => {
    // arrange
    const nextSpy = spyOn(component.stateChanges, 'next');
    const setValueSpy = spyOn(component.parts, 'setValue');

    // act
    component.value = { amount: 123, unit: 'kg' };

    // assert
    expect(nextSpy).toHaveBeenCalled();
    expect(setValueSpy).toHaveBeenCalledWith({ amount: 123, unit: 'kg' });
  });

  it('should return the error state', () => {
    // arrange
    component.touched = true;

    // act & assert
    expect(component.errorState).toBeTrue();
  });

  it('should set the focus and fire the state changes event', () => {
    // arrange
    component.focused = false;

    const nextSpy = spyOn(component.stateChanges, 'next');

    // act
    component.onFocusIn(new FocusEvent('FocusIn'));

    // assert
    expect(component.focused).toBeTrue();
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should loose focus', () => {
    // arrange
    const focusEvent = new FocusEvent('FocusOut');

    const nextSpy = spyOn(component.stateChanges, 'next');
    const onTouchedSpy = spyOn(component, 'onTouched');

    // act
    component.onFocusOut(focusEvent);

    // assert
    expect(component.touched).toBeTrue();
    expect(component.focused).toBeFalse();
    expect(onTouchedSpy).toHaveBeenCalled();
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should auto focus next control', () => {
    // arrange
    const control = new FormControl('123');
    const nextElement = Object.create(HTMLInputElement.prototype, {});

    const focusMonitor = TestBed.inject(FocusMonitor);
    const focusViaSpy = spyOn(focusMonitor, 'focusVia');

    // act
    component.autoFocusNext(control, nextElement);

    // assert
    expect(focusViaSpy).toHaveBeenCalled();
  });

  it('should auto focus previous control', () => {
    // arrange
    const control = new FormControl('');
    const prevElement = Object.create(HTMLInputElement.prototype, {});
    const focusMonitor = TestBed.inject(FocusMonitor);
    const focusViaSpy = spyOn(focusMonitor, 'focusVia');

    // act
    component.autoFocusPrev(control, prevElement);

    // assert
    expect(focusViaSpy).toHaveBeenCalled();
  });

  it('should build an aria describedby id list', () => {
    // arrange
    const controlElement = Object.create(HTMLInputElement.prototype, {});
    const setAttributeSpy = spyOn(controlElement, 'setAttribute');
    spyOn(component._elementRef.nativeElement, 'querySelector').and.returnValue(controlElement);

    // act
    component.setDescribedByIds(['123', 'TEST']);

    // assert
    expect(setAttributeSpy).toHaveBeenCalledWith('aria-describedby', '123 TEST');
  });

  it('should set the value', () => {
    // arrange
    const expectedElement = new ValueInputValue(123, 'kg');

    // act
    component.writeValue(expectedElement);

    // assert
    expect(component.value).toEqual(expectedElement);
  });

  it('should register change listener', () => {
    // arrange
    const expectedFunction = () => {};

    // act
    component.registerOnChange(expectedFunction);

    // assert
    expect(component.onChange).toEqual(expectedFunction);
  });

  it('should register touched listener', () => {
    // arrange
    const expectedFunction = () => {};

    // act
    component.registerOnTouched(expectedFunction);

    // assert
    expect(component.onTouched).toEqual(expectedFunction);
  });

  it('should set the disabled state', () => {
    // arrange
    component.disabled = true;

    // act
    component.setDisabledState(false);

    // assert
    expect(component.disabled).toBeFalse();
  });

  it('should handle input', () => {
    // arrange
    const expectedElement = new ValueInputValue(123, 'kg');
    component.value = expectedElement;

    const onChangeSpy = spyOn(component, 'onChange');

    // act
    component._handleInput();

    // assert
    expect(onChangeSpy).toHaveBeenCalledWith(expectedElement);
  });
});

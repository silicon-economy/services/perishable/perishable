// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValueDisplayComponent } from 'src/app/components/value-display/value-display.component';
import { ValueInputComponent } from './value-input/value-input.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    ValueDisplayComponent,
    ValueInputComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  exports: [
    ValueDisplayComponent,
    ValueInputComponent,
  ]
})
export class ComponentsModule { }

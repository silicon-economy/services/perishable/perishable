// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Input } from '@angular/core';
import { Value } from 'src/app/interfaces/shipments/value';

/**
 * Displays a {@link Value} in form of a amount / unit pair.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-value-display',
  templateUrl: './value-display.component.html',
  styleUrls: ['./value-display.component.scss']
})
export class ValueDisplayComponent {
  /**
   * The {@link Value} to display.
   */
  @Input() val: Value;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { Value } from 'src/app/interfaces/shipments/value';
import { ShipmentsComponent } from './shipments.component';
import {PipesModule} from "../../services/pipe/pipes.module";
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';

@Component({
  selector: 'app-value-display',
  template: '',
})
class FakeValueDisplayComponent {
  /**
   * The {@link Value} to display.
   */
   @Input() val: Value;
}

describe('ShipmentsComponent', () => {
  let component: ShipmentsComponent;
  let fixture: ComponentFixture<ShipmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ShipmentsComponent,
        FakeValueDisplayComponent,
      ],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        PipesModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct string for the tooltip', () => {
    // arrange
    // act
    var text = component.tooltipMessage(ShipmentStatus.COMPLETED);
    // assert
    expect(text).toEqual('Data complete');
  });

  it('should return correct string for the tooltip', () => {
    // arrange
    // act
    var text = component.tooltipMessage(ShipmentStatus.EXPORTED);
    // assert
    expect(text).toEqual('Data for pre declaration exported');
  });

  it('should return correct string for the tooltip', () => {
    // arrange
    // act
    var text = component.tooltipMessage(ShipmentStatus.INCOMPLETE);
    // assert
    expect(text).toEqual('New shipment record, data incomplete, pre-declaration open');
  });
});

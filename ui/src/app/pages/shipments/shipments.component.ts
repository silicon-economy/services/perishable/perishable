// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, OnInit } from '@angular/core';
import { ShipmentInfo } from 'src/app/interfaces/shipments/shipment-info';
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';
import { ShipmentsService } from 'src/app/services/api/shipments.service';

/**
 * Page that displayes the table of shipments.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-shipments',
  templateUrl: './shipments.component.html',
  styleUrls: ['./shipments.component.scss']
})
export class ShipmentsComponent implements OnInit {

  /**
   * Array that holds all available shipments.
   */
  shipments: ShipmentInfo[];

  /**
   * @param status the hovered ShipmentStatus
   * @returns the tooltip text dynamically, depending on the shipmentStatus of the hovered shipment
   */
  tooltipMessage(status:ShipmentStatus): string {
    if (status === ShipmentStatus.EXPORTED) {
      return "Data for pre declaration exported"
    }
    else if (status === ShipmentStatus.COMPLETED) {
      return "Data complete"
    }
    else {
      return "New shipment record, data incomplete, pre-declaration open"
    }
  }

  /**
   * Controls which colums get displayed.
   */
  displayedColumns: string[] = [
    'awb',
    'externalReference',
    'type',
    'actualKg',
    'ulds',
    'shipper',
    'org',
    'flightNr',
    'sta',
    'eta',
    'pickUpDate',
    'deliveryDeadlineLocation',
    'shipmentStatus',
    'details',
  ];

  constructor(
    private shipmentsService: ShipmentsService,
  ) { }

  ngOnInit(): void {
    this.shipmentsService.getShipments().subscribe(data => {
      this.shipments = data;
    })
  }
}

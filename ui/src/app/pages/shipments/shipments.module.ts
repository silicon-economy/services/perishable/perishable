// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShipmentsComponent } from './shipments.component';
import { ShipmentsRoutingModule } from 'src/app/pages/shipments/shipments-routing.module';
import { MatTableModule } from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ComponentsModule } from 'src/app/components/components.module';
import {PipesModule} from "../../services/pipe/pipes.module";
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [
    ShipmentsComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    ShipmentsRoutingModule,
    MatTableModule,
    FlexLayoutModule,
    PipesModule,
    MatTooltipModule,
  ]
})
export class ShipmentsModule { }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailedViewComponent } from 'src/app/pages/detailed-view/detailed-view.component';

const routes: Routes = [
  { path: '', component: DetailedViewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailedViewRoutingModule { }

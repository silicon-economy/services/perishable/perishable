// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AssociatedPosition } from 'src/app/pages/detailed-view/associated-position';
import { AssociatedDocument } from 'src/app/pages/detailed-view/associated-document';
import { ShipmentDetails } from 'src/app/interfaces/shipments/shipment-details';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';
import { DocumentStatusEditComponent } from './detailed-view-document-status-edit/detailed-view-document-status-edit.component';
import { MatDialog } from '@angular/material/dialog';
import { State } from './detailed-view-document-status-edit/detailed-view-document-status-edit-state.enum';
import { ValidationState } from 'src/app/interfaces/shipments/validation-state.enum';
import { PieceDocumentValidationState } from 'src/app/interfaces/shipments/piece-document-validation-state.enum';
import { DetailedViewPositionDialogComponent } from 'src/app/pages/detailed-view/detailed-view-position-dialog/detailed-view-position-dialog.component';
import { DetailedViewPositionDialogResult } from 'src/app/pages/detailed-view/detailed-view-position-dialog/detailed-view-position-dialog-result.enum';
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';

/**
 * This page shows detailed information about a shipment.
 *
 * @author Marc Dickmann, Anton Thomas Scholz
 */
@Component({
  selector: 'app-detailed-view',
  templateUrl: './detailed-view.component.html',
  styleUrls: ['./detailed-view.component.scss']
})
export class DetailedViewComponent implements OnInit, OnDestroy {

  /**
   * A collector subscription for the refresh method. All subscriptions to
   * oberservables inside this function should be added to this.
   */
  private refreshSubscription: Subscription = new Subscription();

  /**
   * Holds the data to display.
   */
  shipment: ShipmentDetails;

  /**
   * Controls which colums get displayed inside the table of positions.
   */
  displayedPositionColumns = [
    'position',
    'type',
    'hsCode',
    'eppoCode',
    'countryOfOrigin',
    'quantity',
    'weight',
    'packaging',
    'packagingCode',
    'plannedTemp',
    'uldId',
    'actions'
  ];

  displayedDocumentColumns = [
    'position',
    'document',
    'comments',
    'status',
    'actions',
  ];

  /**
   * Holds all positions, each with a unique number.
   */
  associatedPositions: AssociatedPosition[];

  /**
   * Holds all documents with potentially associated positions.
   */
  associatedDocuments: AssociatedDocument[];

  /**
   * Used to make the enum ValidationState useable inside template.
   */
  ValidationState = ValidationState;

  /**
   * Used to make the enum PieceDocumentValidationState useable inside template.
   */
  PieceDocumentValidationState = PieceDocumentValidationState;

  /**
   * Holds true if the mawb and phyto certificate documents are valid.
   */
  get shipmentDocumentsValid(): boolean {
    return this.shipment?.validationResult.mawb === PieceDocumentValidationState.VALID &&
           this.shipment?.validationResult.phytoCertificate === PieceDocumentValidationState.VALID;
  }

  /**
   * Holds true if the consignor, consignee and place of destination agents are valid.
   */
  get shipmentAgentsValid(): boolean {
    return this.shipment?.validationResult.consignor === ValidationState.VALID &&
           this.shipment?.validationResult.consignee === ValidationState.VALID &&
           this.shipment?.validationResult.placeOfDestination === ValidationState.VALID;
  }

  /**
   * Holds true if the shipment is valid.
   */
  get shipmentValid(): boolean {
    if (this.shipment?.positions.filter(iter => {
      if (iter.validationResult.countryOfOrigin === ValidationState.VALID &&
          iter.validationResult.eppoCode === ValidationState.VALID &&
          iter.validationResult.hsCode === ValidationState.VALID &&
          iter.validationResult.quantity === ValidationState.VALID &&
          iter.validationResult.weight === ValidationState.VALID) {
            // Remove
            return false;
          } else {
            // Keep
            return true;
          }
    }).length !== 0) {
      return false;
    }
    return this.shipmentDocumentsValid && this.shipmentAgentsValid;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private shipmentDetailsService: ShipmentDetailsService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.refresh();
  }

  /**
   * This method fetches the data which is shown in the detailed view table.
   */
  refresh(): void {
    this.refreshSubscription.unsubscribe();
    this.refreshSubscription = new Subscription();
    this.refreshSubscription.add(this.activatedRoute.paramMap.subscribe(paramMap => {
      if (paramMap.has('shipmentId')) {
        const id = paramMap.get('shipmentId');
        this.refreshSubscription.add(this.shipmentDetailsService.getShipmentDetails(id).subscribe(shipmentDetails => {
          this.shipment = shipmentDetails;
          this.shipment.shipmentAgents.sort((a, b) => this.shipmentAgentSort(a.role, b.role));

          // Give a unique identifier (human legible) to each position!
          this.associatedPositions = [];
          for (let i = 0; i < shipmentDetails.positions.length; i++) {
            const pos = shipmentDetails.positions[i] as AssociatedPosition;
            pos.no = i + 1;
            this.associatedPositions.push(pos);
          }

          // Associate documents with positions!
          this.associatedDocuments = shipmentDetails.documents.map(iter => {
            const r = iter as AssociatedDocument;
            r.nos = this.associatedPositions
              .filter(pos => iter.pieces?.includes(pos.id))
              .map(pos => pos.no);
            return r;
          });
        }));
      }
    }));
  }

  /**
   * Calculates a rank for a {@link ShipmentAgent} role.
   * Based on the place it should take from left to right the number will be descending.
   * Meaning the first agent gets the highest number.
   *
   * @param role Role of the {@link ShipmentAgent}
   * @returns Rank of the {@link ShipmentAgent}
   */
  shipmentAgentSortRank(role: string): number {
    switch(role) {
      case 'Consignor':
        return 5000;
      case 'Consignee':
        return 4000;
      case 'Carrier':
        return 3000;
      case 'Export Agent':
        return 2000;
      case 'Place of Destination':
        return 1000;
      default:
        return 0;
    }
  }

  /**
   * Sorting functions for sorting {@link ShipmentAgent}s based on their role with a prefered ordering of
   * some entries.
   *
   * @param roleA Comparative a
   * @param roleB Comparative b
   * @returns Negative number when a is greater than b
   */
  shipmentAgentSort(roleA: string, roleB: string): number {
    return (this.shipmentAgentSortRank(roleB) - this.shipmentAgentSortRank(roleA)) + roleA.localeCompare(roleB);
  }

  /**
   * The content of the document can be downloaded from this link.
   *
   * @param link is the link of requested document
   * @returns A link which will trigger the Document Download when user clicks it
   */
  getDocumentUrl(link: string): string {
    return `${environment.apiHost}:${environment.apiPort}${ link }`;
   }

  ngOnDestroy(): void {
    this.refreshSubscription.unsubscribe();
  }

  /**
   * Counts the amount of unique ULDs inside the shipment.
   *
   * @returns The amount of unique ULDs
   */
  countUniqueULDs(): number {
    // create a list of non duplicated uld ids!
    let uniqueULDs = this.shipment.positions.map(elem => elem.uldId);
    uniqueULDs = uniqueULDs.filter((iter, index) => uniqueULDs.indexOf(iter) === index);

    return uniqueULDs.length;
  }

  /**
   * This function opens a dialog to edit a chosen document,
   * when the dialog is closed the resulting enum state shows if changes where made and the data should be reloaded
   *
   * @param doc is the chosen document the user wants to edit
   */
  callForm(doc: AssociatedDocument): void {
    let dialogRef = this.dialog.open(DocumentStatusEditComponent, {
      data: {
        doc: doc,
        positions: this.associatedPositions,
        shipmentId: this.shipment.id,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    })
  }

  /**
   * This function returns a link to download the predeclaration data
   * @param id refers to the specific shipment detail
   * @returns the link leads to download the data from backend
   */
  getPredeclarationUrl(id: string): string {
    const apiURL = `${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails`;
    return `${ apiURL}/${ id }/predeclaration`;
  }

  /**
   * This function returns a downloadlink to download the document
   * @returns the downloadlink to download the clicked document data
   */
  getDocumentUrls(): string[] {
    return this.associatedDocuments.map((doc) => {
      return `${environment.apiHost}:${environment.apiPort}${doc.ref.documentLink}`;
    });
  }

  /**
   * Event handler that is invokek when the user clicks the "Export data for Pre-Declaration" button.
   * Opens the link to the pre declaration document in a new browser tab.
   */
  onExportPreDeclarationClicked(): void {
    window.open(this.getPredeclarationUrl(this.shipment.id), '_blank');
  }

  /**
   * Shows the position dialog and refreshes the data if the dialog
   * results in {@link DetailedViewPositionDialogResult#SAVED}.
   *
   * @param position Position to edit; defaults to undefined
   */
  openPositionDialog(position: AssociatedPosition = undefined): void {
    const dialogRef = this.dialog.open(DetailedViewPositionDialogComponent, {
      data: {
        id: this.shipment.id,
        position,
        shipmentRev: this.shipment.shipmentAgents[0].rev,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === DetailedViewPositionDialogResult.SAVE) {
        this.refresh();
      }
    });
  }

  /**
   * Event handler for the add position button.
   */
  onAddPositionClicked(): void {
    this.openPositionDialog();
  }

  /**
   * Event handler for the edit position button.
   *
   * @param position Position to edit
   */
  onEditPositionClicked(position: AssociatedPosition): void {
    this.openPositionDialog(position);
  }

  /**
   * This method returns the dynamic string for the tooltip.
   * @param status the hovered ShipmentStatus
   * @returns the tooltip text dynamically, depending on the shipmentStatus of the hovered shipment
   */
   tooltipMessage(status: ShipmentStatus): string {
    switch (status) {
      case ShipmentStatus.EXPORTED:
        return 'Data for pre declaration exported';
      case ShipmentStatus.COMPLETED:
        return 'Data complete';
      default:
        return 'New shipment record, data incomplete, pre-declaration open';
    }
  }
}

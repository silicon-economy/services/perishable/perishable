// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { PieceDocument } from 'src/app/interfaces/shipments/piece-document';

/**
 * Wrapper for {@link Document} that get associated with a position.
 *
 * @author Marc Dickmann
 */
export interface AssociatedDocument extends PieceDocument {
  /**
   * The positions the document is associated with.
   */
  nos?: number[];
}

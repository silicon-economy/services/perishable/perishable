// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { PieceDocumentState } from 'src/app/interfaces/shipments/piece-document-state.enum';
import { PieceDocumentType } from 'src/app/interfaces/shipments/piece-document-type.enum';
import { PieceDocumentValidationState } from 'src/app/interfaces/shipments/piece-document-validation-state.enum';
import { SequenceDescription } from 'src/app/interfaces/shipments/sequence-description.enum';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { ShipmentDetails } from 'src/app/interfaces/shipments/shipment-details';
import { TemperatureHandling } from 'src/app/interfaces/shipments/temperature-handling.enum';
import { TimeSequence } from 'src/app/interfaces/shipments/time-sequence';
import { ValidationState } from 'src/app/interfaces/shipments/validation-state.enum';
import { Value } from 'src/app/interfaces/shipments/value';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';
import { environment } from 'src/environments/environment';
import { AssociatedDocument } from './associated-document';
import { MatIconModule } from '@angular/material/icon';
import { DetailedViewComponent } from './detailed-view.component';
import { State } from './detailed-view-document-status-edit/detailed-view-document-status-edit-state.enum';
import {PipesModule} from "../../services/pipe/pipes.module";
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';
import {MatTooltipModule} from '@angular/material/tooltip';

const mockDoc: AssociatedDocument = {
  nos: [1,2],
  id: '235',
  rev: 'testREV',
  pieces: ['1', '2'],
  ref: {
    uri: 'test',
    documentName: 'testName',
    documentLink: 'testLink'
  },
  comment: 'testender Kommentar',
  documentType : PieceDocumentType.CATCH_CERTIFICATE,
  state: PieceDocumentState.APPROVED
}

const EXPECTED_SHIPMENT: ShipmentDetails = {
  shipmentStatus: ShipmentStatus.COMPLETED,
  id: 'AWB',
  rev: 'ShipmentRev',
  goodType: 'Roses',
  org: 'ADD',
  uldId: 'ULDID12345',
  flightNo: '999 Banane',
  std: new Date('2019-01-04T08:50:24.514Z'),
  etd: new Date('2019-01-04T08:50:24.514Z'),
  sta: new Date('2019-01-04T08:50:24.514Z'),
  eta: new Date('2019-01-04T08:50:24.514Z'),
  timeSequences: [
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T08:50:24.514Z'),
          timeDescription: 'In',
        },
        {
          time: new Date('2019-01-04T09:50:24.514Z'),
          timeDescription: 'Out',
        },
      ],
      sequenceDescription: SequenceDescription.EXPORT_WAREHOUSE,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T10:50:24.514Z'),
          timeDescription: '',
        },
      ],
      sequenceDescription: SequenceDescription.OFF_BLOCKS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T11:50:24.514Z'),
          timeDescription: '',
        },
      ],
      sequenceDescription: SequenceDescription.ON_BLOCKS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T12:50:24.514Z'),
          timeDescription: 'Received',
        },
        {
          time: new Date('2019-01-04T13:50:24.514Z'),
          timeDescription: 'Delivered',
        },
      ],
      sequenceDescription: SequenceDescription.GROUND_HANDLER,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T14:50:24.514Z'),
          timeDescription: 'Acceptance',
        },
        {
          time: new Date('2019-01-04T15:50:24.514Z'),
          timeDescription: 'Check time',
        },
        {
          time: new Date('2019-01-04T16:50:24.514Z'),
          timeDescription: 'Ready for inspection',
        },
      ],
      sequenceDescription: SequenceDescription.IMPORT_WAREHOUSE,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T17:50:24.514Z'),
          timeDescription: 'Phytosanitarian release',
        },
        {
          time: new Date('2019-01-04T18:00:24.514Z'),
          timeDescription: 'Veterinarian release',
        },
        {
          time: new Date('2019-01-04T18:50:24.514Z'),
          timeDescription: 'BLE release',
        },
      ],
      sequenceDescription: SequenceDescription.RELEASES,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T19:50:24.514Z'),
          timeDescription: 'Release',
        },
      ],
      sequenceDescription: SequenceDescription.CUSTOMS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T20:00:24.514Z'),
          timeDescription: 'Ready',
        },
        {
          time: new Date('2019-01-04T21:00:24.514Z'),
          timeDescription: 'Arrival',
        },
        {
          time: new Date('2019-01-04T22:00:24.514Z'),
          timeDescription: 'Departure',
        },
      ],
      sequenceDescription: SequenceDescription.PICK_UP,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T23:50:24.514Z'),
          timeDescription: 'Arrival',
        },
      ],
      sequenceDescription: SequenceDescription.DELIVERY_ADDRESS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T23:50:24.514Z'),
          timeDescription: 'Arrival',
        },
      ],
      sequenceDescription: SequenceDescription.DELIVERY_ADDRESS,
    },
  ],
  shipmentAgents: [
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'consignor',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'consignee',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'carrier',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'export_agent',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'place_of_destination',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityName: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    }
  ],
  positions: [
    {
      id: 'PIECE_ID',
      uldId: 'UNIQUE_ULD-ID',
      hsCode: 'HS-CODE',
      eppoCode: 'EPPO-CODE',
      goodType: 'Roses',
      country: 'DE',
      packaging: 'pallet',
      packagingCode: 'pallet-code',
      plannedTemp: TemperatureHandling.FRO,
      group: 'GROUP',
      weight: {
        uri: 'URI',
        amount: 105.0,
        unit: 'kg',
      },
      quantity: {
        uri: 'URI',
        amount: 1234,
        unit: 'stk',
      },
      validationResult: {
        hsCode: ValidationState.VALID,
        eppoCode: ValidationState.VALID,
        countryOfOrigin: ValidationState.VALID,
        quantity: ValidationState.VALID,
        weight: ValidationState.VALID,
      }
    },
    {
      id: 'PIECE_ID2',
      uldId: 'NON-UNIQUE_ULD-ID',
      hsCode: 'HS-CODE',
      eppoCode: 'EPPO-CODE',
      goodType: 'Roses',
      country: 'DE',
      packaging: 'pallet',
      packagingCode: 'pallet-code',
      plannedTemp: TemperatureHandling.FRO,
      group: 'GROUP',
      weight: {
        uri: 'URI',
        amount: 105.0,
        unit: 'kg',
      },
      quantity: {
        uri: 'URI',
        amount: 1234,
        unit: 'stk',
      },
      validationResult: {
        hsCode: ValidationState.VALID,
        eppoCode: ValidationState.VALID,
        countryOfOrigin: ValidationState.VALID,
        quantity: ValidationState.VALID,
        weight: ValidationState.VALID,
      }
    },
    {
      id: 'PIECE_ID3',
      uldId: 'NON-UNIQUE_ULD-ID',
      hsCode: 'HS-CODE',
      eppoCode: 'EPPO-CODE',
      goodType: 'Roses',
      country: 'DE',
      packaging: 'pallet',
      packagingCode: 'pallet-code',
      plannedTemp: TemperatureHandling.FRO,
      group: 'GROUP',
      weight: {
        uri: 'URI',
        amount: 105.0,
        unit: 'kg',
      },
      quantity: {
        uri: 'URI',
        amount: 1234,
        unit: 'stk',
      },
      validationResult: {
        hsCode: ValidationState.VALID,
        eppoCode: ValidationState.VALID,
        countryOfOrigin: ValidationState.VALID,
        quantity: ValidationState.VALID,
        weight: ValidationState.VALID,
      }
    },
  ],
  actualWeight: {
    uri: 'URI',
    amount: 105.0,
    unit: 'kg',
  },
  documents: [
    {
      id: 'testID',
      rev: 'testREV',
      pieces: [
        'PIECE_ID',
        'PIECE_ID3',
      ],
      ref: {
        uri: 'URI',
        documentName: 'TestDokument.pdf',
        documentLink: 'LINK TO DOCUMENT',
      },
      comment: 'Just a comment.',
      documentType: PieceDocumentType.CATCH_CERTIFICATE,
      state: PieceDocumentState.INCORRECT
    }
  ],
  validationResult: {
    consignor: ValidationState.VALID,
    consignee: ValidationState.VALID,
    placeOfDestination: ValidationState.VALID,
    phytoCertificate: PieceDocumentValidationState.VALID,
    mawb: PieceDocumentValidationState.VALID,
  }
};

class FakeShipmentDetailsService {
  getShipmentDetails(awb: string, ref: string): Observable<ShipmentDetails> {
    return of(EXPECTED_SHIPMENT);
  }
}

@Component({
  selector: 'app-detailed-view-agent-card',
  template: '',
})
class FakeDetailedViewAgentCardComponent {
  /**
   * Agent to display.
   */
   @Input()
   agent: ShipmentAgent;

   /**
   * Awb to pass.
   */
    @Input()
    awb: string;
}

@Component({
  selector: 'app-value-display',
  template: '',
})
class FakeValueDisplayComponent {
  /**
   * The {@link Value} to display.
   */
   @Input() val: Value;
}

@Component({
  selector: 'app-detailed-view-sequence-entry',
  template: '',
})
class FakeDetailedViewSequenceEntryComponent {
  /**
   * The {@link TimeSequence} to display.
   */
   @Input()
   entry: TimeSequence;
}

describe('DetailedViewComponent', () => {
  let component: DetailedViewComponent;
  let fixture: ComponentFixture<DetailedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DetailedViewComponent,
        FakeDetailedViewAgentCardComponent,
        FakeDetailedViewSequenceEntryComponent,
        FakeValueDisplayComponent,
      ],
      providers: [
        { provide: ShipmentDetailsService, useClass: FakeShipmentDetailsService },
        {
          provide: ActivatedRoute, useValue: {
            paramMap: of(convertToParamMap({
              shipmentId: EXPECTED_SHIPMENT.id,
            }))
          }
        },
        { provide: MAT_DIALOG_DATA, useValue: { data: mockDoc } },
      ],
      imports: [
        MatTableModule,
        MatCardModule,
        MatDialogModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PipesModule,
        MatIconModule,
        MatTooltipModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load the shipment details on init', () => {
    // arrange
    const shipmentDetailsService = TestBed.inject(ShipmentDetailsService);
    const getShipmentDetailsSpy = spyOn(shipmentDetailsService, 'getShipmentDetails').and.returnValue(of(EXPECTED_SHIPMENT));

    // act
    component.ngOnInit();

    // assert
    expect(component).toBeTruthy();
    expect(getShipmentDetailsSpy).toHaveBeenCalledWith(EXPECTED_SHIPMENT.id);
    expect(component.shipment).toEqual(EXPECTED_SHIPMENT);
  });

  it('should build the document download link properly', () => {
    // arrange
    const link = "/api/v1/shipment/563-73609538/Veterinary.pdf";

    // act
    const testUrl = component.getDocumentUrl(link);

    // assert
    expect(testUrl).toEqual(`${environment.apiHost}:${environment.apiPort}/api/v1/shipment/563-73609538/Veterinary.pdf`);
  });

  it('should count unique ULDs', () => {
    // act & assert
    expect(component.countUniqueULDs()).toEqual(2);
  });

  it('should associate documents with positions', () => {
    // arrange
    const expectedDocument = {
      nos: [
        0,
        2,
      ],
      ... EXPECTED_SHIPMENT.documents[0],
    };
    const expectedPosition = {
      no: 0,
      ... EXPECTED_SHIPMENT.positions[0],
    };

    // assert
    expect(component.associatedDocuments).toContain(expectedDocument);
    expect(component.associatedPositions).toContain(expectedPosition);
  })

  it('should rank shipment agents', () => {
    // act & assert
    expect(component.shipmentAgentSortRank('Consignor')).toEqual(5000);
    expect(component.shipmentAgentSortRank('Consignee')).toEqual(4000);
    expect(component.shipmentAgentSortRank('Carrier')).toEqual(3000);
    expect(component.shipmentAgentSortRank('Export Agent')).toEqual(2000);
    expect(component.shipmentAgentSortRank('Place of Destination')).toEqual(1000);
    expect(component.shipmentAgentSortRank('Unknonw')).toEqual(0);
  });

  it('should sort shipmentAgents', () => {
    // act && assert
    expect(component.shipmentAgentSort('Consignee', 'Consignor')).toEqual(999);
  });

  it('should generate correct link', () => {
    // arrange
      const id = '123-78787';
    // act && assert
      expect(component.getPredeclarationUrl(id)).toEqual(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/123-78787/predeclaration`);
  });

  it('should open the link the predeclaration document in a new tab', () => {
    // arrange
    const openSpy = spyOn(window, 'open');
    const getPredeclarationUrlSpy = spyOn(component, 'getPredeclarationUrl').and.returnValue('TEST-URL');

    // act
    component.onExportPreDeclarationClicked();

    // assert
    expect(getPredeclarationUrlSpy).toHaveBeenCalledWith(EXPECTED_SHIPMENT.id);
    expect(openSpy).toHaveBeenCalledWith('TEST-URL', '_blank');
  })

  it('should call form', () => {
    // arrange
    spyOn(component.dialog, 'open')
     .and
     .returnValue(
       {afterClosed: () => of(State.CANCLED)} as MatDialogRef<typeof component>
      );
    // act
    component.callForm(mockDoc);

    // assert
    expect(component).toBeTruthy();
  })

  it('should call getDocumentUrls() correct', () => {
    // act
    let testValue = component.getDocumentUrls();

    // assert
    expect(testValue).toEqual([`${environment.apiHost}:${environment.apiPort}LINK TO DOCUMENT`]);
  })

  it('should return correct string for the tooltip', () => {
    // act
    var text = component.tooltipMessage(ShipmentStatus.COMPLETED);

    // assert
    expect(text).toEqual('Data complete');
  });

  it('should return correct string for the tooltip', () => {
    // act
    var text = component.tooltipMessage(ShipmentStatus.EXPORTED);

    // assert
    expect(text).toEqual('Data for pre declaration exported');
  });

  it('should return correct string for the tooltip', () => {
    // act
    var text = component.tooltipMessage(ShipmentStatus.INCOMPLETE);

    // assert
    expect(text).toEqual('New shipment record, data incomplete, pre-declaration open');
  });
});

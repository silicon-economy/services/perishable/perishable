// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PieceDetails } from 'src/app/interfaces/shipments/piece-details';
import { AssociatedPosition } from 'src/app/pages/detailed-view/associated-position';
import { DetailedViewPositionDialogResult } from 'src/app/pages/detailed-view/detailed-view-position-dialog/detailed-view-position-dialog-result.enum';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';

/**
 * Dialog for editing / adding positions to the shipment details.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-detailed-view-position-dialog',
  templateUrl: './detailed-view-position-dialog.component.html',
  styleUrls: ['./detailed-view-position-dialog.component.scss']
})
export class DetailedViewPositionDialogComponent {
  /**
   * Triggers the alertbox when data is changed in parallel
   */
  showAlertBox = false;

  /**
   * Makes the form accessible via TypeScript.
   */
  positionForm = new FormGroup ({
    pos: new FormControl(this.data.position?.no),
    type: new FormControl(this.data.position?.goodType),
    hsCode: new FormControl(this.data.position?.hsCode, Validators.required),
    eppoCode: new FormControl(this.data.position?.eppoCode, Validators.required),
    countryOfOrigin: new FormControl(this.data.position?.country, Validators.required),
    quantity: new FormControl(this.data.position?.quantity, Validators.required),
    weight: new FormControl(this.data.position?.weight, Validators.required),
    packaging: new FormControl(this.data.position?.packaging),
    packagingCode: new FormControl(this.data.position?.packagingCode),
  });

  /**
   * Holds true when the dialog is used to edit an existing position.
   */
  get isEditing(): boolean {
    return this.data.position !== undefined;
  }

  constructor(
    private dialogRef: MatDialogRef<DetailedViewPositionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { id: string, position: AssociatedPosition, shipmentRev: string },
    private shipmentDetailsService: ShipmentDetailsService,
  ) { }

  /**
   * Propagates the entered position data to the server and
   * closes the dialog resulting in {@link DetailedViewPositionDialogResult#SAVE}.
   */
  onSaveClicked(): void {
    let position: PieceDetails = {} as PieceDetails;
    if (this.isEditing) {
      position = this.data.position;
    }

    position.goodType = this.positionForm.value.type;
    position.hsCode = this.positionForm.value.hsCode;
    position.eppoCode = this.positionForm.value.eppoCode;
    position.country = this.positionForm.value.countryOfOrigin;
    position.quantity = this.positionForm.value.quantity;
    position.weight = this.positionForm.value.weight;
    position.packaging = this.positionForm.value.packaging;
    position.packagingCode = this.positionForm.value.packagingCode;

    this.shipmentDetailsService.putPieceDetails(this.data.id, position, this.data.shipmentRev).subscribe(result => {
      this.dialogRef.close(DetailedViewPositionDialogResult.SAVE);
    }, error => {
      // In this case the conflict 409 status code means that the backend observed two data changes on the same data without saving the first.
      if (error.status === 409) {
        this.showAlertBox = true;
      }
    }
    );
  }

  /**
   * Closes the dialog resulting in {@link DetailedViewPositionDialogResult#CANCLE}.
   */
  onCancleClicked(): void {
    this.dialogRef.close(DetailedViewPositionDialogResult.CANCLE);
  }
}

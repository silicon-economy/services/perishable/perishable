// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Possible actions the {@link DetailedViewPositionDialogComponent} can result in.
 *
 * @author Marc Dickmann
 */
export enum DetailedViewPositionDialogResult {
  CANCLE,
  SAVE,
}

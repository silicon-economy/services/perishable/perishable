// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';
import { ComponentsModule } from 'src/app/components/components.module';
import { PieceDetails } from 'src/app/interfaces/shipments/piece-details';
import { Value } from 'src/app/interfaces/shipments/value';
import { DetailedViewPositionDialogResult } from 'src/app/pages/detailed-view/detailed-view-position-dialog/detailed-view-position-dialog-result.enum';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';

import { DetailedViewPositionDialogComponent } from './detailed-view-position-dialog.component';

// mock dialog with close method
const dialogMock = {
  close: () => { }
};

describe('DetailedViewPositionDialogComponent', () => {
  let component: DetailedViewPositionDialogComponent;
  let fixture: ComponentFixture<DetailedViewPositionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DetailedViewPositionDialogComponent,
      ],
      imports: [
        MatDialogModule,
        MatFormFieldModule,
        MatButtonModule,
        HttpClientTestingModule,
        ComponentsModule,
        ReactiveFormsModule,
        MatInputModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
        { provide: MAT_DIALOG_DATA, useValue: {id: 'TestID', position: undefined, shipmentRev: 'ShipmentRev'} },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedViewPositionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should determine if its in editing mode', () => {
    // arrange
    component['data'].position = undefined;

    // act & assert
    expect(component.isEditing).toBeFalse();
  });

  it('should close the dialog if cancle clicked', () => {
    // arrange
    const dialogRef = TestBed.inject(MatDialogRef);
    const closeSpy = spyOn(dialogRef, 'close');

    // act
    component.onCancleClicked();

    // assert
    expect(closeSpy).toHaveBeenCalledWith(DetailedViewPositionDialogResult.CANCLE);
  });

  it('should propagate the position to the server and close the dialog', () => {
    // arrange
    const expectedElement = {
      goodType: 'GoodType',
      hsCode: 'HS-Code',
      eppoCode: 'EppoCode',
      country: 'Country',
      quantity: {
        amount: 123,
        unit: 'Unit',
      } as Value,
      weight: {
        amount: 123,
        unit: 'Unit',
      } as Value,
      packaging: 'Packaging',
      packagingCode: 'PackagingCode',
    } as PieceDetails;

    component.positionForm.setValue({
      pos: 5,
      type: expectedElement.goodType,
      hsCode: expectedElement.hsCode,
      eppoCode: expectedElement.eppoCode,
      countryOfOrigin: expectedElement.country,
      quantity: expectedElement.quantity,
      weight: expectedElement.weight,
      packaging: expectedElement.packaging,
      packagingCode: expectedElement.packagingCode,
    });

    const dialogRef = TestBed.inject(MatDialogRef);
    const closeSpy = spyOn(dialogRef, 'close');

    const shipmentDetailsService = TestBed.inject(ShipmentDetailsService);
    const putPieceDetailsSpy = spyOn(shipmentDetailsService, 'putPieceDetails').and.returnValue(of(expectedElement));

    // act
    component.onSaveClicked();

    // assert
    expect(closeSpy).toHaveBeenCalledWith(DetailedViewPositionDialogResult.SAVE);
    expect(putPieceDetailsSpy).toHaveBeenCalledWith('TestID', expectedElement, 'ShipmentRev');
  });

  it('should show the warning box', () => {
    // act
    component.showAlertBox = true;
    fixture.detectChanges();

    // assert
    const alertBoxElement = fixture.debugElement.query(By.css('.alert-box')).nativeElement;
    expect(alertBoxElement.innerText).toEqual('Current data is not the latest, please close the dialog to recieve the latest data!');
  });

  it('should throw error and change trigger variable', () => {
    // arrange
    const shipmentService = TestBed.inject(ShipmentDetailsService);
    const getShipmentDetailsServiceSpy = spyOn(shipmentService, 'putPieceDetails').and.returnValue(throwError({status: 409}));

    // act
    component.onSaveClicked();

    // assert
    expect(component).toBeTruthy();
    expect(getShipmentDetailsServiceSpy).toHaveBeenCalled();
    expect(component.showAlertBox).toEqual(true);
  });
});

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { DetailedViewAgentCardComponent } from 'src/app/pages/detailed-view/detailed-view-agent-card/detailed-view-agent-card.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';

const EXPECTED_SHIPMENT_AGENT: ShipmentAgent = {
  rev: 'ShipmentRev',
  name: 'testNAME',
  role: 'carrier',
  address: {
    uri: 'URI',
    street: 'Wonderstreet',
    cityCode: 'Dortmund',
    country: {
      uri: 'URI',
      countryName: 'Magic Land',
      countryCode: 'test',
    }
  }
};

describe('DetailedViewAddressCardComponent', () => {
  let component: DetailedViewAgentCardComponent;
  let fixture: ComponentFixture<DetailedViewAgentCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DetailedViewAgentCardComponent,
      ],
      imports: [
        MatCardModule,
        MatDialogModule,
        MatIconModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedViewAgentCardComponent);
    component = fixture.componentInstance;
    component.agent = EXPECTED_SHIPMENT_AGENT;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

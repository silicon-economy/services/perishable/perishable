// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { DetailedViewAddressEditComponent } from '../detailed-view-address-edit/detailed-view-address-edit.component';
/**
 * Used to display an agent inside the details view.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-detailed-view-agent-card',
  templateUrl: './detailed-view-agent-card.component.html',
  styleUrls: ['./detailed-view-agent-card.component.scss']
})
export class DetailedViewAgentCardComponent {
  /**
   * Address to display.
   */
   @Input()
   agent: ShipmentAgent;

  /**
   * Awb to corresponding shipment.
   */
   @Input()
   awb: string;

   constructor(private dialog: MatDialog) {}

   /**
    * This functions opnens the AddressEditComponent in a dialog where changes are made to the specific agent.
    * @param agent This parameter holds the agent data which is about to get changed.
    * @param awb This parameter holds the awb of the shipment the agent belongs to.
    */
   openEditDialog(agent: ShipmentAgent, awb: string): void {
    this.dialog.open(DetailedViewAddressEditComponent, {
      data: {
        agent,
        awb
      }
    });
   }
}

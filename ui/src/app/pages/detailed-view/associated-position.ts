// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { PieceDetails } from 'src/app/interfaces/shipments/piece-details';

/**
 * Wrapper for {@link PieceDetails} that includes a unique number, documents can use
 * to associate with.
 *
 * @author Marc Dickmann
 */
export interface AssociatedPosition extends PieceDetails {
  /**
   * Unique position id that is used to associate documents with a position.
   */
  no: number;
}

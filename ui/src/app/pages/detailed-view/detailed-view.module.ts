// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailedViewRoutingModule } from 'src/app/pages/detailed-view-of-shipment-routing.module';
import { DetailedViewComponent } from './detailed-view.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DetailedViewAgentCardComponent } from 'src/app/pages/detailed-view/detailed-view-agent-card/detailed-view-agent-card.component';
import { DetailedViewAddressEditComponent } from './detailed-view-address-edit/detailed-view-address-edit.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DetailedViewSequenceEntryComponent } from './detailed-view-sequence-entry/detailed-view-sequence-entry.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { DocumentStatusEditComponent } from './detailed-view-document-status-edit/detailed-view-document-status-edit.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {PipesModule} from "../../services/pipe/pipes.module";
import { DetailedViewPositionDialogComponent } from './detailed-view-position-dialog/detailed-view-position-dialog.component';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [
    DetailedViewComponent,
    DetailedViewAgentCardComponent,
    DetailedViewAddressEditComponent,
    DetailedViewSequenceEntryComponent,
    DocumentStatusEditComponent,
    DetailedViewPositionDialogComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    DetailedViewRoutingModule,
    FlexLayoutModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatDialogModule,
    MatSelectModule,
    MatListModule,
    MatIconModule,
    FormsModule,
    PipesModule,
    MatTooltipModule,
  ]
})
export class DetailedViewModule { }

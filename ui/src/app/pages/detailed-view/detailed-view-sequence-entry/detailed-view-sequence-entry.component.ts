// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Input } from '@angular/core';
import { SequenceDescription } from 'src/app/interfaces/shipments/sequence-description.enum';
import { TimeSequence } from 'src/app/interfaces/shipments/time-sequence';

/**
 * Displays an entry of a timesequence.
 *
 * @author Marc Dickmann
 */
@Component({
  selector: 'app-detailed-view-sequence-entry',
  templateUrl: './detailed-view-sequence-entry.component.html',
  styleUrls: ['./detailed-view-sequence-entry.component.scss']
})
export class DetailedViewSequenceEntryComponent {
  /**
   * The {@link TimeSequence} to display.
   */
  @Input()
  entry: TimeSequence;

  /**
   * Maps the CSS icon classes to {@link SequenceDescription}.
   *
   * @param sequenceDescription The value to map (default: the description of this entry)
   * @returns The CSS class for the icon
   */
  getIconClass(sequenceDescription: SequenceDescription = this.entry?.sequenceDescription): string {
    switch(sequenceDescription) {
      case SequenceDescription.CUSTOMS:
        return 'icon-customs';
      case SequenceDescription.DELIVERY_ADDRESS:
        return 'icon-delivery-address';
      case SequenceDescription.GROUND_HANDLER:
        return 'icon-ground-handler';
      case SequenceDescription.IMPORT_WAREHOUSE:
        return 'icon-import-warehouse';
      case SequenceDescription.OFF_BLOCKS:
        return 'icon-off-blocks';
      case SequenceDescription.ON_BLOCKS:
        return 'icon-on-blocks';
      case SequenceDescription.PICK_UP:
        return 'icon-pick-up';
      case SequenceDescription.RELEASES:
        return 'icon-releases';
      case SequenceDescription.EXPORT_WAREHOUSE:
      default:
        return 'icon-export-warehouse';
    }
  }

  /**
   * Maps the {@link SequenceDescription} to a human readable version.
   *
   * @param sequenceDescription The value to map (default: the description of this entry)
   * @returns A human legible version of the {@link SequenceDescription}
   */
   getHumanDescription(sequenceDescription: SequenceDescription = this.entry?.sequenceDescription): string {
    switch(sequenceDescription) {
      case SequenceDescription.CUSTOMS:
        return 'Customs';
      case SequenceDescription.DELIVERY_ADDRESS:
        return 'Delivery address';
      case SequenceDescription.EXPORT_WAREHOUSE:
        return 'Export warehouse';
      case SequenceDescription.GROUND_HANDLER:
        return 'Ground handler';
      case SequenceDescription.IMPORT_WAREHOUSE:
        return 'Import warehouse';
      case SequenceDescription.OFF_BLOCKS:
        return 'Off blocks';
      case SequenceDescription.ON_BLOCKS:
        return 'On blocks';
      case SequenceDescription.PICK_UP:
        return 'Pick up';
      case SequenceDescription.RELEASES:
        return 'Releases';
      default:
        return 'Unknown';
    }
  }
}

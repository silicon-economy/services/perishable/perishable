// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SequenceDescription } from 'src/app/interfaces/shipments/sequence-description.enum';
import { TimeSequence } from 'src/app/interfaces/shipments/time-sequence';

import { DetailedViewSequenceEntryComponent } from './detailed-view-sequence-entry.component';

const TIME_SEQUENCE: TimeSequence = {
  timeWithDescriptions: [
    {
      time: new Date('2019-01-04T08:50:24.514Z'),
      timeDescription: 'In',
    },
    {
      time: new Date('2019-01-04T09:50:24.514Z'),
      timeDescription: 'Out',
    },
  ],
  sequenceDescription: SequenceDescription.EXPORT_WAREHOUSE,
};

describe('DetailedViewSequenceEntryComponent', () => {
  let component: DetailedViewSequenceEntryComponent;
  let fixture: ComponentFixture<DetailedViewSequenceEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailedViewSequenceEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedViewSequenceEntryComponent);
    component = fixture.componentInstance;
    component.entry = TIME_SEQUENCE;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should map SequenceDescription to icon class', () => {
    // act & assert
    expect(component.getIconClass(SequenceDescription.CUSTOMS)).toEqual('icon-customs');
    expect(component.getIconClass(SequenceDescription.DELIVERY_ADDRESS)).toEqual('icon-delivery-address');
    expect(component.getIconClass(SequenceDescription.EXPORT_WAREHOUSE)).toEqual('icon-export-warehouse');
    expect(component.getIconClass(SequenceDescription.GROUND_HANDLER)).toEqual('icon-ground-handler');
    expect(component.getIconClass(SequenceDescription.IMPORT_WAREHOUSE)).toEqual('icon-import-warehouse');
    expect(component.getIconClass(SequenceDescription.OFF_BLOCKS)).toEqual('icon-off-blocks');
    expect(component.getIconClass(SequenceDescription.ON_BLOCKS)).toEqual('icon-on-blocks');
    expect(component.getIconClass(SequenceDescription.PICK_UP)).toEqual('icon-pick-up');
    expect(component.getIconClass(SequenceDescription.RELEASES)).toEqual('icon-releases');
  });

  it('should map SequenceDescription to icon class', () => {
    // act & assert
    expect(component.getHumanDescription(SequenceDescription.CUSTOMS)).toEqual('Customs');
    expect(component.getHumanDescription(SequenceDescription.DELIVERY_ADDRESS)).toEqual('Delivery address');
    expect(component.getHumanDescription(SequenceDescription.EXPORT_WAREHOUSE)).toEqual('Export warehouse');
    expect(component.getHumanDescription(SequenceDescription.GROUND_HANDLER)).toEqual('Ground handler');
    expect(component.getHumanDescription(SequenceDescription.IMPORT_WAREHOUSE)).toEqual('Import warehouse');
    expect(component.getHumanDescription(SequenceDescription.OFF_BLOCKS)).toEqual('Off blocks');
    expect(component.getHumanDescription(SequenceDescription.ON_BLOCKS)).toEqual('On blocks');
    expect(component.getHumanDescription(SequenceDescription.PICK_UP)).toEqual('Pick up');
    expect(component.getHumanDescription(SequenceDescription.RELEASES)).toEqual('Releases');
  });
});

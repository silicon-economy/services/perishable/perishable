// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * This Enum represents the states the closed dialog responses
 *
 * @author Anton Thomas Scholz
 */
 export enum State {
  SAVED,
  CANCLED,
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PieceDocumentsService } from 'src/app/services/api/piece-documents.service';
import { AssociatedDocument } from '../associated-document';
import { AssociatedPosition } from '../associated-position';
import { State } from './detailed-view-document-status-edit-state.enum';

/**
 * This component is the edit component the user opens in a dialog,
 * changed made here will be patched in the backend
 *
 * @author Anton Thomas Scholz
 */
@Component({
  selector: 'app-document-status-edit',
  templateUrl: './detailed-view-document-status-edit.component.html',
  styleUrls: ['./detailed-view-document-status-edit.component.scss']
})
export class DocumentStatusEditComponent {

  showAlertBox = false;

  constructor (
    private dialogRef: MatDialogRef<DocumentStatusEditComponent>,
    private pieceDocService: PieceDocumentsService,
    @Inject(MAT_DIALOG_DATA) public data: {doc: AssociatedDocument, positions: AssociatedPosition[], shipmentId: string}
  ) {}

  get mappedPositions() {
    return this.data.positions
      .filter(iter => this.data.doc.nos.includes(iter.no));
  }

  get positionsOptions() {
    return this.data.positions;
  }

  documentForm = new FormGroup({
    positions: new FormControl(this.mappedPositions),
    type: new FormControl(this.data.doc.documentType),
    state: new FormControl(this.data.doc.state),
    comment: new FormControl(this.data.doc.comment),
  });

  /**
   * when this function is called, the patch function of the piece-document service is called and the changes are submitted to the backend.
   * The state which is passed to the dialog close logic, triggers a refresh of the data, if changes where made.
   */
  onSubmitClicked(): void {
    this.data.doc.pieces = [];
    this.documentForm.get('positions').value.forEach((element: { id: string; }) => {
      this.data.doc.pieces.push(element.id);
    });

    this.data.doc.state = this.documentForm.get('state').value;
    this.data.doc.comment = this.documentForm.get('comment').value;

    this.pieceDocService.patchPieceDoc(this.data.doc, this.data.shipmentId).subscribe(data => {
      // It's saved correctly
      this.dialogRef.close(State.SAVED);
    }, error => {
      // In this case the conflict 409 status code means that the backend observed two data changes on the same data without saving the first.
      if (error.status === 409) {
        this.showAlertBox = true;
      }
    });
  }
}

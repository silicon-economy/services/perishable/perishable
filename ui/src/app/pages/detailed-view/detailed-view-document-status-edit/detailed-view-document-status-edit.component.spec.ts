// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { throwError } from 'rxjs';
import { PieceDocumentsService } from 'src/app/services/api/piece-documents.service';
import { environment } from 'src/environments/environment';

import { DocumentStatusEditComponent } from './detailed-view-document-status-edit.component';

const mockData = {
  doc: {
    nos: [1]
  },
  positions: [1],
  shipmentId: 'SHIPMENT_ID',
}

describe('DocumentStatusEditComponent', () => {
  let component: DocumentStatusEditComponent;
  let fixture: ComponentFixture<DocumentStatusEditComponent>;
  let httpClient: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentStatusEditComponent ],
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        ReactiveFormsModule,
        MatInputModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: mockData }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentStatusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.inject(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onSubmitClicked properly', () => {
    // arrange
    component.data.doc.pieces = ['1'];

    // act
    component.onSubmitClicked();

    // assert
    const req = httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/piece-document?shipmentId=SHIPMENT_ID`);
    expect(req.request.method).toEqual("PATCH");
    expect(req.request.params.get('shipmentId')).toEqual('SHIPMENT_ID');
    expect(component.data.doc.pieces).toEqual([]);
  });

  it('should throw error and change trigger variable', () => {
    // arrange
    const pieceDocService = TestBed.inject(PieceDocumentsService);
    const getPieceDocServiceSpy = spyOn(pieceDocService, 'patchPieceDoc').and.returnValue(throwError({status: 409}));

    // act
    component.onSubmitClicked();

    // assert
    expect(component).toBeTruthy();
    expect(getPieceDocServiceSpy).toHaveBeenCalled();
    expect(component.showAlertBox).toEqual(true);
  });

  it('should show the warning box', () => {
    // act
    component.showAlertBox = true;
    fixture.detectChanges();

    // assert
    const alertBoxElement = fixture.debugElement.query(By.css('.alert-box')).nativeElement;
    expect(alertBoxElement.innerText).toEqual('Current data is not the latest, please close the dialog to recieve the latest data!');
  });
});

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';

/**
 * This page lets the user edit the desired shipmentAgent.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
@Component({
  selector: 'app-detailed-view-address-edit',
  templateUrl: './detailed-view-address-edit.component.html',
  styleUrls: ['./detailed-view-address-edit.component.scss']
})
export class DetailedViewAddressEditComponent {
  constructor (
      private dialogRef: MatDialogRef<DetailedViewAddressEditComponent>,
      @Inject(MAT_DIALOG_DATA) private data: {awb: string, agent: ShipmentAgent},
      private shipmentService: ShipmentDetailsService,
    ) {}

  /**
   * Triggers the alertbox when data is changed in parallel
   */
  showAlertBox = false;

  agentForm = new FormGroup ({
    name: new FormControl(this.data.agent.name, Validators.required),
    street: new FormControl(this.data.agent.address.street),
    poBox: new FormControl(this.data.agent.address.poBox),
    postalCode: new FormControl(this.data.agent.address.postalCode),
    cityCode: new FormControl(this.data.agent.address.cityCode),
    cityName: new FormControl(this.data.agent.address.cityName),
    regionCode: new FormControl(this.data.agent.address.regionCode),
    regionName: new FormControl(this.data.agent.address.regionName),
    addressCodeType: new FormControl(this.data.agent.address.addressCodeType),
    addressCode: new FormControl(this.data.agent.address.addressCode),
    countryCode: new FormControl(this.data.agent.address.country.countryCode),
    countryName: new FormControl(this.data.agent.address.country.countryName),
  });

  get agent() {
    return this.data.agent;
  }

  get saveDisabled() {
    return !this.agentForm.valid || !this.agentForm.dirty;
  }

  /**
   * This function calls the ShipmentDetailsService to patch the changed agent in backend.
   */
  onSubmit(): void {
    this.data.agent.name = this.agentForm.get('name').value;
    this.data.agent.address.street = this.agentForm.get('street').value;
    this.data.agent.address.poBox = this.agentForm.get('poBox').value;
    this.data.agent.address.postalCode = this.agentForm.get('postalCode').value;
    this.data.agent.address.cityCode = this.agentForm.get('cityCode').value;
    this.data.agent.address.cityName = this.agentForm.get('cityName').value;
    this.data.agent.address.regionCode = this.agentForm.get('regionCode').value;
    this.data.agent.address.regionName = this.agentForm.get('regionName').value;
    this.data.agent.address.addressCodeType = this.agentForm.get('addressCodeType').value;
    this.data.agent.address.addressCode = this.agentForm.get('addressCode').value;
    this.data.agent.address.country.countryCode = this.agentForm.get('countryCode').value;
    this.data.agent.address.country.countryName = this.agentForm.get('countryName').value;

    this.shipmentService.putShipmentAgent(this.data.awb, this.data.agent).subscribe(data => {
      // It's saved correctly
      this.dialogRef.close("SAVED");
    }, error => {
      // In this case the conflict 409 status code means that the backend observed two data changes on the same data without saving the first.
      if (error.status === 409) {
        this.showAlertBox = true;
      }
    });

  }

}

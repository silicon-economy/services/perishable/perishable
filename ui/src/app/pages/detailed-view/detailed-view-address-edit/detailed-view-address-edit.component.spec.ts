// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ComponentFixture, TestBed } from '@angular/core/testing';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DetailedViewAddressEditComponent } from './detailed-view-address-edit.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { ShipmentDetailsService } from 'src/app/services/api/shipment-details.service';
import { throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('DetailedViewAddressEditComponent', () => {
  let component: DetailedViewAddressEditComponent;
  let fixture: ComponentFixture<DetailedViewAddressEditComponent>;
  let httpClient: HttpTestingController;

  const dummyAgent = {
    rev: 'ShipmentRev',
    role: 'tetsRole',
    name: 'testname',
    address: {
      uri: 'testUri',
      street: 'teststraße',
      poBox: 'testbox',
      postalCode: '45677',
      cityCode: 'testCode',
      cityName: 'testName',
      regionCode: 'testCode',
      regionName: 'testName',
      addressCodeType: 'testType',
      addressCode: 'testCode',
      country: {
        uri: 'testUri',
        countryCode: 'testcode',
        countryName: 'testName'
      }
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailedViewAddressEditComponent ],
      imports: [
        MatDialogModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {agent: dummyAgent} },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedViewAddressEditComponent);
    component = fixture.componentInstance;
    httpClient = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get agent correctly', () => {
    var value = component.agent;
    expect(value).toBe(dummyAgent);
  });

  it('should get saveDisabled correctly', () => {
    // act
    component.agentForm.patchValue({name: ''})
    component.agentForm.markAsDirty();

    // assert
    expect(component.saveDisabled).toBe(true);
  });

  it('should get saveDisabled correctly', () => {
    // act
    component.agentForm.patchValue({name: '2'})
    component.agentForm.markAsDirty();

    // assert
    expect(component.saveDisabled).toBe(false);
  });

  it('should show the warning box', () => {
    // act
    component.showAlertBox = true;
    fixture.detectChanges();

    // assert
    const alertBoxElement = fixture.debugElement.query(By.css('.alert-box')).nativeElement;
    expect(alertBoxElement.innerText).toEqual('Current data is not the latest, please close the dialog to recieve the latest data!');
  });

  it('should call onSubmit properly', () => {
    // act
    component.onSubmit();

    // assert
    const req = httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/undefined/shipmentAgent`);
    expect(req.request.method).toEqual("PUT");
  });

  it('should throw error and change trigger variable', () => {
    // arrange
    const shipmentService = TestBed.inject(ShipmentDetailsService);
    const getShipmentDetailsServiceSpy = spyOn(shipmentService, 'putShipmentAgent').and.returnValue(throwError({status: 409}));

    // act
    component.onSubmit();

    // assert
    expect(component).toBeTruthy();
    expect(getShipmentDetailsServiceSpy).toHaveBeenCalled();
    expect(component.showAlertBox).toEqual(true);
  });

});

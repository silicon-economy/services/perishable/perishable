// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ApprovalStatus } from 'src/app/interfaces/shipments/approval-status.enum';

/**
 * Contains information for authorities information of a certain type.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface AuthoritiesInfo {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The type of the authorities information.
   */
  type: string;

  /**
   * The time on which the information has been released.
   */
  releaseTime: Date;

  /**
   * The status of the approval.
   */
  approvalStatus: ApprovalStatus;

  /**
   * A reference to a {@link Piece}.
   */
  piece: string;
 }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Country } from 'src/app/interfaces/shipments/country';

/**
 * Represents an address as stored in the Database which is conform the schema of an address in ONE Record.
 *
 * @author Marc Dickmann
 */
export interface Address {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The street of the address.
   */
  street: string;

  /**
   * The postal box of the address.
   */
  poBox?: string;

  /**
   * The postal code of the address.
   */
  postalCode?: string;

  /**
   * The city code of the address.
   */
  cityCode?: string;

  /**
   * The city of the address.
   */
  cityName?: string;

  /**
   * The region code of the address.
   */
  regionCode?: string;

  /**
   * The region of the address.
   */
  regionName?: string;

  /**
   * The address code type.
   */
  addressCodeType?: string;

  /**
   * The address code type.
   */
   addressCode?: string;

  /**
   * The country.
   */
  country: Country;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Approval status:
 * - APPROVED     - All pieces have been approved
 * - NOT_APPROVED - A minimum of one piece has been disapproved
 * - IN_PROGRESS  - A minimum of one piece has been approved (the others have not been judged)
 * - NA           - All pieces do not possess a value
 */
export enum ApprovalStatus {
  APPROVED = 'APPROVED',
  NOT_APPROVED = 'NOT_APPROVED',
  IN_PROGRESS = 'IN_PROGRESS',
  NA = 'NA',
}

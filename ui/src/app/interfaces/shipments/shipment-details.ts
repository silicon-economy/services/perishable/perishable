// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { Value } from 'src/app/interfaces/shipments/value';
import { TimeSequence } from 'src/app/interfaces/shipments/time-sequence';
import { PieceDetails } from 'src/app/interfaces/shipments/piece-details';
import { ApprovalStatus } from 'src/app/interfaces/shipments/approval-status.enum';
import { PieceDocument } from 'src/app/interfaces/shipments/piece-document';
import { ShipmentDetailsValidationResult } from 'src/app/interfaces/shipments/shipment-details-validation-result';
import { ShipmentStatus } from './shipment-status.enum';

/**
 * This object represents a shipment in the details view of a shipment.
 *
 * @author Marc Dickmann
 */
export interface ShipmentDetails {
  /**
   * (Master) Air Waybill + External Reference (House Air Waybill - HAWB). One AWB can have multiple External References. This will be used as primary key for all shipments in the service.
   */
  id: string;

  /**
   * The revision of the shipment details.
   */
  rev: string;

  /**
   * Good type of this shipment.
   */
  goodType: string;

  /**
   * Origin Airport in three letter IATA Code.
   */
  org: string;

  /**
   * ULD identification code consisting of 10 digits (combination of letters and numbers). Not editable.
   */
  uldId: string;

  /**
   * Flight number. Taken from the last flight.
   */
  flightNo: string;

  /**
   * Scheduled Time of Departure of flight.
   */
  std: Date;

  /**
   * Scheduled Time of Departure of flight.
   */
  etd: Date;

  /**
   * Scheduled Time of arrival of flight.
   */
  sta: Date;

  /**
   * Estimated Time of arrival of flight.
   */
  eta: Date;

  /**
   * The status which is changed by clicking on the "export data for pre-delaration" button. It appears as a traffic light in the phyto column.
   */
  shipmentStatus: ShipmentStatus;

  /**
   * Times for the blue list in the frontend, namely for export warehouse, offblocks, onblocks, groundhandler,
   * importwarehouse, releases, customs, pickup and delivery address. Sometimes there are multiple times per
   * list (e.g. in and out of warehouse).
   */
  timeSequences: TimeSequence[];

  /**
   * Contains the information for consignor, consignee, carrier, exportAgent and place of destination.
   */
  shipmentAgents: ShipmentAgent[];

  /**
    *The goods that belong to the shipment.
    */
  positions: PieceDetails[];

  /**
   * Actual weight of the Shipment.
   */
  actualWeight: Value;

  /**
   * Documents that are attached to a shipment.
   */
  documents: PieceDocument[];

  /**
   * Result of validating the shipment details.
   */
  validationResult: ShipmentDetailsValidationResult;
}

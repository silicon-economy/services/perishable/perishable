// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Item } from "./item";

/**
 * An item of which pieces consist.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
export interface Product {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The commodity name of the product.
   */
  commodityName: string;

  /**
   * The hs code of the product.
   */
  hsCode: string;

  /**
   * The eppo code of the product.
   */
  eppoCode: string;

  /**
   * The items consisting of the product.
   */
  items: Item[];

  /**
   * A list of references to {@link Piece}s that are part of this product.
   */
  pieces: string[];
}

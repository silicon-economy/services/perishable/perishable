// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { TransportMovement } from "./transport-movement";

/**
 * Containing the movement times of a single transport movement
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface MovementTimes {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * Information on how the time has been calculated.
   */
  timeType: string;

  /**
   * The direction of the movement.
   */
  direction: string;

  /**
   * The time of the movement.
   */
  movementTimeStamp: Date;

  /**
   * Describing a special handling action for a piece.
   */
  transportMovement: TransportMovement;
 }

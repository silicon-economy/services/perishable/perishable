// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Country } from './country';
import { Product } from './product';
import { Value } from './value';

/**
 * An item of which pieces consist.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
export interface Item {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The quantity of the items inside the product.
   */
  quantity: Value;

  /**
   * The weight of the item.
   */
  weight: Value;

  /**
   * Information of a country.
   */
  productionCountry: Country;

  /**
   * Describing a single product.
   */
  product: Product;

  /**
   * List of references to {@link Piece}s contained inside the {@link Item}.
   */
  pieces: string[];
}

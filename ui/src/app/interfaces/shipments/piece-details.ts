// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { PieceDetailsValidationResult } from 'src/app/interfaces/shipments/piece-details-validation-result';
import { TemperatureHandling } from 'src/app/interfaces/shipments/temperature-handling.enum';
import { Value } from 'src/app/interfaces/shipments/value';

/**
 * A position of goods in a shipment. Used for ShipmentDetails.
 *
 * @author Marc Dickmann
 */
export interface PieceDetails {
  /**
   * Identifier.
   */
  id: string;

  /**
   * ULD identification code consisting of 10 digits (combination of letters and numbers). Not editable.
   */
  uldId: string;

  /**
   * The hs code. Taken from product.
   */
  hsCode: string;

  /**
   * The eppo code. Taken from product.
   */
  eppoCode: string;

  /**
   * The type of the goods. Taken from goodDescription. When they are more than one, use MIXED.
   */
  goodType: string;

  /**
   * The production country. (Country ISO Code) Taken from piece.
   */
  country: string;

  /**
   * The packing which with the goods have been packed. Taken from PackagingType.
   */
  packaging: string;

  /**
   * The packaging code for the packaging. Taken from PackagingType.
   */
  packagingCode: string;

  /**
   * Temperature handling the shipment is supposed to keep. Taken from SpecialHandling. Not editable.
   */
  plannedTemp: TemperatureHandling;

  /**
   * Code for the type of cargo. Taken from SpecialHandling.
   */
  group: string;

  /**
   * Weight of the piece.
   */
  weight: Value;

  /**
   * Quantity of the piece.
   */
  quantity: Value;

  /**
   * Result of validating piece details.
   */
  validationResult: PieceDetailsValidationResult;
}

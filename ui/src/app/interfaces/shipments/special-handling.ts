// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * This object represents a good in the details view of a shipment.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface SpecialHandling {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * Describes the special handling code.
   */
  code: string;

  /**
   * A reference to a {@link Piece}.
   */
  piece: string;
 }

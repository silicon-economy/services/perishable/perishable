// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Value } from './value';
import { Location } from './location';
import { Company } from './company';
import { Party } from './party';
import { BookingOptionRequest } from './booking-option-request';

/**
 * This object represents a shipment in the details view of a shipment.
 *
 * @author Marc Dickmann
 */
export interface Shipment {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * Describes the goods transported by the shipment.
   */
  goodsDescription: string;

  /**
   * Declared weight of the shipment.
   */
  totalGrossWeight: Value;

  /**
   * Date of delivery.
   */
  deliveryDate: Date;

  /**
   * Location of delivery.
   */
  deliveryLocation: Location;

  /**
   * Shipper company.
   */
  shipper: Company;

  /**
   * Forwarder company.
   */
  forwarder: Company;

  /**
   * Further companies along with their roles during the shipment.
   */
  parties: Party[];

  /**
   * The booking option requests for the shipment leading to the airwaybills.
   */
  bookingOptionRequests: BookingOptionRequest[];

  /**
   * References to the URIs of pieces.
   */
  pieces: string[];
}

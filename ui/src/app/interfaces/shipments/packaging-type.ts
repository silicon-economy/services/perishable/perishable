// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Contains the type of the packaging of a piece.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface PackagingType {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The code of the packaging type.
   */
  typeCode: string;

  /**
   * The description of the packaging type.
   */
  packagingTypeDescription: string;

  /**
   * A reference to a {@link Piece}.
   */
  piece: string;
 }

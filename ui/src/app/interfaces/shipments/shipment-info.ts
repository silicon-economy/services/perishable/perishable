// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ApprovalStatus } from 'src/app/interfaces/shipments/approval-status.enum';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { Value } from 'src/app/interfaces/shipments/value';
import { ShipmentStatus } from './shipment-status.enum';

/**
 * This object represents a shipment in the Perishable service.
 *
 * @author Marc Dickmann
 */
export interface ShipmentInfo {
  /**
   * (Master) Air Waybill + External Reference (House Air Waybill - HAWB). One AWB can have multiple External References. This will be used as primary key for all shipments in the service.
   */
  id: string;

  /**
   * Good type of this shipment.
   */
  goodType: string;

  /**
  * Actual weight of the shipment. (Floating number)
   */
  actualWeight: Value;

  /**
   * Amount of ulds inside the shipment.
   */
  ulds: number;

  /**
   * Origin Airport in three letter IATA Code.
   */
  org: string;

  /**
   * Date and time of delivery.
   */
  deliveryDate: Date;

  /**
   * Date and time of pickup.
   */
  pickUpDate: Date;

  /**
   * Flight number.
   */
  flightNo: string;

  /**
   * Scheduled Time of arrival of flight. (Datetime)
   */
  sta: Date;

  /**
   * Estimated Time of arrival of flight. (Datetime)
   */
  eta: Date;

  /**
   * The shipper.
   */
  shipper: ShipmentAgent;

  /**
   * The delivery location.
   */
  deliveryLocation: ShipmentAgent;

  /**
   * The status which is changed by clicking on the "send pre-declaration" button.
   */
  shipmentStatus: ShipmentStatus;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ULD } from 'src/app/interfaces/shipments/uld';
import { MovementTimes } from "./movement-times";
import { TransportMeans } from "./transport-means";

/**
 * Describing a special handling action for a piece.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface TransportMovement {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The location of the departure.
   */
  departureLocation: string;

   /**
   * The location of the arrival.
   */
  arrivalLocation: string;

  /**
   * The MovementTimes belonging to the transport movement.
   */
  movementTimes: MovementTimes[];

   /**
   * The means of a certain transport movement.
   */
  transportMeans: TransportMeans;

  /**
   * The official identifier for the transport movement.
   */
  transportIdentifier: string;

   /**
   * The ULDs.
   */
  ulds: ULD[];

  /**
   * A list of references to {@link Piece}s belonging to the transport movement.
   */
  pieces: string[];
 }

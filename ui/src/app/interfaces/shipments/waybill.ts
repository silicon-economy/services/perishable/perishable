// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { WaybillType } from "./waybill-type.enum";

/**
 * Containing information for the airwaybill. There can be master- and house airwaybills. a houseairwaybill can belong to a
 * single masterairwaybill. A masterairwaybill can contain multiple houseairwaybills.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface Waybill {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The number for the waybill.
   */
  waybillNumber: number;

  /**
   * The prefix of the waybill.
   */
  waybillPrefix: number;

  /**
   * The type of the waybill.
   */
  waybillType: WaybillType;

  /**
   * Links to the Master airwaybill (for house airwaybills).
   */
  linkToMaster: string;

  /**
   * Links to the house airwaybills (for master airwaybills).
   */
  linksToHouses: string[];
 }

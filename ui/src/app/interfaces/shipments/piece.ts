// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Item } from './item';
import { PackagingType } from './packaging-type';
import { SpecialHandling } from './special-handling';
import { Event } from "./event";
import { TransportMovement } from './transport-movement';
import { AuthoritiesInfo } from './authorities-info';
import { ExternalReference } from './external-reference';
import { ULD } from 'src/app/interfaces/shipments/uld';

/**
 * This object represents a good in the details view of a shipment.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
export interface Piece {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The UP ID.
   */
  upid: string;

  /**
   * Further documents which belong to the piece.
   */
  externalReferences: ExternalReference[];

  /**
   * Items which are contained in the piece.
   */
  containedItems: Item[];

  /**
   * Special handling instructions for the piece.
   */
  specialHandlings: SpecialHandling[];

  /**
   * Contains the type of the packaging of a piece.
   */
  packagingType: PackagingType;

  /**
   * Transport movements for the piece.
   */
  transportMovements: TransportMovement[];

  /**
   * Events for the piece (e.g. ready for pickup time).
   */
  events: Event[];

  /**
   * ULDs of the piece.
   */
  ulds: ULD[];

  /**
   * AuthoritiesInfos (e.g. phytosanitaryRelease) of the piece.
   */
  authoritiesInfos: AuthoritiesInfo[];

  /**
   * References to the URIs of the shipments containing the piece.
   */
  shipments: string[];

  /**
   * References to the URIs of pieces contained by the piece.
   */
  pieces: string[];

}

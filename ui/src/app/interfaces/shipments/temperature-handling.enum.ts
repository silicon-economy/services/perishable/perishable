// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Handling Procedures to keep a certain temperature:
 * - COL - Cool store, temperature between +2 to ü8 degrees
 * - FRI - frozen store, subject to veterinarian / phyto inspection
 * - FRO - frozen store, temperature between -5 to +15 degrees
 * - CRT - controlled room temperature, temperature between +15 to +25 degrees
 * - ERT - extended room temperature, temperature between +2 to +25 degrees
 * - ACT - active temperature controlled system
 */
export enum TemperatureHandling {
  COL = 'COL',
  FRI = 'FRI',
  FRO = 'FRO',
  CRT = 'CRT',
  ERT = 'ERT',
  ACT = 'ACT',
}

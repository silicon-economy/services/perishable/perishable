// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * A value for a certain measurement.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
export interface Value {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The unit of the value.
   */
  unit: string;

  /**
   * The amount of units.
   */
  amount: number;
}

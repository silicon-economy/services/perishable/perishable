// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { BookingOption } from "./booking-option";

/**
 * Linking to a booking option.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface BookingOptionRequest {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The booking options.
   */
  bookingOptions: BookingOption[];

  /**
   * A reference to an {@link Shipment} transporting certain goods.
   */
  shipment: string;
 }

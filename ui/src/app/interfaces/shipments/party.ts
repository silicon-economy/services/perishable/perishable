// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Company } from "./company";

/**
 * A company which has a certain role during a shipment.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface Party {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The role of the company during the shipment.
   */
  partyRole: string;

  /**
   * Contains information about a company.
   */
  partyDetails: Company;
 }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * The state of a PieceDocument validation. Extends the enum ValidationState.
 * Its unchecked whenever the document state is unset.
 *
 * @author Marc Dickmann
 */
 export enum PieceDocumentValidationState {
  VALID = 'VALID',
  INCORRECT = 'INCORRECT',
  MISSING = 'MISSING',
  UNCHECKED = 'UNCHECKED',
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Information of a country.
 *
 * @author Marc Dickmann
 */
export interface Country {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The code of the country.
   */
  countryCode: string;

  /**
   * The name of the country.
   */
  countryName: string;
}

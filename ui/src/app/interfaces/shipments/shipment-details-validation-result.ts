// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { PieceDocumentValidationState } from 'src/app/interfaces/shipments/piece-document-validation-state.enum';
import { ValidationState } from 'src/app/interfaces/shipments/validation-state.enum';

/**
 * Container for a multitude of validation results of an ShipmentDetails object.
 *
 * @author Marc Dickmann
 */
export interface ShipmentDetailsValidationResult {
  /**
   * State of the consignor agent.
   */
  consignor: ValidationState;

  /**
   * State of the consignee agent.
   */
  consignee: ValidationState;

  /**
   * State of the place of destination agent.
   */
  placeOfDestination: ValidationState;

  /**
   * State of the phytho certificate document.
   */
  phytoCertificate: PieceDocumentValidationState;

  /**
   * State of the MAWB document.
   */
  mawb: PieceDocumentValidationState;
}

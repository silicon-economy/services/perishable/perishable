// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * Further documents which belong to the piece.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface ExternalReference {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The name describing the document.
   */
  documentName: string;

  /**
   *The link to the document.
   */
  documentLink: string;
 }

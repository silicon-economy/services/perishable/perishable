// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Company } from "./company";

/**
 * Describing an event during the transport of a piece.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface Event {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * Describes the type of the event.
   */
  eventTypeIndicator: string;

   /**
   * The date and time of the certain event.
   */
  dateTime: Date;

  /**
   * Contains information about a company.
   */
  performedBy: Company;
 }

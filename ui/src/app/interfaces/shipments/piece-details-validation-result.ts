// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ValidationState } from 'src/app/interfaces/shipments/validation-state.enum';

/**
 * Container for a multitude of validation results of an PieceDetails object (aka position).
 *
 * @author Marc Dickmann
 */
export interface PieceDetailsValidationResult {
  /**
   * Validity state of hs code.
   */
  hsCode: ValidationState;

  /**
   * Validity state of eppo code.
   */
  eppoCode: ValidationState;

  /**
   * Validity state of country of origin.
   */
  countryOfOrigin: ValidationState;

  /**
   * Validity state of quantity.
   */
  quantity: ValidationState;

  /**
   * Validity state of weight.
   */
  weight: ValidationState;
}

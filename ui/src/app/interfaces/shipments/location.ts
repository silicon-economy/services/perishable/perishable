// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Address } from "./address";

/**
 * A certain location.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface Location {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The name of the location.
   */
  name?: string;

  /**
   * Describing a special handling action for a piece.
   */
  address: Address;
 }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * A time stamp during a shipment with a belonging description.
 *
 * @author Marc Dickmann
 */
export interface TimeWithDescription {
  /**
   * The time.
   */
  time: Date;

  /**
   * The description for the meaning of the time.
   */
  timeDescription: string;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * The status which is changed by clicking on the "send pre-declaration" button. It appears as a traffic light in the phyto column. Shipment status:
 *  - INCOMPLETE  - The shipment is new or incomplete. The color is grey.
 *  - COMPLETED   - The shipment has all information that is needed for the pre declaration. The color is yellow.
 *  - EXPORTED    - The pre declaration has been sent (by clicking on the button "send pre declaration"). The color is green.
 *
 * @author Anton Thomas Scholz
 */
 export enum ShipmentStatus {
  INCOMPLETE = 'INCOMPLETE',
  COMPLETED = 'COMPLETED',
  EXPORTED = 'EXPORTED',
 }

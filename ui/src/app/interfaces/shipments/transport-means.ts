// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { TransportMovement } from "./transport-movement";

/**
 * The means of a certain transport movement.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface TransportMeans {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The type of the vehicle or location.
   */
  vehicleType: string;

  /**
   * The action that has been taken.
   */
  transportMovement: TransportMovement;
 }

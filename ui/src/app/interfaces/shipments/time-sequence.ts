// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { SequenceDescription } from 'src/app/interfaces/shipments/sequence-description.enum';
import { TimeWithDescription } from 'src/app/interfaces/shipments/time-with-description';

/**
 * A time sequence during a shipment. It contains a list of times with their descriptions (the sequence) and the belonging description.
 *
 * @author Marc Dickmann
 */
export interface TimeSequence {
  /**
   * List of times with their description attached.
   */
  timeWithDescriptions: TimeWithDescription[];

  /**
   * The description for the meaning of the time sequence.
   */
  sequenceDescription: SequenceDescription;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ExternalReference } from 'src/app/interfaces/shipments/external-reference';
import { PieceDocumentState } from './piece-document-state.enum';
import { PieceDocumentType } from './piece-document-type.enum';

/**
 * This contains the reference to a document and its belonging piece along with a comment.
 *
 * @author Marc Dickmann, Anton Thomas Scholz
 */
export interface PieceDocument {
  /**
   * An internal identifier that is used by the database.
   */
  id: string;

  /**
   * The revision of the piece-document.
   */
  rev: string;

  /**
   * Ids referencing the pieces.
   */
  pieces: string[];

  /**
   * Actual document data.
   */
  ref: ExternalReference;

  /**
   * A user comment for this document.
   */
  comment: string;

  /**
   * The internal type of an PieceDocument.
   */
  documentType: PieceDocumentType;

  /**
   * The internal state of an PieceDocument.
   */
  state: PieceDocumentState;
}

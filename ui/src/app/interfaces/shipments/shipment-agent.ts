// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Address } from 'src/app/interfaces/shipments/address';

/**
 * Represents an agent in the shipping process. A shipping process usually goes through several intermediate stations.
 * A shipment agent is someone who works at an intermediate station in the shipping process. A shipment agent can be,
 * for example a consignor, an export agent, a carrier etc.
 *
 * @author Marc Dickmann
 */
 export interface ShipmentAgent {
  /**
   * The revision of the shipment agent.
   */
  rev: string;

  /**
   * The role of the company during the shipment.
   */
  role: string;

  /**
   * Name of Company or Person.
   */
  name: string;

  /**
   * Address.
   */
  address: Address;
}

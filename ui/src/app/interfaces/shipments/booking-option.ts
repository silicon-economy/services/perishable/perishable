// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { BookingOptionRequest } from "./booking-option-request";
import { Company } from "./company";
import { Waybill } from "./waybill";

/**
 * Linking to a booking option.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface BookingOption {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * Contains information about a company.
   */
  company: Company;

   /**
   * Containing information for the airwaybill. There can be master- and house airwaybills. a houseairwaybill can belong to
   * a single masterairwaybill. A masterairwaybill can contain multiple houseairwaybills.
   */
  waybill: Waybill;

  /**
   * Linking to a booking option.
   */
  bookingOptionRequest: BookingOptionRequest;
 }

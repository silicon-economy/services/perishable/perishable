// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Event } from "./event";
import { TransportMovement } from "./transport-movement";

/**
 * Containing the ULD id.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface ULD {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The serial ULD id.
   */
  serialNumber: string;

  /**
   * Links to a TransportMovement which can contain the movement times for std, etd, offblocks, onblocks, sta, eta.
   */
  transportMovements: TransportMovement[];

  /**
   * Links to the events belonging to the ULD.
   */
  events: Event[];

  /**
   * A list of references to the {@link Piece}s.
   */
  pieces: string[];
 }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

/**
 * The state of a validation. Its incorrect whenever the value is given but
 * its wrong on a technical level. If the value is missing its declared as missing.
 *
 * @author Marc Dickmann
 */
export enum ValidationState {
  VALID = 'VALID',
  INCORRECT = 'INCORRECT',
  MISSING = 'MISSING',
}

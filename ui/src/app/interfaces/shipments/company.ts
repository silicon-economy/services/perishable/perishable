// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { CompanyBranch } from "./company-branch";

/**
 * Contains information about a company.
 *
 * @author Anton Thomas Scholz, Marc Dickmann
 */
 export interface Company {
  /**
   * The Uniform Resource Identifier. This can be used for linking other objects to this one.
   */
  uri: string;

  /**
   * The name of the company.
   */
  companyName: string;

  /**
   * The branch name and location for a company.
   */
  companyBranch: CompanyBranch;
 }

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SidenavService } from 'src/app/services/state/sidenav.service';
import { ThemeService } from 'src/app/services/state/theme.service';
import { AppComponent } from './app.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';

class FakeThemeService {
  toggleMode(): void {}
  getNextModeIcon(): string { return 'dark_mode'; }
  getModeStr(): string { return 'light-theme'; }
  getModeObj() { return { name: 'light-theme', icon: 'light_mode' }; }
  getNextModeObj() { return { name: 'dark-theme', icon: 'dark_mode' }; }
}

class FakeSidenavService {
  toggleSidenavState() {}
  getSidenavState(): boolean { return true; }
}

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        BrowserAnimationsModule,
        MatSidenavModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: ThemeService, useClass: FakeThemeService },
        { provide: SidenavService, useClass: FakeSidenavService },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should toggle the sidenav state', () => {
    // arrange
    app.breakpointStateLtMd = false;

    const sidenavService = TestBed.inject(SidenavService);
    const toggleSidenavStateSpy = spyOn(sidenavService, 'toggleSidenavState');

    // act
    app.sidenavStateToggle();

    // expect
    expect(toggleSidenavStateSpy).toHaveBeenCalled();
  });

  it('should toggle the blue themes mode', () => {
    // arrange
    const addSpy = spyOn(document.body.classList, 'add');
    const removeSpy = spyOn(document.body.classList, 'remove');

    const themeService = TestBed.inject(ThemeService);
    const toggleModeSpy = spyOn(themeService, 'toggleMode');

    // act
    app.toggleThemeMode();

    // assert
    expect(addSpy).toHaveBeenCalledWith('blue-dark-theme');
    expect(removeSpy).toHaveBeenCalledWith('blue-light-theme');
    expect(app.themeModeIcon).toEqual('dark_mode');
  });

});

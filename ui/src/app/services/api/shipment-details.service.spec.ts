// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ApprovalStatus } from 'src/app/interfaces/shipments/approval-status.enum';
import { PieceDocumentState } from 'src/app/interfaces/shipments/piece-document-state.enum';
import { PieceDocumentType } from 'src/app/interfaces/shipments/piece-document-type.enum';
import { PieceDocumentValidationState } from 'src/app/interfaces/shipments/piece-document-validation-state.enum';
import { SequenceDescription } from 'src/app/interfaces/shipments/sequence-description.enum';
import { ShipmentDetails } from 'src/app/interfaces/shipments/shipment-details';
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';
import { TemperatureHandling } from 'src/app/interfaces/shipments/temperature-handling.enum';
import { ValidationState } from 'src/app/interfaces/shipments/validation-state.enum';
import { environment } from 'src/environments/environment';

import { ShipmentDetailsService } from './shipment-details.service';

const SHIPMENT_RESPONSE: ShipmentDetails = {
  shipmentStatus: ShipmentStatus.COMPLETED,
  id: 'AWB',
  rev: 'ShipmentRev',
  goodType: 'Roses',
  org: 'ADD',
  uldId: 'ULDID12345',
  flightNo: '999 Banane',
  std: new Date('2019-01-04T08:50:24.514Z'),
  etd: new Date('2019-01-04T08:50:24.514Z'),
  sta: new Date('2019-01-04T08:50:24.514Z'),
  eta: new Date('2019-01-04T08:50:24.514Z'),
  timeSequences: [
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T08:50:24.514Z'),
          timeDescription: 'In',
        },
        {
          time: new Date('2019-01-04T09:50:24.514Z'),
          timeDescription: 'Out',
        },
      ],
      sequenceDescription: SequenceDescription.EXPORT_WAREHOUSE,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T10:50:24.514Z'),
          timeDescription: '',
        },
      ],
      sequenceDescription: SequenceDescription.OFF_BLOCKS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T11:50:24.514Z'),
          timeDescription: '',
        },
      ],
      sequenceDescription: SequenceDescription.ON_BLOCKS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T12:50:24.514Z'),
          timeDescription: 'Received',
        },
        {
          time: new Date('2019-01-04T13:50:24.514Z'),
          timeDescription: 'Delivered',
        },
      ],
      sequenceDescription: SequenceDescription.GROUND_HANDLER,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T14:50:24.514Z'),
          timeDescription: 'Acceptance',
        },
        {
          time: new Date('2019-01-04T15:50:24.514Z'),
          timeDescription: 'Check time',
        },
        {
          time: new Date('2019-01-04T16:50:24.514Z'),
          timeDescription: 'Ready for inspection',
        },
      ],
      sequenceDescription: SequenceDescription.IMPORT_WAREHOUSE,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T17:50:24.514Z'),
          timeDescription: 'Phytosanitarian release',
        },
        {
          time: new Date('2019-01-04T18:00:24.514Z'),
          timeDescription: 'Veterinarian release',
        },
        {
          time: new Date('2019-01-04T18:50:24.514Z'),
          timeDescription: 'BLE release',
        },
      ],
      sequenceDescription: SequenceDescription.RELEASES,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T19:50:24.514Z'),
          timeDescription: 'Release',
        },
      ],
      sequenceDescription: SequenceDescription.CUSTOMS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T20:00:24.514Z'),
          timeDescription: 'Ready',
        },
        {
          time: new Date('2019-01-04T21:00:24.514Z'),
          timeDescription: 'Arrival',
        },
        {
          time: new Date('2019-01-04T22:00:24.514Z'),
          timeDescription: 'Departure',
        },
      ],
      sequenceDescription: SequenceDescription.PICK_UP,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T23:50:24.514Z'),
          timeDescription: 'Arrival',
        },
      ],
      sequenceDescription: SequenceDescription.DELIVERY_ADDRESS,
    },
    {
      timeWithDescriptions: [
        {
          time: new Date('2019-01-04T23:50:24.514Z'),
          timeDescription: 'Arrival',
        },
      ],
      sequenceDescription: SequenceDescription.DELIVERY_ADDRESS,
    },
  ],
  shipmentAgents: [
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'consignor',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'consignee',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'carrier',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'export_agent',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityCode: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    },
    {
      rev: 'ShipmentRev',
      name: 'testNAME',
      role: 'place_of_destination',
      address: {
        uri: 'URI',
        street: 'Wonderstreet',
        cityName: 'Dortmund',
        country: {
          uri: 'URI',
          countryName: 'Magic Land',
          countryCode: 'test',
        }
      }
    }
  ],
  positions: [
    {
      id: 'PIECE_ID',
      uldId: 'ULD-ID',
      hsCode: 'HS-CODE',
      eppoCode: 'EPPO-CODE',
      goodType: 'Roses',
      country: 'DE',
      packaging: 'pallet',
      packagingCode: 'pallet-code',
      plannedTemp: TemperatureHandling.FRO,
      group: 'GROUP',
      weight: {
        uri: 'URI',
        amount: 105.0,
        unit: 'kg',
      },
      quantity: {
        uri: 'URI',
        amount: 1234,
        unit: 'stk',
      },
      validationResult: {
        hsCode: ValidationState.INCORRECT,
        eppoCode: ValidationState.VALID,
        countryOfOrigin: ValidationState.VALID,
        quantity: ValidationState.MISSING,
        weight: ValidationState.VALID,
      }
    },
  ],
  actualWeight: {
    uri: 'URI',
    amount: 105.0,
    unit: 'kg',
  },
  documents: [
    {
      id: 'testID',
      rev: 'testREV',
      pieces: [
        'PIECE_ID',
      ],
      ref: {
        uri: 'URI',
        documentName: 'TestDokument.pdf',
        documentLink: 'LINK TO DOCUMENT',
      },
      comment: 'Just a comment.',
      documentType: PieceDocumentType.CATCH_CERTIFICATE,
      state: PieceDocumentState.INCORRECT
    }
  ],
  validationResult: {
    consignor: ValidationState.INCORRECT,
    consignee: ValidationState.MISSING,
    placeOfDestination: ValidationState.VALID,
    phytoCertificate: PieceDocumentValidationState.INCORRECT,
    mawb: PieceDocumentValidationState.UNCHECKED,
  }
};

describe('ShipmentDetailsService', () => {
  let service: ShipmentDetailsService;
  let httpClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ShipmentDetailsService);
    httpClient = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ShipmentDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get one shipment', () => {
    // act
    service.getShipmentDetails('testId',).subscribe(data => {
      expect(data).toEqual(SHIPMENT_RESPONSE)
    })

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/testId`).event(
      new HttpResponse<{}>({ body: SHIPMENT_RESPONSE})
    );
  });

  it('should get the predeclaration of a shipment', () => {
    // arrange
    const expectedElement = new Blob();

    // act
    service.getPredeclaration('testAWB').subscribe(data => {
      expect(data).toEqual(expectedElement)
    })

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/testAWB/predeclaration`).event(
      new HttpResponse<{}>({ body: expectedElement})
    );
  });

  it('should put the given PieceDetails', () => {
    // arrange
    const expectedElement = SHIPMENT_RESPONSE.positions[0];

    // act
    service.putPieceDetails('PIECE_ID', expectedElement, 'ShipmentRev').subscribe(data => {
      expect(data).toEqual(expectedElement);
    });

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/PIECE_ID/ShipmentRev/pieceDetails`).event(
      new HttpResponse<{}>({ body: expectedElement})
    );
  });

  it('should put the given ShipmentAgent', () => {
    // arrange
    const expectedElement = SHIPMENT_RESPONSE.shipmentAgents[0];

    // act
    service.putShipmentAgent('testId', expectedElement).subscribe(data => {
      expect(data).toEqual(expectedElement)
    })

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails/testId/shipmentAgent`).event(
      new HttpResponse<{}>({ body: expectedElement})
    );
  });
});

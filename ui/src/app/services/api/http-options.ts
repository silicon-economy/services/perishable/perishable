// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpHeaders } from '@angular/common/http';

/**
 * Allows for a centralized way of controlling the options used by HTTP-Requests.
 *
 * @returns HttpOptions that enable json based requests.
 * @author Marc Dickmann
 */
export function createHttpOptions() {
  return {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'charset': 'UTF-8' }),
  };
}

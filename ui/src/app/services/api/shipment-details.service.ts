// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { ShipmentDetails } from 'src/app/interfaces/shipments/shipment-details';
import { createHttpOptions } from 'src/app/services/api/http-options';
import { environment } from 'src/environments/environment';
import { PieceDetails } from "../../interfaces/shipments/piece-details";

/**
 * REST-Service thats provides the ability to interact with the ShipmentDetails API.
 *
 * @author Marc Dickmann
 */
@Injectable({
  providedIn: 'root'
})
export class ShipmentDetailsService {
  private apiURL = `${environment.apiHost}:${environment.apiPort}/api/v1/shipmentDetails`;
  private httpOptions = createHttpOptions();

  constructor(
    // Angular
    private http: HttpClient,
  ) { }

  /**
   * Fetches a specific shipment by awb and ref.
   *
   * @param id The AWB of the shipment to fetch
   * @returns An observable that contains the desired Shipment
   */
  getShipmentDetails(id: string): Observable<ShipmentDetails> {
    const url = `${ this.apiURL}/${ id }`;
    return this.http.get<ShipmentDetails>(url, this.httpOptions);
  }

  /**
   * Fetches the latest predeclaration document of a shipment from the server.
   *
   * @param id The AWB of the shipment to fetch the predeclaration for
   * @returns An {@link Blob} that contains the predeclaration document
   */
  getPredeclaration(awb: string): Observable<Blob> {
    const url = `${ this.apiURL}/${ awb }/predeclaration`;
    return this.http.get(url, {
      headers: new HttpHeaders({ 'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'}),
      responseType: 'blob',
    });
  }

  /**
   * Updates a PieceDetails of a given shipment.
   * <p>
   *  If the shipment with the provided id does not exist
   *  in the DB, then it will create a new one. Otherwise
   *  an existing shipment will be updated.
   *
   * @param id the shipment id to update a piece detail
   * @param pieceDetail the piece details to update
   * @param shipmentRev the revision of the shipment
   */
  putPieceDetails(id: string, pieceDetail: PieceDetails, shipmentRev: string): Observable<PieceDetails> {
    const url = `${this.apiURL}/${id}/${shipmentRev}/pieceDetails`;
    return this.http.put<PieceDetails>(url, pieceDetail, this.httpOptions);
  }

  /**
   * Updates / Stores a given {@link ShipmentAgent} inside the DB.
   *
   * @param id The id of shipment to modify
   * @param agent An {@link ShipmentAgent} to store
   * @returns The stored {@link ShipmentAgent}
   */
  putShipmentAgent(id: string, agent: ShipmentAgent): Observable<ShipmentAgent> {
    const url = `${ this.apiURL}/${ id }/shipmentAgent`;
    return this.http.put<ShipmentAgent>(url, agent, this.httpOptions);
  }
}

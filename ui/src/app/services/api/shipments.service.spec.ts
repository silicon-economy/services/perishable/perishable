// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ShipmentsService } from './shipments.service';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { ShipmentInfo } from 'src/app/interfaces/shipments/shipment-info';
import { ShipmentAgent } from 'src/app/interfaces/shipments/shipment-agent';
import { ApprovalStatus } from 'src/app/interfaces/shipments/approval-status.enum';
import { ShipmentStatus } from 'src/app/interfaces/shipments/shipment-status.enum';

const SHIPMENT_INFO_RESPONSE: ShipmentInfo[] = [{
  id: 'AWB',
  goodType: 'Roses',
  actualWeight: {
    uri: 'URI',
    amount: 100.5,
    unit: 'kg',
  },
  ulds: 1,
  org: 'ADD',
  deliveryDate: new Date('2019-01-04T08:50:24.514Z'),
  pickUpDate: new Date('2019-01-04T08:50:24.514Z'),
  flightNo: '999 Banane',
  sta: new Date('2019-01-04T08:50:24.514Z'),
  eta: new Date('2019-01-04T08:50:24.514Z'),
  shipper: {
    role: 'Shipper',
    name: 'Banana in the Pyjama Limited',
    address: {
      uri: 'URI',
      street: 'Bananenstraße',
      poBox: '12',
      postalCode: '99699',
      cityCode: 'Hamburg',
      country: {
        uri: 'URI',
        countryCode: 'DE',
        countryName: 'Germany',
      },
    },
  } as ShipmentAgent,
  deliveryLocation: {
    role: 'Carrier',
    name: 'Würstchenmobil Incorporated',
    address: {
      uri: 'URI',
      street: 'An der Wurst',
      poBox: '12',
      postalCode: '99699',
      cityName: 'Hamburg',
      country: {
        uri: 'URI',
        countryCode: 'DE',
        countryName: 'Germany',
      },
    },
  } as ShipmentAgent,
  shipmentStatus: ShipmentStatus.INCOMPLETE,
}];

describe('ShipmentsService', () => {
  let service: ShipmentsService;
  let httpClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ShipmentsService);
    httpClient = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all shipments', () => {
    // act
    service.getShipments().subscribe(data => {
      expect(data).toEqual(SHIPMENT_INFO_RESPONSE)
    })

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/shipment`).event(
      new HttpResponse<{}>({ body: SHIPMENT_INFO_RESPONSE})
    );
  });

});

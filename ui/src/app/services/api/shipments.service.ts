// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { createHttpOptions } from 'src/app/services/api/http-options';
import { ShipmentInfo } from 'src/app/interfaces/shipments/shipment-info';

/**
 * REST-Service thats provides the ability to interact with the Shipments API.
 *
 * @author Marc Dickmann
 */
@Injectable({
  providedIn: 'root'
})
export class ShipmentsService {
  private apiURL = `${environment.apiHost}:${environment.apiPort}/api/v1/shipment`;
  private httpOptions = createHttpOptions();

  constructor(
    // Angular
    private http: HttpClient,
  ) { }

  /**
   * Fetches all all shipments stored in the DB.
   *
   * @returns An observable that contains a list of Shipments
   */
  getShipments(): Observable<ShipmentInfo[]> {
    const url = `${ this.apiURL }`;
    return this.http.get<ShipmentInfo[]>(url, this.httpOptions);
  }
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PieceDocument } from 'src/app/interfaces/shipments/piece-document';
import { environment } from 'src/environments/environment';
import { createHttpOptions } from './http-options';

@Injectable({
  providedIn: 'root'
})
export class PieceDocumentsService {

  private apiURL = `${environment.apiHost}:${environment.apiPort}/api/v1/piece-document`;

  private httpOptions = createHttpOptions();

  constructor(
    // Angular
    private http: HttpClient,
  ) { }

  /**
   * Over this endpoint, a document of an piece can be updated. The document is identified by the "documentLink" inside the "ref".
   *
   * @param doc the {@link PieceDocument} to update
   * @param shipmentId the shipment id where this {@link PieceDocument} belongs to
   * @returns An observable that contains the status response
   */
   patchPieceDoc(doc: PieceDocument, shipmentId: string): Observable<any> {
    const url = `${ this.apiURL}`;

    return this.http.patch(url, doc, {
      ...this.httpOptions,
      params: {
        shipmentId: shipmentId,
      }
    });
  }

}

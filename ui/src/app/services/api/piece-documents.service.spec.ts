// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { PieceDocumentState } from 'src/app/interfaces/shipments/piece-document-state.enum';
import { PieceDocumentType } from 'src/app/interfaces/shipments/piece-document-type.enum';
import { PieceDocument } from 'src/app/interfaces/shipments/piece-document';
import { environment } from 'src/environments/environment';

import { PieceDocumentsService } from './piece-documents.service';

const PIECEDOC: PieceDocument= {
  id: 'testID',
  rev: 'tetsREV',
  pieces: [
    'testPiece',
  ],
  ref: {
    uri: 'test',
    documentName: 'testName',
    documentLink: 'link'
  },
  documentType: PieceDocumentType.CERTIFICATE_OF_INSPECTION,
  state: PieceDocumentState.MISSING,
  comment: 'blabla',
}


describe('PieceDocumentsService', () => {
  let service: PieceDocumentsService;
  let httpClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(PieceDocumentsService);
    httpClient = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call patch service properly', () => {
    // act
    service.patchPieceDoc(PIECEDOC, 'SHIPMENT_ID').subscribe(data => {
      expect(data).toEqual('testResponse')
    })

    // assert
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/api/v1/piece-document?shipmentId=SHIPMENT_ID`).event(
      new HttpResponse<{}>({ body: 'testResponse'})
    );
  });

});

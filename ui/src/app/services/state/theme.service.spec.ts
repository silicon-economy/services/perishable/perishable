// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { TestBed } from '@angular/core/testing';

import { ThemeService } from './theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the light mode from localStorage when nothing is set', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue(null);

    // act & assert
    expect(service['modeState']).toBeFalse();
    expect(getItemSpy).toHaveBeenCalledWith('dark-mode-activated');
  });

  it('should return the dark mode when requested from localStorage', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('true');

    // act & assert
    expect(service['modeState']).toBeTrue();
    expect(getItemSpy).toHaveBeenCalledWith('dark-mode-activated');
  });

  it('should update the value inside the localStorage when updating the local variable', () => {
    // arrange
    const setItemSpy = spyOn(localStorage, 'setItem');

    // act
    service['modeState'] = true;

    // assert
    expect(setItemSpy).toHaveBeenCalledWith('dark-mode-activated', 'true');
  })

  it('should reset the mode to false (light-mode)', () => {
    // arrange
    const setItemSpy = spyOn(localStorage, 'setItem');

    // act
    service.resetMode();

    // assert
    expect(setItemSpy).toHaveBeenCalledWith('dark-mode-activated', 'false');
  });

  it('should return the string identifier from the current mode', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('true');

    // act & assert
    expect(service.getModeStr()).toEqual('dark-theme');
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('should return the object of the light mode when requesting the current mode object', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('false');

    // act
    const returnValue = service.getModeObj();

    // act & assert
    expect(returnValue.name).toEqual('light-theme');
    expect(returnValue.icon).toEqual('light_mode');
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('should return the object of the dark mode when requesting the current mode object', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('true');

    // act
    const returnValue = service.getModeObj();

    // act & assert
    expect(returnValue.name).toEqual('dark-theme');
    expect(returnValue.icon).toEqual('dark_mode');
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('should return the object of the dark mode when requesting the next mode object', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('false');

    // act
    const returnValue = service.getNextModeObj();

    // act & assert
    expect(returnValue.name).toEqual('dark-theme');
    expect(returnValue.icon).toEqual('dark_mode');
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('should return the icon of the next mode', () => {
    // arrange
    const lightMode = { name: 'light-theme', icon: 'light_mode' };
    const getNextModeObjSpy = spyOn(service, 'getNextModeObj').and.returnValue(lightMode);

    // act & assert
    expect(service.getNextModeIcon()).toEqual(lightMode.icon);
    expect(getNextModeObjSpy).toHaveBeenCalled();
  });

  it('should toggle mode', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('false');
    const setItemSpy = spyOn(localStorage, 'setItem');

    // act
    service.toggleMode();

    // assert
    expect(getItemSpy).toHaveBeenCalled();
    expect(setItemSpy).toHaveBeenCalledWith('dark-mode-activated', 'true')
  });

});

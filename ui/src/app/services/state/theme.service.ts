// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import {Injectable} from '@angular/core';

/**
 * Allows for localStorage based switching between dark and light mode.
 * The selected mode is saved to the browsers LocalStorage.
 * The default (light) mode is actived whenever the dark mode is not explicitly set to activate.
 *
 * @author Marc Dickmann
 */
@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private readonly ModeStorageKey = 'dark-mode-activated';

  private readonly Modes = [
    { name: 'dark-theme', icon: 'dark_mode' },
    { name: 'light-theme', icon: 'light_mode' }
  ];

  /**
   * Returns the localStorage value of modeState.
   */
  private get modeState(): boolean {
    const localStorageEntry = localStorage.getItem(this.ModeStorageKey);
    return localStorageEntry === 'true' ? true : false;
  }

  /**
   * Allows for setting the localStorage value of modeState.
   */
  private set modeState(val: boolean) {
    localStorage.setItem(this.ModeStorageKey, val.toString());
  }

  /**
   * Resets the mode to light mode.
   */
  resetMode(): void {
    this.modeState = false;
  }

  /**
   * Returns currently string-identifier for selected mode/theme
   * @returns currently string-identifier for selected mode/theme
   */
  getModeStr(): string {
    return this.getModeObj().name;
  }

  /**
   * Returns currently selected mode/theme object from resp. array
   *
   * @returns currently selected mode/theme object from resp. array
   */
  getModeObj() {
    if (this.modeState) {
      return this.Modes[0];
    } else {
      return this.Modes[1];
    }
  }

  /**
   * Returns the next modes object. Next as in after toggling.
   *
   * @returns The object of the next mode
   */
  getNextModeObj() {
    if (this.modeState) {
      return this.Modes[1];
    } else {
      return this.Modes[0];
    }
  }

  /**
   * Returns the icon of the next mode. Next as in after toggling.
   *
   * @returns the identifier of the next modes icon
   */
  getNextModeIcon(): string {
    return this.getNextModeObj().icon;
  }

  /**
   * Toggles between dark and light mode.
   */
  toggleMode() {
    this.modeState = !this.modeState;
  }

}

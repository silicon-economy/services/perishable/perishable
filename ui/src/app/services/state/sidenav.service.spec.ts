// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { TestBed } from '@angular/core/testing';

import { SidenavService } from './sidenav.service';

describe('SidenavService', () => {
  let service: SidenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidenavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should reset sidenav state to true', () => {
    // arrange
    const setItemSpy = spyOn(localStorage, 'setItem');

    // act
    service.resetSidenavState();

    // assert
    expect(setItemSpy).toHaveBeenCalledWith('sidenav-state', 'true');
  });

  it('should return the sidenav state', () => {
    // arrange
    const getItemSpy = spyOn(localStorage, 'getItem').and.returnValue('false');

    // act & assert
    expect(service.getSidenavState()).toBeFalse();
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('should set sidenav state to false', () => {
    // arrange
    const setItemSpy = spyOn(localStorage, 'setItem');

    // act
    service.setSidenavState(false);

    // assert
    expect(setItemSpy).toHaveBeenCalledWith('sidenav-state', 'false');
  });


  it('should toggle sidenav state', () => {
    // arrange
    const getSidenavStateSpy = spyOn(service, 'getSidenavState').and.returnValue(true);
    const setSidenavStateSpy = spyOn(service, 'setSidenavState');

    // act
    service.toggleSidenavState();

    // assert
    expect(getSidenavStateSpy).toHaveBeenCalled();
    expect(setSidenavStateSpy).toHaveBeenCalledWith(false);
  });
});

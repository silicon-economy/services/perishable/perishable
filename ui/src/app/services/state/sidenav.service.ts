// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Injectable } from '@angular/core';

/**
 * Tracks the state of the sidebar inside the browsers LocalStorage.
 * The default state of the sidebar is open (true).
 *
 * @author Marc Dickmann
 */
@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private readonly storageKey = 'sidenav-state';

  private get sidenavState(): boolean {
    return localStorage.getItem(this.storageKey) === 'true';
  }

  private set sidenavState(val: boolean) {
    localStorage.setItem(this.storageKey, val.toString());
  }

  /**
   * Resets the state of the sidebar to open.
   */
  resetSidenavState(): void {
    this.sidenavState = true;
  }

  /**
   * Returns the state of the sidebar.
   *
   * @returns True if the sidebar is open; otherwise false.
   */
  getSidenavState(): boolean {
    return this.sidenavState;
  }

  /**
   * Sets the state of the sidebar.
   *
   * @param state True if the sidebar is open; otherwise false.
   */
  setSidenavState(state: boolean): void {
    this.sidenavState = state;
  }

  /**
   * Toggles between open and closed sidebar.
   */
  toggleSidenavState(): void {
    this.setSidenavState(!this.getSidenavState());
  }
}

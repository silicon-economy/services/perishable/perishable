// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { SplitPipe } from './split.pipe';

describe('SplitPipe', () => {
  const pipe = new SplitPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform "563-73609534_F0324EC4" to "563-73609534"', () => {
    expect(pipe.transform('563-73609534_F0324EC4', '_', 0)).toBe('563-73609534');
  });

  it('transform "563-73609534_F0324EC4" to "F0324EC4"', () => {
    expect(pipe.transform('563-73609534_F0324EC4', '_', 1)).toBe('F0324EC4');
  });
});

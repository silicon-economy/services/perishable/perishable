// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

import { Pipe, PipeTransform } from '@angular/core';

/**
 * A custom pipe for transforming the AWB and the external reference
 * in the right format.
 *
 * @author Pajtim Thaqi
 */
@Pipe({
  name: 'split'
})
export class SplitPipe implements PipeTransform {

  transform(text: string, by: string, index: number = 0): string {
    let splitted = text.split(by); // split text by "by" parameter
    return splitted[index];
  }

}

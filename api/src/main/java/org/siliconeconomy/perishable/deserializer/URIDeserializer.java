// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * This class is used to deserialize objects that have an uri field. 
 * The uri field is generated when it is retrieved from the database, as the uri is not stored in the DB.
 * 
 * @author Sascha Wüster
 */
public class URIDeserializer extends StdDeserializer<String> { 

    private static final long serialVersionUID = 6041445945235549250L;

    public URIDeserializer() { 
        this(null); 
    } 

    public URIDeserializer(Class<?> vc) { 
        super(vc); 
    }

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) 
      throws IOException {
        String uri = jp.getText();
        
        if (uri != null) {
            return uri;
        }
        return "uri";
    }
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.entities;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.models.ExternalReference;
import org.siliconeconomy.perishable.models.PieceDocumentState;
import org.siliconeconomy.perishable.models.PieceDocumentType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.groocraft.couchdb.slacker.annotation.Document;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This contains the reference to a document and its belonging piece along with a comment.
 * 
 * @author swuester
 */
@Data
@Document(database = "perishable", accessByView = true, view = "pieceDocument", type = "pieceDocument")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PieceDocument {
    
    /**
     * The id. The document link acts as the unique id of the document.
     */
    @JsonProperty("_id")
    @EqualsAndHashCode.Include
    @NotNull
    private String id;
    
    /**
     * The revision id for each pieceDocument. This revision will be needed to make
     * updates on the newest pieceDocument instance for a given id in the database.
     */
    @NotNull
    @EqualsAndHashCode.Include
    @JsonProperty("_rev")
    private String rev;
    
    /**
     * Ids referencing the pieces.
     */
    @NotNull
    private List<String> pieces;
    
    /**
     *  References to external information.
     */
    @NotNull
    private ExternalReference ref;
    
    /**
     * A comment to the reference and its document.
     */
    private String comment;
    
    /**
     * The internal type of an PieceDocument.
     */
    private PieceDocumentType documentType;
    
    /**
     * The internal state of an PieceDocument.
     */
    private PieceDocumentState state;
    
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.entities;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.models.AuthoritiesInfo;
import org.siliconeconomy.perishable.models.Event;
import org.siliconeconomy.perishable.models.ExternalReference;
import org.siliconeconomy.perishable.models.Item;
import org.siliconeconomy.perishable.models.PackagingType;
import org.siliconeconomy.perishable.models.SpecialHandling;
import org.siliconeconomy.perishable.models.TransportMovement;
import org.siliconeconomy.perishable.models.ULD;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.groocraft.couchdb.slacker.annotation.Document;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This object represents a piece of a shipment part.
 * 
 * @author swuester
 */
@Data
@Document(database = "perishable", accessByView = true, view = "piece", type = "piece")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Piece {
    /**
     * The identifier.
     */
    @JsonProperty("_id")
    @EqualsAndHashCode.Include
    @NotNull
    private String id;
    
    /**
     * The revision id for each piece. This revision will be needed to make
     * updates on the newest piece instance for a given id in the database.
     */
    @JsonProperty("_rev")
    @EqualsAndHashCode.Include
    @NotNull
    private String rev;
    
    /**
     * The UP ID.
     */
    private String upid;
    
    /**
     * Further documents which belong to the piece.
     */
    private List<ExternalReference> externalReferences;
    
    /**
     * Items which are contained in the piece.
     */
    private List<Item> containedItems;
    
    /**
     * Special handling instructions for the piece.
     */
    private List<SpecialHandling> specialHandlings;
    
    /**
     * Contains the type of the packaging of a piece.
     */
    private PackagingType packagingType;
    
    /**
     * Transport movements for the piece.
     */
    private List<TransportMovement> transportMovements;
    
    /**
     * Events for the piece (e.g. ready for pickup time).
     */
    private List<Event> events;
    
    /**
     * ULDs of the piece.
     */
    private List<ULD> ulds;
    
    /**
     * AuthoritiesInfos (e.g. phytosanitaryRelease) of the piece.
     */
    private List<AuthoritiesInfo> authoritiesInfos;
    
    /**
     * References to the URIs of the shipments containing the piece.
     */
    private List<String> shipments;
    
    /**
     * References to the URIs of pieces contained by the piece.
     */
    private List<String> pieces;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.entities;


import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.groocraft.couchdb.slacker.annotation.Document;
import java.util.HashMap;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.Shipment;

/**
 * Represents a shipment in the database.   
 * A shipment transporting certain goods.
 *
 * @author swuester
 */
@Document(database = "perishable", accessByView = true, view = "shipment", type = "shipment")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class ShipmentWrapper {
    /**
     * This id represents a combination of master and house awb code as known for a shipment. 
     */
    @JsonProperty("_id")
    @EqualsAndHashCode.Include
    @NotNull
    private String id;
    
    /**
     * The revision id for each shipment. This revision will be needed to make
     * updates on the newest shipment instance for a given id in the database.
     */
    @JsonProperty("_rev")
    @EqualsAndHashCode.Include
    @NotNull
    private String rev;
   
    
    private Shipment data;
    
    /**
     * The metadata of the attached documents
     */
    @JsonProperty("_attachments")
    private HashMap<String,HashMap<String,Object>> attachments;
    
    
    /**
     * The shipment meta data
     */
    private MetaInformation metainformation;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.word;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This class represents a single row in the generated Word-Document.
 * 
 * @author Pajtim Thaqi
 */
@Data
@AllArgsConstructor
public class Row {
	/* The number of box representing one row. */
	private String numberOfBox;

	/* The English description of the row content. */
	private String english;

	/* The German description of the row content. */
	private String german;

	/* The mandatory field. */
	private String mandatory;

	/* The information for a row. */
	private String information;

}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.word;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.Shipment;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.springframework.stereotype.Service;

/**
 * This service is responsible for generating a word document for a certain
 * {@link Shipment}.
 *
 * @author Pajtim Thaqi, Marc Dickmann
 */
@Service
public class WordDocumentGenerator {

	private static final String TO_BE_FILLED_BY_IMPORT_AGENT = "To be filled by import agent";
	private static final String GREY_HEX = "D3D3D3";

	private static final String[] HEADERS={"Number of Box","Type of Information English","Type of Information German","Mandatory (y/n)","Information"};

	/**
	 * Generates a word document for the given shipment.
	 *
	 * @param shipmentDetails the shipment to generate the document for
	 * @return the generated document as a byte array
	 * @throws IOException
	 */
	public byte[] generateDocument(final ShipmentDetails shipmentDetails) throws IOException {

		try (// Blank document
		XWPFDocument document = new XWPFDocument()) {
            // paragraph for header
			String headerTitle = "Common Health Entry Document for Consignments of Plants, Plant Products, and other Objects (CHED-PP)";
			XWPFParagraph headerParagraph = document.createParagraph();
			XWPFRun headerRun = headerParagraph.createRun();
			headerRun.setBold(true);
			headerRun.setText(headerTitle);

			// paragraph for date
			XWPFParagraph dateParagraph = document.createParagraph();
			XWPFRun dateRun = dateParagraph.createRun();
			dateRun.setText(String.format("Date: %s", LocalDateTime.now()));

			// paragraph for agent
			XWPFParagraph agentParagraph = document.createParagraph();
			XWPFRun agentRun = agentParagraph.createRun();
			agentRun.setText(String.format("Agent: %s", TO_BE_FILLED_BY_IMPORT_AGENT));

			// create <table>
			XWPFTable table = document.createTable();

			// table header
			XWPFTableRow headerRow = table.createRow();
            for(int i=0;i<HEADERS.length;i++) {
                headerRow.getCell(i).setColor(GREY_HEX);
                headerRow.getCell(i).setText(HEADERS[i]);
                headerRow.createCell();
            }

			// consignor
			Optional<ShipmentAgent> consignor = shipmentDetails.getShipmentAgents().stream()
					.filter(x -> x.getRole().equals("Consignor")).findFirst();
			String consignorStr = "";
			if (consignor.isPresent()) {
				consignorStr = consignor.get().toString();
			}
			createRow(table.createRow(), new Row("I.1", "Consignor/Exporter", "Versender/Ausführer", "y", consignorStr));

			createRow(table.createRow(),
					new Row("I.2", "CHED reference", "CGED-Nummer", "y", TO_BE_FILLED_BY_IMPORT_AGENT));

			createRow(table.createRow(),
					new Row("I.3", "Local reference", "Lokale Bezugsnummer", "y", shipmentDetails.getId().split("_")[1]));

			createRow(table.createRow(), new Row("I.4", "Border Control Post/Control Point/Control Unit",
					"Grenzkontrollstelle/Kontroll-stelle/Kontrolleinheit", "y", TO_BE_FILLED_BY_IMPORT_AGENT));

			createRow(table.createRow(), new Row("I.5", "Border Control Post/Control Point/Control Unit Code",
					"Code der Grenzkontrollstelle/Kontrollstelle/Kontrolleinheit", "y", TO_BE_FILLED_BY_IMPORT_AGENT));

			// consignee
			Optional<ShipmentAgent> consignee = shipmentDetails.getShipmentAgents().stream()
					.filter(x -> x.getRole().equals("Consignee")).findFirst();
			String consigneeStr = "";
			if (consignee.isPresent()) {
				consigneeStr = consignee.get().toString();
			}
			createRow(table.createRow(), new Row("I.1", "Consignee/Importer", "Empfänger/Einführer", "y", consigneeStr));

			Optional<ShipmentAgent> placeOfDestination = shipmentDetails.getShipmentAgents().stream()
					.filter(x -> x.getRole().equals("Place of Destination")).findFirst();
			String placeOfDestStr = "";
			if (placeOfDestination.isPresent()) {
				placeOfDestStr = placeOfDestination.get().toString();
			}
			createRow(table.createRow(), new Row("I.7", "Place of destination", "Bestimmungsort", "y", placeOfDestStr));

			createRow(table.createRow(), new Row("I.8", "Operator responsible for the consignment",
					"Für die Sendung verantwortlicher Unternehmer", "y", TO_BE_FILLED_BY_IMPORT_AGENT));

			String documentTypes = shipmentDetails.getDocuments().stream().map(doc -> doc.getDocumentType().toString())
					.collect(Collectors.joining(", "));
			createRow(table.createRow(), new Row("I.9", "Accompanying documents", "Begleitdokumente", "y", documentTypes));
			createRow(table.createRow(),
					new Row("I.10", "Prior notification", "Voranmeldung", "y", shipmentDetails.getSta().toString()));

			String listOfCountriesOfOriginOfPos = shipmentDetails.getPositions().stream().map(PieceDetails::getCountry)
					.collect(Collectors.joining(", "));

			createRow(table.createRow(),
					new Row("I.11", "Country of origin", "Ursprungsland", "y", listOfCountriesOfOriginOfPos));
			createRow(table.createRow(), new Row("I.12", "Region of origin", "Ursprungsregion", "n", ""));

			createRow(table.createRow(),
					new Row("I.13", "Means of transport", "Transportmittel", "y", shipmentDetails.getFlightNo()));
			createRow(table.createRow(),
					new Row("I.14", "Country of dispatch", "Versandland", "y", shipmentDetails.getOrg()));
			createRow(table.createRow(), new Row("I.15", "Establishment of origin", "Ursprungsbetrieb", "n", ""));
			createRow(table.createRow(),
					new Row("I.17", "Container Number/Seal Number", "Containernummer/Plomben-nummer", "n", ""));
			createRow(table.createRow(), new Row("I.20", "For transhipment/transfer", "Zur Umladung/Beförderung", "y",
					TO_BE_FILLED_BY_IMPORT_AGENT));
			createRow(table.createRow(), new Row("I.21", "For onward transportation", "Für den Weitertransport", "y", ""));
			createRow(table.createRow(), new Row("I.22", "For transit to", "Zur Durchfuhr nach", "y", ""));
			createRow(table.createRow(), new Row("I.23", "For internal market", "Für den Binnenmarkt", "y", ""));
			createRow(table.createRow(), new Row("I.25", "For re-entry", "Zur Wiedereinfuhr", "y", ""));
			createRow(table.createRow(), new Row("I.27", "Means of transport after BCP/storage",
					"Transportmittel nach der Grenzkontrollstelle/Lagerung", "n", ""));
			createRow(table.createRow(), new Row("I.29", "Date of departure", "Datum des Abtransports", "n", ""));

			for (PieceDetails pieceDetails : shipmentDetails.getPositions()) {
				createRow(table.createRow(), new Row("I.31", "Commodity", "Erzeugnis", "y", pieceDetails.getHsCode()));
				createRow(table.createRow(), new Row("I.31", "EPPO-Code", "EPPO-Code", "y", pieceDetails.getEppoCode()));
				createRow(table.createRow(),
						new Row("I.31", "Product Type", "Warentyp", "y", TO_BE_FILLED_BY_IMPORT_AGENT));
				createRow(table.createRow(), new Row("I.31", "Net Weight", "Nettogewicht", "y",
						pieceDetails.getWeight().getAmount().toString()));
				createRow(table.createRow(),
						new Row("I.31", "Package count", "Anzahl der Packstücke", "y", TO_BE_FILLED_BY_IMPORT_AGENT));
				createRow(table.createRow(),
						new Row("I.31", "Country of origin", "Ursprungsland", "y", pieceDetails.getCountry()));
				createRow(table.createRow(),
						new Row("I.31", "Quantity", "Menge", "y", pieceDetails.getQuantity().getAmount().toString()));
				createRow(table.createRow(), new Row("I.31", "Net volume", "Nettovolumen", "n", ""));
				if (pieceDetails.getPackagingCode() != null) {
                    createRow(table.createRow(),
                        new Row("I.31", "Packaging material", "Verpackungsmaterial", "n", pieceDetails.getPackagingCode()));
                }
				createRow(table.createRow(), new Row("I.31", "Region of origin", "Ursprungsregion", "n", ""));
				createRow(table.createRow(), new Row("I.31", "Establishment", "Ursprungsbetrieb", "n", ""));
				createRow(table.createRow(),
						new Row("I.31", "Position", "Postennummer", "n", TO_BE_FILLED_BY_IMPORT_AGENT));
			}

			createRow(table.createRow(), new Row("I.32", "Total number of packages", "Gesamtanzahl der Packstücke", "y",
					TO_BE_FILLED_BY_IMPORT_AGENT));
			createRow(table.createRow(), new Row("I.33", "Total quantity", "Menge", "y", TO_BE_FILLED_BY_IMPORT_AGENT));
			createRow(table.createRow(),
					new Row("I.34", "Total Net Weight", "Gesamtnettogewicht", "y", TO_BE_FILLED_BY_IMPORT_AGENT));
			createRow(table.createRow(),
					new Row("I.34", "Total Gross Weight", "Gesamtbruttogewicht", "y", TO_BE_FILLED_BY_IMPORT_AGENT));
			createRow(table.createRow(), new Row("I.35", "Declaration", "Erklärung", "y", TO_BE_FILLED_BY_IMPORT_AGENT));

			// info paragraph
			String infoNote = "Please note that the information is based on the "
					+ "input given on the Perishable Import Dashboard.";
			XWPFParagraph infoParagraph = document.createParagraph();
			XWPFRun infoRun = infoParagraph.createRun();
			infoRun.setBold(true);
			infoRun.setText(infoNote);

			// write to bytestream
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			document.write(out);
			out.close();

			return out.toByteArray();
		}
	}

	/**
	 * Helper method to fill a table row with content
	 *
	 * @param tableRow the table row in the word document
	 * @param content the content to add to the row
	 */
	private void createRow(final XWPFTableRow tableRow, final Row content) {
		tableRow.getCell(0).setText(content.getNumberOfBox());
		tableRow.createCell();
		tableRow.getCell(1).setText(content.getEnglish());
		tableRow.createCell();
		tableRow.getCell(2).setText(content.getGerman());
		tableRow.createCell();
		tableRow.getCell(3).setText(content.getMandatory());

		// transform newline characters into break characters that
		// are supported by ms word
		XWPFTableCell infoCell = tableRow.createCell();
		XWPFParagraph infoParagraph = infoCell.addParagraph();
		XWPFRun infoRun = infoParagraph.createRun();

		for (String textOnNewLine : content.getInformation().split("\n")) {
		    infoRun.setText(textOnNewLine);
		    infoRun.addBreak();
        }
	}
}

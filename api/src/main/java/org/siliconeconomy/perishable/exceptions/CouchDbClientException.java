// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.exceptions;

/**
 * An exception that is thrown when something went wrong
 * while fetching data from the database.
 * 
 * @author Pajtim Thaqi
 */
public class CouchDbClientException extends RuntimeException {
    private static final long serialVersionUID = -6720869739000737464L;
    
    public CouchDbClientException(String message, Throwable cause) {
        super(message, cause);
    }

}

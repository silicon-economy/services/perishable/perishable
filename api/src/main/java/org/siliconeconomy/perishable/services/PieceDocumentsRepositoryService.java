// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import java.io.IOException;

import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.exceptions.CouchDbClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.groocraft.couchdb.slacker.CouchDbClient;
import com.groocraft.couchdb.slacker.exception.CouchDbException;
import org.springframework.http.HttpStatus;

/**
 * This class represents the repository service for {@link PieceDocument}s
 * and implements the {@link PieceDocumentsService}.
 *
 * @author Sascha Wüster
 */
@Service
public class PieceDocumentsRepositoryService implements PieceDocumentsService {
    @Autowired
    private CouchDbClient client;

    @Override
    public PieceDocument findOne(String id) {
        try {
            return client.read(id, PieceDocument.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public PieceDocument update(PieceDocument doc) {
        PieceDocument result = null;
        try {
            result=client.save(doc);
        } catch (CouchDbException ex) {
            if (ex.getStatusCode() != HttpStatus.CONFLICT.value()) {
                throw new CouchDbClientException("Error while saving document data to CouchDB", ex);
            }
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
        return result;
    }
}

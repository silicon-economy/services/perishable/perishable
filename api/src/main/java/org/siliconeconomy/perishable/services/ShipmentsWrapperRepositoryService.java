// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.exceptions.CouchDbClientException;
import org.siliconeconomy.perishable.models.BookingOption;
import org.siliconeconomy.perishable.models.BookingOptionRequest;
import org.siliconeconomy.perishable.models.Company;
import org.siliconeconomy.perishable.models.CompanyBranch;
import org.siliconeconomy.perishable.models.Location;
import org.siliconeconomy.perishable.models.Party;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.groocraft.couchdb.slacker.CouchDbClient;
import com.groocraft.couchdb.slacker.exception.CouchDbException;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.Shipment;
import org.springframework.http.HttpStatus;

/**
 * This class represents the repository service for {@link ShipmentWrapper}s and
 * implements the {@link ShipmentsWrapperService}.
 *
 * @author Sascha Wüster, Pajtim Thaqi
 */
@Service
public class ShipmentsWrapperRepositoryService implements ShipmentsWrapperService {

    @Autowired
    private CouchDbClient client;

    @Override
    public List<ShipmentWrapper> findAll() {
        try {
            List<String> ids = client.readAll(ShipmentWrapper.class);
            return client.readAll(ids, ShipmentWrapper.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<ShipmentWrapper> findAllInIds(List<String> ids) {
        try {
            return client.readAll(ids, ShipmentWrapper.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public ShipmentWrapper findOne(String id) {
        try {
            return client.read(id, ShipmentWrapper.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public ShipmentWrapper update(ShipmentWrapper doc) {
        ShipmentWrapper result = null;
        try {
            result = client.save(doc);
        } catch (CouchDbException ex) {
            if (ex.getStatusCode() != HttpStatus.CONFLICT.value()) {
                throw new CouchDbClientException("Error while saving document data to CouchDB", ex);
            }
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public List<ShipmentWrapper> updateAll(List<ShipmentWrapper> docs) {
        try {
            List<ShipmentWrapper> shipments = new ArrayList<>();
            client.saveAll(docs, ShipmentWrapper.class).forEach(shipments::add);
            return shipments;
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    /**
     * Overwrites the corresponding fields in the {@link ShipmentWrapper} of the
     * database with the new data from the updated shipment agent.
     *
     * @param shipmentId the identifier of the {@link ShipmentWrapper} to update
     * @param agent the updated {@link ShipmentAgent}
     * @return the updated {@link ShipmentAgent} or null if a conflict has
     * happened
     */
    public ShipmentAgent updateShipmentAgent(String shipmentId, ShipmentAgent agent) {
        ShipmentWrapper wrapper = findOne(shipmentId);
        Shipment shipment = wrapper.getData();
        ShipmentAgent result = null;
        if (wrapper.getRev().equals(agent.getRev())) {
            result = agent;
            if (agent.getRole().trim().equalsIgnoreCase("Consignee")) {
                updateParties(agent, shipment);
            } else if (agent.getRole().trim().equalsIgnoreCase("Consignor")) {
                shipment.setShipper(getCompanyByAgent(agent));

            } else if (agent.getRole().trim().equalsIgnoreCase("ExportAgent")) {
                shipment.setForwarder(getCompanyByAgent(agent));

            } else if (agent.getRole().trim().equalsIgnoreCase("PlaceofDestination")) {
                Location deliveryLocation = new Location();
                deliveryLocation.setAddress(agent.getAddress());
                deliveryLocation.setName(agent.getName());
                shipment.setDeliveryLocation(deliveryLocation);

            } else if (agent.getRole().trim().equalsIgnoreCase("Carrier")) {
                updateBookingOptionRequests(agent, shipment);
            }

            if (update(wrapper) == null) {
                result = null;
            }
        }
        return result;
    }

    /**
     * Delivers the company to a shipment agent.
     *
     * @param agent the {@link ShipmentAgent}
     * @return the company of the {@link ShipmentAgent}
     */
    public Company getCompanyByAgent(ShipmentAgent agent) {
        Location location = new Location();
        location.setAddress(agent.getAddress());

        CompanyBranch branch = new CompanyBranch();
        branch.setLocation(location);

        Company company = new Company();
        company.setCompanyName(agent.getName());
        company.setCompanyBranch(branch);
        return company;
    }

    /**
     * Update all shipments containing the piece. Returns true if update was
     * successful, otherwise returns false.
     *
     * @param shipmentIds the shipments containing the piece
     * @param pieceWeightDelta the delta of the piece weight
     * @return a success boolean
     */
    public Boolean updateAllShipmentsContainingPiece(List<String> shipmentIds, Float pieceWeightDelta) {
        Boolean success = false;

        if (shipmentIds != null && !shipmentIds.isEmpty()) {
            List<ShipmentWrapper> shipments = this.findAllInIds(shipmentIds);

            if (shipments != null) {
                for (ShipmentWrapper wrapper : shipments) {
                    Shipment shipment = wrapper.getData();
                    // Adjust the total gross weight of the shipment
                    Value shipmentWeight = new Value();
                    // It is assumed that the weight unit for all pieces of a shipment and between piece changes by a user is identical
                    shipmentWeight.setUnit(shipment.getTotalGrossWeight().getUnit());
                    Float shipmentWeightAmount = shipment.getTotalGrossWeight().getAmount();
                    shipmentWeightAmount += pieceWeightDelta;
                    shipmentWeight.setAmount(shipmentWeightAmount);
                    shipment.setTotalGrossWeight(shipmentWeight);
                }
                // Update the shipments containing the changed piece
                if (this.updateAll(shipments) != null) {
                    success = true;
                }
            }
        }
        return success;
    }

    /**
     * Add the new piece to the shipment in the database.
     *
     * @param pieceDetails the {@link PieceDetails} containing the changed
     * fields
     * @param id the id of the shipment holding the piece
     * @param revId the revision id of the shipment holding the piece
     * @param pieceWeightDelta the delta of the piece weight
     */
    public boolean addPieceToShipment(PieceDetails pieceDetails, String id, String revId, Float pieceWeightDelta) {
        boolean result = false;
        if (id != null) {
            ShipmentWrapper wrapper = this.findOne(id);
            Shipment shipment = wrapper.getData();
            if (shipment != null && wrapper.getRev().equals(revId)) {
                // Adjust the total gross weight of the shipment
                Value shipmentWeight = new Value();
                // It is assumed that the weight unit for all pieces of a shipment and between piece changes by a user is identical
                if (shipment.getTotalGrossWeight() != null) {
                    shipmentWeight.setUnit(shipment.getTotalGrossWeight().getUnit());
                    Float shipmentWeightAmount = shipment.getTotalGrossWeight().getAmount();
                    if (shipmentWeightAmount != null) {
                        shipmentWeightAmount += pieceWeightDelta;
                    }
                    shipmentWeight.setAmount(shipmentWeightAmount);
                }
                shipment.setTotalGrossWeight(shipmentWeight);

                List<String> pieces = shipment.getPieces();
                if (pieces == null) {
                    pieces = new ArrayList<>();
                }
                pieces.add(pieceDetails.getId());
                shipment.setPieces(pieces);

                // Update the changed shipment in the database
                result = this.update(wrapper) != null;

            }
        }
        return result;
    }

    
    /**
     * Returns the meta information for a certain shipment.
     * 
     * @param shipmentId
     * @return the meta information
     */
    public MetaInformation getMetaInformation(String shipmentId) {
        return findOne(shipmentId).getMetainformation();
    }
    
    
    /**
     * Updates the booking option requests list in the {@link ShipmentWrapper} with the
     * new data from the updated shipment agent.
     *
     * @param agent the updated {@link ShipmentAgent}
     * @param shipment the {@link ShipmentWrapper} from the database
     */
    private void updateBookingOptionRequests(ShipmentAgent agent, Shipment shipment) {
        for (BookingOptionRequest request : shipment.getBookingOptionRequests()) {
            for (BookingOption option : request.getBookingOptions()) {
                // Assumption: the name of a carrier is unique
                if (option.getCompany().getCompanyName().trim().equalsIgnoreCase(agent.getName().trim())) {
                    // There is already a carrier in the shipment, which should be updated in the booking options list
                    option.setCompany(getCompanyByAgent(agent));
                    // Exit this  method as the option requests are updated
                    return;
                }
            }
        }
        // Otherwise there is no carrier yet, so it has to be added to the option requests
        BookingOptionRequest lastRequest = shipment.getBookingOptionRequests().get(shipment.getBookingOptionRequests().size() - 1);
        BookingOption carrier = new BookingOption();
        carrier.setCompany(getCompanyByAgent(agent));
        lastRequest.getBookingOptions().add(carrier);
    }

    /**
     * Updates the parties list in the {@link ShipmentWrapper} with the new data from
     * the updated shipment agent.
     *
     * @param agent the updated {@link ShipmentAgent}
     * @param shipment the {@link ShipmentWrapper} from the database
     */
    private void updateParties(ShipmentAgent agent, Shipment shipment) {
        for (Party party : shipment.getParties()) {
            // Assumption: the name of a consignee is unique
            if (party.getPartyDetails().getCompanyName().trim().equalsIgnoreCase(agent.getName().trim())) {
                // There is already a consignee in the shipment, which should be updated in the parties list
                party.setPartyRole(agent.getRole());
                party.setPartyDetails(getCompanyByAgent(agent));
                // Exit this  method as the parties are updated
                return;
            }
        }
        // Otherwise there is no consignee yet, so it has to be added to the parties
        Party consignee = new Party();
        consignee.setPartyRole(agent.getRole());
        consignee.setPartyDetails(getCompanyByAgent(agent));
        shipment.getParties().add(consignee);
    }
}

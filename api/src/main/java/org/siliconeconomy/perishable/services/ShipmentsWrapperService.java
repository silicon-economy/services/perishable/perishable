// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import java.util.List;

import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.ShipmentInfo;
import org.springframework.stereotype.Service;

/**
 * Service for {@link ShipmentWrapper}s.
 *
 * @author Sascha Wüster, Pajtim Thaqi
 */
@Service
public interface ShipmentsWrapperService {

    /**
     * Returns a list of all {@link ShipmentInfo}s.
     *
     * @return all shipments
     */
    List<ShipmentWrapper> findAll();

    /**
     * Returns a {@link ShipmentWrapper} for the given id.
     *
     * @param id the id to search for
     * @return a {@link ShipmentWrapper}
     */
    ShipmentWrapper findOne(String id);

    /**
     * Updates a {@link ShipmentWrapper}.
     *
     * @param doc the ShipmentWrapper to update
     * @return the updated entity
     */
    ShipmentWrapper update(ShipmentWrapper doc);

    /**
     * Returns all {@link ShipmentWrapper}s for the given ids.
     *
     * @param ids the list of ids to search for
     * @return a list of {@link ShipmentWrapper}s
     */
    List<ShipmentWrapper> findAllInIds(List<String> ids);

    /**
     * Update all {@link ShipmentWrapper}s.
     *
     * @param docs the {@link ShipmentWrapper}s list to update
     * @return the updated entities
     */
    List<ShipmentWrapper> updateAll(List<ShipmentWrapper> docs);
}

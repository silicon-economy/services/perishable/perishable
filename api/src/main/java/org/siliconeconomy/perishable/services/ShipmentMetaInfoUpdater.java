// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.Shipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Helper class used to update the {@link MetaInformation#isExported()} of a
 * {@link Shipment}.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class ShipmentMetaInfoUpdater {

	@Autowired
	private ShipmentsWrapperService repository;

	/**
	 * Updates a {@link Shipment} by updating the
	 * {@link MetaInformation#isExported()}.
	 * 
	 * @param shipmentId the shipment to update
	 * @param exported   the new exported flag
	 * @return true, if the update completed successfully, false otherwise
	 */
	public boolean updateExportFlag(final String shipmentId, boolean exported) {
		ShipmentWrapper shipmentWrapper = repository.findOne(shipmentId);
		MetaInformation metainformation = shipmentWrapper.getMetainformation();
		metainformation.setExported(exported);

		return repository.update(shipmentWrapper) != null;
	}

}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.PieceDocumentValidationState;
import org.siliconeconomy.perishable.models.ShipmentDetailsValidationResult;
import org.siliconeconomy.perishable.models.ShipmentStatus;
import org.siliconeconomy.perishable.models.ValidationState;
import org.springframework.stereotype.Service;

/**
 * Helper class for determining the {@link ShipmentStatus} for a
 * {@link ShipmentDetailsValidationResult} and the {@link MetaInformation} which
 * belongs to a shipmnent.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class ShipmentStatusDeterminer {
	/**
	 * Determine the shipment status for the provided shipment.
	 * 
	 * <p>
	 * Have a look at {@link ShipmentStatus} to see, how the status is determined.
	 * 
	 * @param details  the shipment details validation result
	 * @param metaInfo the meta information for this shipment
	 * @return the determined shipment status
	 */
	public ShipmentStatus determineShipmentStatus(ShipmentDetailsValidationResult validationResult,
			MetaInformation metaInfo) {

		boolean validationStatus = validationResult.getConsignee().equals(ValidationState.VALID)
				&& validationResult.getConsignor().equals(ValidationState.VALID)
				&& validationResult.getPlaceOfDestination().equals(ValidationState.VALID)
				&& validationResult.getPhytoCertificate().equals(PieceDocumentValidationState.VALID)
				&& validationResult.getMawb().equals(PieceDocumentValidationState.VALID);

		ShipmentStatus status = null;

		if (validationStatus && metaInfo.isExported()) {
			status = ShipmentStatus.EXPORTED;
		} else if (validationStatus && !metaInfo.isExported()) {
			status = ShipmentStatus.COMPLETED;
		} else {
			status = ShipmentStatus.INCOMPLETE;
		}

		return status;
	}
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import java.util.List;

import org.siliconeconomy.perishable.entities.Piece;
import org.springframework.stereotype.Service;

/**
 * Service for {@link Piece}s.
 * 
 * @author swuester
 */
@Service
public interface PiecesService {
    /**
     * Returns a {@link Piece} for the given id.
     * 
     * @param id the id to search for
     * @return a {@link Piece}
     */
    Piece findOne(String id);
    
    /**
     * Returns all {@link Piece}s for the given ids.
     * 
     * @param ids the list of ids to search for
     * @return a list of {@link Piece}s
     */
    List<Piece> findAllInIds(List<String> ids);
    
    /**
     * Updates a {@link Piece}.
     * 
     * @param doc the piece to update
     * @return the updated piece
     */
    Piece update(Piece doc);
    
    /**
     * Save a {@link Piece}.
     * 
     * @param doc the piece to save
     * @return the saved piece
     */
    Piece save(Piece doc);
}

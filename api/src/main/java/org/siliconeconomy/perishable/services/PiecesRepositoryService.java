// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.exceptions.CouchDbClientException;
import org.siliconeconomy.perishable.models.Country;
import org.siliconeconomy.perishable.models.Item;
import org.siliconeconomy.perishable.models.PackagingType;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.Product;
import org.siliconeconomy.perishable.models.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.groocraft.couchdb.slacker.CouchDbClient;
import org.siliconeconomy.perishable.models.Shipment;

/**
 * This class represents the repository service for {@link Piece}s
 * and implements the {@link PiecesService}.
 *
 * @author swuester
 */
@Service
public class PiecesRepositoryService implements PiecesService {
    @Autowired
    private CouchDbClient client;
    @Autowired
    private ShipmentsWrapperRepositoryService shipmentsService;

    @Override
    public Piece findOne(String id) {
        try {
            return client.read(id, Piece.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<Piece> findAllInIds(List<String> ids) {
        try {
            return client.readAll(ids, Piece.class);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public Piece update(Piece doc) {
        try {
            doc.setRev(findOne(doc.getId()).getRev());
            return client.save(doc);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    @Override
    public Piece save(Piece doc) {
        try {
            return client.save(doc);
        } catch (IOException ex) {
            throw new CouchDbClientException(ex.getMessage(), ex);
        }
    }

    /**
     * Update or add the piece in the database.
     *
     * @param pieceDetails containing the changed fields
     * @param shipmentId the id of the {@link Shipment} holding the piece
     * @param shipmentRevId the revision id of the shipment
     * @return the updated {@link PieceDetails} or null if a conflict happened
     */
    public PieceDetails updatePiece(PieceDetails pieceDetails, String shipmentId, String shipmentRevId) {
        PieceDetails result = pieceDetails;
        if (pieceDetails.getId() != null) {
            Piece piece = this.findOne(pieceDetails.getId());

            if (piece != null) {
                Float oldItemAmount = piece.getContainedItems().get(0).getWeight().getAmount();
                int itemNumber = piece.getContainedItems().size();
                // Update an existing piece in the database
                Piece updatedPiece = updatePieceFields(pieceDetails, piece);

                /* Analogous to other passages, it is assumed here for the sake of simplicity that all items have the same amount
                 * and have the same unit. */
                Float pieceWeightDelta = 0f;
                if (pieceDetails.getWeight().getAmount() != null && oldItemAmount != null) {
                    // If no piece weight was set, set the weight delta to 0
                    pieceWeightDelta = pieceDetails.getWeight().getAmount()
                            - oldItemAmount * itemNumber;
                }

                this.shipmentsService.updateAllShipmentsContainingPiece(updatedPiece.getShipments(), pieceWeightDelta);
                this.update(updatedPiece);
            }

        } else {
            // Add a new piece to the database
            Piece piece = new Piece();
            /* create an item for the new piece. This can be removed later as soon as there is a possibility in the UI
             * to choose the number of items to be created for. */
            Item item = new Item();
            item.setProduct(new Product());
            Country productionCountry = new Country();
            item.setProductionCountry(productionCountry);
            piece.setContainedItems(Arrays.asList(item));

            Piece updatedPiece = updatePieceFields(pieceDetails, piece);

            updatedPiece = this.save(updatedPiece);
            String pieceId = null;
            if (updatedPiece != null) {
                pieceId = updatedPiece.getId();
            }
            pieceDetails.setId(pieceId);
            if (!this.shipmentsService.addPieceToShipment(pieceDetails, shipmentId, shipmentRevId, pieceDetails.getWeight().getAmount())) {
                // we have a conflict
                result = null;
            }
        }
        return result;
    }

    /**
     * Update the changed fields of the {@link Piece} and return the updated piece.
     *
     * @param pieceDetails containing the changed fields
     * @param piece the piece to be updated
     * @return the updated piece
     */
    private Piece updatePieceFields(PieceDetails pieceDetails, Piece piece) {
        for (Item item: piece.getContainedItems()) {
            // Update these fields for each item in this piece
            Product product = item.getProduct();
            product.setCommodityName(pieceDetails.getGoodType());
            product.setHsCode(pieceDetails.getHsCode());
            product.setEppoCode(pieceDetails.getEppoCode());
            if (item.getProductionCountry() != null) {
                item.getProductionCountry().setCountryName(pieceDetails.getCountry());
            }
            item.setProduct(product);

            // Set quantity for each item
            Value quantity = new Value();
            if (pieceDetails.getQuantity() == null) {
                // If no amount was set, set it to an empty object
                pieceDetails.setQuantity(quantity);
            }
            Float amountPerItem = null;
            if (pieceDetails.getQuantity().getAmount() != null) {
                amountPerItem = pieceDetails.getQuantity().getAmount() / piece.getContainedItems().size();
            }
            quantity.setAmount(amountPerItem);
            quantity.setUnit(pieceDetails.getQuantity().getUnit());
            item.setQuantity(quantity);

            // Set weight for each item
            Value weight = new Value();
            if (pieceDetails.getWeight() == null) {
                // If no weight was set, set it to an empty object
                pieceDetails.setWeight(weight);
            }
            Float weightPerItem = null;
            if (pieceDetails.getWeight().getAmount() != null) {
                weightPerItem = pieceDetails.getWeight().getAmount() / piece.getContainedItems().size();
            }
            weight.setAmount(weightPerItem);
            weight.setUnit(pieceDetails.getWeight().getUnit());
            item.setWeight(weight);
        }
        PackagingType packaging = new PackagingType();
        packaging.setPackagingTypeDescription(pieceDetails.getPackaging());
        packaging.setTypeCode(pieceDetails.getPackagingCode());
        piece.setPackagingType(packaging);
        return piece;
    }
}

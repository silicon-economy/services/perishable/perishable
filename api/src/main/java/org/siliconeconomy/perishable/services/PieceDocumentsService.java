// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import org.siliconeconomy.perishable.entities.PieceDocument;
import org.springframework.stereotype.Service;

/**
 * Service for {@link PieceDocument}s.
 * 
 * @author Sascha Wüster
 */
@Service
public interface PieceDocumentsService {
    /**
     * Returns a {@link PieceDocument} for the given id.
     * 
     * @param id the id to search for
     * @return a {@link PieceDocument}
     */
    PieceDocument findOne(String id);
    
    /**
     * Updates a {@link PieceDocument}.
     * 
     * @param doc the pieceDocument to update
     * @return the updated entity
     */
    PieceDocument update(PieceDocument doc);
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.models.AuthoritiesInfo;
import org.siliconeconomy.perishable.models.Event;
import org.siliconeconomy.perishable.models.Item;
import org.siliconeconomy.perishable.models.MovementTimes;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.PieceDetailsValidationResult;
import org.siliconeconomy.perishable.models.SequenceDescription;
import org.siliconeconomy.perishable.models.SpecialHandling;
import org.siliconeconomy.perishable.models.TemperatureHandling;
import org.siliconeconomy.perishable.models.TimeSequence;
import org.siliconeconomy.perishable.models.TimeWithDescription;
import org.siliconeconomy.perishable.models.TransportMovement;
import org.siliconeconomy.perishable.models.ValidationState;
import org.siliconeconomy.perishable.models.Value;
import org.springframework.stereotype.Component;

/**
 * Utility methods for {@link ShipmentMapper}.
 *
 * @author Sascha Wüster
 */
@Component
public class ShipmentMapperUtils {
    public static final String DEPARTURE = "departure";
    public static final String SCHEDULED = "scheduled";
    public static final String ARRIVAL = "arrival";
    public static final String ACTUAL = "actual";
    /**
     * This list of constants represents the special handling codes for perishable goods (PER), that are required to display the group in the ui.
     */
    private static final List<String> GROUP_CODES = Arrays.asList(
            "PEA", "PEF", "PES", "PEM", "PEP", "EAT",
            "PIL", "AVI", "HEG", "AVF", "AVC", "AVX",
            "SPF", "PER");
    /**
     * This list of constants represents the temperature range codes for storage, that are required to display the planned temperature in the ui.
     */
    private static final List<TemperatureHandling> TEMPERATURE_CODES = Arrays.asList(
            TemperatureHandling.COL, TemperatureHandling.FRI, TemperatureHandling.FRO, TemperatureHandling.CRT, TemperatureHandling.ERT, TemperatureHandling.ACT);

    /**
     * Returns the last flight of a shipment. The last flight is determined from a random piece (here the first)
     * and there the last uld. Then the latest flight is to be selected according to the scheduled time departure of the flight.
     *
     * @param firstPiece the first piece of shipment
     * @param lastUld the index of the last uld
     * @return the last flight {@link TransportMovement}
     */
    public TransportMovement getLastFlight(Piece firstPiece, Integer lastUld) {
        TransportMovement lastFlight = null;
        Date currentStd = null;
        for (TransportMovement tm: firstPiece.getUlds().get(lastUld).getTransportMovements()) {
            for (MovementTimes mt: tm.getMovementTimes()) {
                if (mt.getTimeType().equalsIgnoreCase(SCHEDULED) && mt.getDirection().equalsIgnoreCase(DEPARTURE)) {
                    currentStd = mt.getMovementTimeStamp();
                }
                if (lastFlight == null) {
                    lastFlight = tm;
                }
                for (MovementTimes lastFlightTime: lastFlight.getMovementTimes()) {
                    if (lastFlightTime.getTimeType().equalsIgnoreCase(SCHEDULED) &&
                            lastFlightTime.getDirection().equalsIgnoreCase(DEPARTURE) &&
                            lastFlightTime.getMovementTimeStamp().before(currentStd)) {
                        lastFlight = tm;
                    }
                }
            }
        }
        return lastFlight;
    }

    /**
     * Delivers the ground handler and import warehouse for the blue bar in the ui.
     *
     * @param firstPiece the first piece of the shipment
     * @param lastUld the index of the last uld
     * @return the {@link TimeSequence}s for the blue bar
     */
    public List<TimeSequence> getTimeSequencesFromEvents(Piece firstPiece, Integer lastUld) {
        List<TimeSequence> groundhandlers = new ArrayList<>();
        List<TimeSequence> importWarehouses = new ArrayList<>();

        Collections.sort(firstPiece.getUlds().get(lastUld).getEvents());

        TimeSequence groundhandler = null;
        List<TimeWithDescription> tdList = null;
        TimeSequence importWarehouse = null;
        List<TimeWithDescription> importWarehouseTdList = null;

        for (Event event: firstPiece.getUlds().get(lastUld).getEvents()) {
            // Ground Handler
            if (event.getEventTypeIndicator().equalsIgnoreCase("received by ground handler")) {
                if (groundhandler != null) {
                    groundhandler.setTimeWithDescriptions(tdList);
                    groundhandlers.add(groundhandler);
                }
                groundhandler = new TimeSequence();
                groundhandler.setSequenceDescription(SequenceDescription.GROUND_HANDLER);
                tdList = new ArrayList<>();
                tdList.add(getTimeWithDescription("Received", event.getDateTime()));
            }
            if (event.getEventTypeIndicator().equalsIgnoreCase("delivered by ground handler") && tdList != null) {
                tdList.add(getTimeWithDescription("Delivered", event.getDateTime()));
            }
            // Import Warehouse
            if (event.getEventTypeIndicator().equalsIgnoreCase("accepted by pcf")) {
                if (importWarehouse != null) {
                    importWarehouse.setTimeWithDescriptions(importWarehouseTdList);
                    importWarehouses.add(importWarehouse);
                }
                importWarehouse = new TimeSequence();
                importWarehouse.setSequenceDescription(SequenceDescription.IMPORT_WAREHOUSE);
                importWarehouseTdList = new ArrayList<>();
                importWarehouseTdList.add(getTimeWithDescription("Acceptance", event.getDateTime()));
            }
            if (event.getEventTypeIndicator().equalsIgnoreCase("check time pcf")) {
                importWarehouseTdList.add(getTimeWithDescription("Check time", event.getDateTime()));
            }
            if (event.getEventTypeIndicator().equalsIgnoreCase("ready for inspection pcf")) {
                importWarehouseTdList.add(getTimeWithDescription("Ready for inspection", event.getDateTime()));
            }
        }

        if (groundhandler != null) {
            groundhandler.setTimeWithDescriptions(tdList);
            groundhandlers.add(groundhandler);
        }
        importWarehouse.setTimeWithDescriptions(importWarehouseTdList);
        importWarehouses.add(importWarehouse);

        List<TimeSequence> sequences = groundhandlers;
        sequences.addAll(importWarehouses);
        return sequences;
    }

    /**
     * Delivers the customs and releases for the blue bar in the ui.
     *
     * @param firstPiece the first piece of the shipment
     * @return the {@link TimeSequence}s for the blue bar
     */
    public List<TimeSequence> getTimeSequencesFromAuthoritiesInfos(Piece firstPiece) {
        List<TimeSequence> releasesList = new ArrayList<>();
        List<TimeSequence> customsList = new ArrayList<>();

        Collections.sort(firstPiece.getAuthoritiesInfos());
        TimeSequence releases = null;
        List<TimeWithDescription> tdList = null;

        for (AuthoritiesInfo authInfo: firstPiece.getAuthoritiesInfos()) {
            if (authInfo.getType().equalsIgnoreCase("customs")) {
                // Customs
                TimeSequence customs = new TimeSequence();
                customs.setSequenceDescription(SequenceDescription.CUSTOMS);
                customs.setTimeWithDescriptions(Arrays.asList(
                    getTimeWithDescription("Release", authInfo.getReleaseTime())));
                customsList.add(customs);
            } else {
                // Releases
                if (authInfo.getType().equalsIgnoreCase("phytosanitary")) {
                    // Releases => phyto
                    if (releases != null) {
                        releases.setTimeWithDescriptions(tdList);
                        releasesList.add(releases);
                    }
                    releases = new TimeSequence();
                    tdList = new ArrayList<>();
                    releases.setSequenceDescription(SequenceDescription.RELEASES);
                    tdList.add(getTimeWithDescription("Phytosanitarian release", authInfo.getReleaseTime()));
                }
                if (authInfo.getType().equalsIgnoreCase("veterinarian") && tdList != null) {
                    // Releases => vet
                    tdList.add(getTimeWithDescription("Veterinarian release", authInfo.getReleaseTime()));
                }
                if (authInfo.getType().equalsIgnoreCase("ble") && tdList != null) {
                    // Releases => ble
                    tdList.add(getTimeWithDescription("BLE release", authInfo.getReleaseTime()));
                }
            }
        }
        if (releases != null) {
            releases.setTimeWithDescriptions(tdList);
        }
        releasesList.add(releases);

        List<TimeSequence> sequences = releasesList;
        sequences.addAll(customsList);
        return sequences;
    }

    /**
     * Delivers the export warehouse, on blocks, off blocks, delivery address and pickUp for the blue bar in the ui.
     *
     * @param firstPiece the first piece of the shipment
     * @return the {@link TimeSequence}s for the blue bar
     */
    public List<TimeSequence> getTimeSequencesFromTransportMovements(Piece firstPiece, Integer lastUld) {
        List<TimeSequence> exportWarehouses = new ArrayList<>();
        List<TimeSequence> pickups = new ArrayList<>();
        List<TimeSequence> deliveryAddresses = new ArrayList<>();
        List<TimeSequence> offBlocksOnBlocksList = new ArrayList<>();

        List<TimeWithDescription> pickUpTimeWithDescriptions = null;

        firstPiece.getTransportMovements().forEach(tm -> Collections.sort(tm.getMovementTimes()));
        Collections.sort(firstPiece.getTransportMovements());

        for (TransportMovement tm: firstPiece.getTransportMovements()) {
            // Export Warehouse
            if (tm.getTransportMeans().getVehicleType().equalsIgnoreCase("warehouse")) {
                exportWarehouses = getExportWarehouses(tm, exportWarehouses);
            }
            if (tm.getTransportMeans().getVehicleType().equalsIgnoreCase("truck")) {
                TimeSequence pickup = new TimeSequence();
                TimeSequence deliveryAddress = new TimeSequence();
                pickUpTimeWithDescriptions = new ArrayList<>();
                pickup.setSequenceDescription(SequenceDescription.PICK_UP);
                deliveryAddress.setSequenceDescription(SequenceDescription.DELIVERY_ADDRESS);
                for (Event event: firstPiece.getEvents()) {
                    if(event.getEventTypeIndicator().equalsIgnoreCase("ready for pick up")) {
                        // Pick Up => ready
                       pickUpTimeWithDescriptions.add(getTimeWithDescription("Ready", event.getDateTime()));
                    }
                    if(event.getEventTypeIndicator().equalsIgnoreCase("arrival at pcf")) {
                        // Pick Up => arrival
                        pickUpTimeWithDescriptions.add(getTimeWithDescription("Arrival", event.getDateTime()));
                     }
                }
                for (MovementTimes mt: tm.getMovementTimes()) {
                    if (mt.getDirection().equalsIgnoreCase(DEPARTURE)) {
                        // Pick Up => departure
                        pickUpTimeWithDescriptions.add(getTimeWithDescription("Departure", mt.getMovementTimeStamp()));
                    }
                    if (mt.getDirection().equalsIgnoreCase(ARRIVAL)) {
                        // Delivery Address
                        List<TimeWithDescription> deliveryTimeWithDescriptions = new ArrayList<>();
                        deliveryTimeWithDescriptions.add(getTimeWithDescription("Arrival", mt.getMovementTimeStamp()));
                        deliveryAddress.setTimeWithDescriptions(deliveryTimeWithDescriptions);
                    }
                }
                pickup.setTimeWithDescriptions(pickUpTimeWithDescriptions);
                pickups.add(pickup);
                deliveryAddresses.add(deliveryAddress);
            }
        }
        for (TransportMovement tm: firstPiece.getUlds().get(lastUld).getTransportMovements()) {
            if (tm.getTransportMeans().getVehicleType().equalsIgnoreCase("aircraft")) {
                offBlocksOnBlocksList.addAll(getOffBlocksOnBlocksList(tm));
            }
        }
        firstPiece.getUlds().get(lastUld).getTransportMovements().forEach(tm -> Collections.sort(tm.getMovementTimes()));
        Collections.sort(firstPiece.getUlds().get(lastUld).getTransportMovements());

        List<TimeSequence> sequences = exportWarehouses;
        sequences.addAll(offBlocksOnBlocksList);
        sequences.addAll(pickups);
        sequences.addAll(deliveryAddresses);
        return sequences;
    }

    /**
     * Returns the export warehouses for the blue list.
     *
     * @param tm the {@link TranpsortMovement}
     * @param exportWarehouses the {@link ExportWarehouse}s
     * @return the filled export warehouses list
     */
    private List<TimeSequence> getExportWarehouses(TransportMovement tm, List<TimeSequence> exportWarehouses) {
        TimeSequence exportWarehouse = null;
        List<TimeWithDescription> warehouseTimeWithDescriptions = null;

        for (MovementTimes mt: tm.getMovementTimes()) {
            TimeWithDescription td = new TimeWithDescription();
            if (mt.getDirection().equalsIgnoreCase(DEPARTURE)) {
                if (exportWarehouse != null) {
                    exportWarehouse.setTimeWithDescriptions(warehouseTimeWithDescriptions);
                    exportWarehouses.add(exportWarehouse);
                }
                warehouseTimeWithDescriptions = new ArrayList<>();
                exportWarehouse = new TimeSequence();
                exportWarehouse.setSequenceDescription(SequenceDescription.EXPORT_WAREHOUSE);
                td.setTimeDescription("In");
            }
            if (mt.getDirection().equalsIgnoreCase(ARRIVAL)) {
                td.setTimeDescription("Out");
            }
            td.setTime(mt.getMovementTimeStamp());
            if (warehouseTimeWithDescriptions != null) {
                warehouseTimeWithDescriptions.add(td);
            }
        }
        if (exportWarehouse != null) {
            exportWarehouse.setTimeWithDescriptions(warehouseTimeWithDescriptions);
            exportWarehouses.add(exportWarehouse);
        }
        return exportWarehouses;
    }

    /**
     * Returns the concatenated list from off blocks and on blocks for the blue list.
     *
     * @param tm the {@link TranpsortMovement}
     * @param offBlocksList the off blocks list
     * @param onBlocksList the on blocks list
     * @return the offblocksOnBlocksList
     */
    private List<TimeSequence> getOffBlocksOnBlocksList(TransportMovement tm) {
        List<TimeSequence> offBlocksList = new ArrayList<>();
        List<TimeSequence> onBlocksList = new ArrayList<>();
        TimeSequence offBlocks = null;
        TimeSequence onBlocks = null;

        for (MovementTimes mt: tm.getMovementTimes()) {
            if (mt.getDirection().equalsIgnoreCase(DEPARTURE) && mt.getTimeType().equalsIgnoreCase(ACTUAL)) {
                // Off Blocks
                offBlocks = new TimeSequence();
                offBlocks.setSequenceDescription(SequenceDescription.OFF_BLOCKS);
                offBlocks.setTimeWithDescriptions(Arrays.asList(
                    getTimeWithDescription(Strings.EMPTY, mt.getMovementTimeStamp())));
                offBlocksList.add(offBlocks);
            }
            if (mt.getDirection().equalsIgnoreCase(ARRIVAL) && mt.getTimeType().equalsIgnoreCase(ACTUAL)) {
                // On Blocks
                onBlocks = new TimeSequence();
                onBlocks.setSequenceDescription(SequenceDescription.ON_BLOCKS);
                onBlocks.setTimeWithDescriptions(Arrays.asList(
                    getTimeWithDescription(Strings.EMPTY, mt.getMovementTimeStamp())));
                onBlocksList.add(onBlocks);
            }
        }
        List<TimeSequence> sequences = offBlocksList;
        sequences.addAll(onBlocksList);
        return sequences;
    }

    /**
     * Delivers the {@link PieceDetails} for the positions table in the ui.
     *
     * @param pieces the pieces of the shipment
     * @param lastUld the index of the last uld
     * @return the {@link pieceDetails} list for the positions table in the ui
     */
    public List<PieceDetails> createPosTableFromShipment(List<Piece> pieces, Integer lastUld) {
        List<PieceDetails> pieceDetailsList = new ArrayList<>();
        // Create a {@link PieceDetails} for each piece in the shipment
        for (Piece piece: pieces) {
            // Add the position (={@link PieceDetails}) to the position table
            pieceDetailsList.add(createPieceDetails(piece, lastUld));
        }
        return pieceDetailsList;
    }

    /**
     * Create a {@link PieceDetails} for a piece.
     *
     * @param piece the piece for which a {@link PieceDetails} is to be created
     * @param lastUld the index of the last uld of the piece
     * @return the newly created {@link PieceDetails}
     */
    private PieceDetails createPieceDetails(Piece piece, Integer lastUld) {
        PieceDetails pieceDetails = new PieceDetails();
        pieceDetails.setId(piece.getId());
        if (piece.getContainedItems() != null) {
            pieceDetails.setHsCode(piece.getContainedItems().get(0).getProduct().getHsCode());
            pieceDetails.setEppoCode(piece.getContainedItems().get(0).getProduct().getEppoCode());
            pieceDetails.setGoodType(piece.getContainedItems().get(0).getProduct().getCommodityName());
            if (piece.getContainedItems().get(0).getProductionCountry() != null) {
                pieceDetails.setCountry(piece.getContainedItems().get(0).getProductionCountry().getCountryName());
            }
        }
        if (piece.getPackagingType() != null) {
            pieceDetails.setPackaging(piece.getPackagingType().getPackagingTypeDescription());
            pieceDetails.setPackagingCode(piece.getPackagingType().getTypeCode());
        }
        if (piece.getUlds() != null) {
            pieceDetails.setUldId(piece.getUlds().get(lastUld).getSerialNumber());
        }

        TemperatureHandling plannedTemp = null;
        String group = null;
        if (piece.getSpecialHandlings() != null) {
            for (SpecialHandling sh: piece.getSpecialHandlings()) {
                /* Compares the value of each special handling code with the group and temperature codes list and sets the planned temperature or group,
            depending on whether it is a temperature or group code */
                if (GROUP_CODES.contains(sh.getCode())) {
                    if (group == null) {
                        group = sh.getCode();
                    } else {
                        group = group.concat(", ").concat(sh.getCode());
                    }
                }
                try {
                    TemperatureHandling.valueOf(sh.getCode());
                    if (TEMPERATURE_CODES.contains(TemperatureHandling.valueOf(sh.getCode()))) {
                        int tempIndex = TEMPERATURE_CODES.indexOf(TemperatureHandling.valueOf(sh.getCode()));
                        if (plannedTemp == null) {
                            plannedTemp = TEMPERATURE_CODES.get(tempIndex);
                        } else {
                            plannedTemp = TemperatureHandling.valueOf(plannedTemp.toString().concat(", ").concat(sh.getCode()));
                        }
                    }
                } catch (IllegalArgumentException ex) {
                    // do nothing
                }
                pieceDetails.setPlannedTemp(plannedTemp);
                pieceDetails.setGroup(group);
            }
        }
        // Add the position (={@link PieceDetails}) to the position table and return it
        return this.getPosWithItemAttributes(pieceDetails, piece);
    }

    /**
     * Delivers the position including the attributes of the items of a piece for a pieceDetails.
     *
     * @param pieceDetails the {@link PieceDetails} positions table entry.
     * @param piece the piece of the shipment
     * @return the position containing the attributes of the items
     */
    private PieceDetails getPosWithItemAttributes(PieceDetails pieceDetails, Piece piece) {
        PieceDetailsValidationResult validResult = new PieceDetailsValidationResult();

        Float weight = Float.valueOf(0);
        Float quantity = Float.valueOf(0);
        if (piece.getContainedItems() != null) {
            // Accumulate the individual weight and quantity values over all items of a piece
            for (Item item: piece.getContainedItems()) {
                if (item.getWeight().getAmount() != null) {
                    weight += item.getWeight().getAmount();
                }
                if (item.getQuantity().getAmount() != null) {
                    quantity += item.getQuantity().getAmount();
                }
            }

            Value weightValue = new Value();
            weightValue.setAmount(weight);
            // It is assumed that the weight unit for all items of a piece is identical
            if (piece.getContainedItems().get(0).getWeight().getUnit() == null) {
                validResult.setWeight(ValidationState.MISSING);
            } else {
                weightValue.setUnit(piece.getContainedItems().get(0).getWeight().getUnit());
                validResult.setWeight(ValidationState.VALID);
            }

            pieceDetails.setWeight(weightValue);
            Value quantityValue = new Value();
            if (piece.getContainedItems().get(0).getQuantity().getUnit() == null) {
                validResult.setQuantity(ValidationState.MISSING);
            } else {
                // It is assumed that the quantity unit for all items of a piece is identical
                quantityValue.setUnit(piece.getContainedItems().get(0).getQuantity().getUnit());
                validResult.setQuantity(ValidationState.VALID);
            }
            quantityValue.setAmount(quantity);
            pieceDetails.setQuantity(quantityValue);

            pieceDetails.setGoodType(piece.getContainedItems().get(0).getProduct().getCommodityName());

            if (piece.getContainedItems().get(0).getProduct().getHsCode() == null) {
                validResult.setHsCode(ValidationState.MISSING);
            } else {
                pieceDetails.setHsCode(piece.getContainedItems().get(0).getProduct().getHsCode());
                validResult.setHsCode(ValidationState.VALID);
            }
            if (piece.getContainedItems().get(0).getProduct().getEppoCode() == null) {
                validResult.setEppoCode(ValidationState.MISSING);
            } else {
                pieceDetails.setEppoCode(piece.getContainedItems().get(0).getProduct().getEppoCode());
                validResult.setEppoCode(ValidationState.VALID);
            }

            if (piece.getContainedItems().get(0).getProductionCountry() == null ||
                    piece.getContainedItems().get(0).getProductionCountry().getCountryName() == null) {
                validResult.setCountryOfOrigin(ValidationState.MISSING);
            } else {
                validResult.setCountryOfOrigin(ValidationState.VALID);
                pieceDetails.setCountry(piece.getContainedItems().get(0).getProductionCountry().getCountryName());
            }
        } else {
            validResult.setCountryOfOrigin(ValidationState.MISSING);
            validResult.setEppoCode(ValidationState.MISSING);
            validResult.setHsCode(ValidationState.MISSING);
            validResult.setWeight(ValidationState.MISSING);
            validResult.setQuantity(ValidationState.MISSING);
        }
        pieceDetails.setValidationResult(validResult);
        return pieceDetails;
    }

    /**
     * Creates a {@link TimeWithDescription} for the TimeSequences of the shipment details view.
     *
     * @param timeDescription the given description
     * @param time the given date time
     * @return the {@link TimeWithDescription} for the description and time passed
     */
    private TimeWithDescription getTimeWithDescription(String timeDescription, Date time) {
        TimeWithDescription td = new TimeWithDescription();
        td.setTimeDescription(timeDescription);
        td.setTime(time);
        return td;
    }
}

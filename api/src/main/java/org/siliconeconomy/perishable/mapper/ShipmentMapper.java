// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.BookingOption;
import org.siliconeconomy.perishable.models.BookingOptionRequest;
import org.siliconeconomy.perishable.models.ExternalReference;
import org.siliconeconomy.perishable.models.MovementTimes;
import org.siliconeconomy.perishable.models.Party;
import org.siliconeconomy.perishable.models.PieceDocumentState;
import org.siliconeconomy.perishable.models.PieceDocumentType;
import org.siliconeconomy.perishable.models.PieceDocumentValidationState;
import org.siliconeconomy.perishable.models.SequenceDescription;
import org.siliconeconomy.perishable.models.Shipment;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.siliconeconomy.perishable.models.ShipmentDetailsValidationResult;
import org.siliconeconomy.perishable.models.ShipmentInfo;
import org.siliconeconomy.perishable.models.TimeSequence;
import org.siliconeconomy.perishable.models.TransportMovement;
import org.siliconeconomy.perishable.models.ValidationState;
import org.siliconeconomy.perishable.services.PieceDocumentsService;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentStatusDeterminer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper class for shipments.
 *
 * @author Sascha Wüster
 */
@Component
public class ShipmentMapper {
	private Integer lastUldNumber;
	@Autowired
	private ShipmentMapperUtils shipmentMapperUtils;
	@Autowired
	private PiecesRepositoryService pieceService;
	@Autowired
	private PieceDocumentsService pieceDocumentsService;
	@Autowired
	private ShipmentStatusDeterminer shipmentStatusDeterminer;

	public static final String ESTIMATED = "estimated";
	private static final String CONSIGNOR = "Consignor";

	/**
	 * Maps the {@link ShipmentWrapper}s from the database to the
	 * {@link ShipmentInfo}s of the api.
	 *
	 * @param shipments the {@link ShipmentWrapper}s from the database
	 * @return the {@link ShipmentInfo}s of the api
	 */
	public List<ShipmentInfo> mapShipmentsToShipmentInfos(List<ShipmentWrapper> shipments) {
		List<ShipmentInfo> shipmentInfos = new ArrayList<>();

		for (ShipmentWrapper wrapper : shipments) {
			Shipment shipment = wrapper.getData();
			Piece firstPiece = pieceService.findOne(shipment.getPieces().get(0));
			ShipmentInfo shipmentInfo = new ShipmentInfo();
			shipmentInfo.setId(wrapper.getId());
			shipmentInfo.setGoodType(shipment.getGoodsDescription());
			shipmentInfo.setActualWeight(shipment.getTotalGrossWeight());
			shipmentInfo.setUlds(firstPiece.getUlds().size());

			setLastUld(firstPiece);

			/* Set the pick up date */
			shipmentInfo.setPickUpDate(getPickUpDate(firstPiece));
			shipmentInfo.setDeliveryDate(shipment.getDeliveryDate());

			/* Set the delivery location */
			ShipmentAgent deliveryLocation = new ShipmentAgent();
			deliveryLocation.setAddress(shipment.getDeliveryLocation().getAddress());
			deliveryLocation.setName(shipment.getDeliveryLocation().getName());
			deliveryLocation.setRole("Place of Destination");
			shipmentInfo.setDeliveryLocation(deliveryLocation);

			/* Set the shipper */
			ShipmentAgent consignor = new ShipmentAgent();
			consignor.setAddress(shipment.getShipper().getCompanyBranch().getLocation().getAddress());
			consignor.setName(shipment.getShipper().getCompanyName());
			consignor.setRole(CONSIGNOR);
			shipmentInfo.setShipper(consignor);

			/*
			 * Set the flightNo to the flightNo of the last flight (latest scheduled time
			 * departure)
			 */
			TransportMovement latestFlight = shipmentMapperUtils.getLastFlight(firstPiece, this.lastUldNumber);

			shipmentInfo.setFlightNo(latestFlight.getTransportIdentifier());
			shipmentInfo.setOrg(latestFlight.getDepartureLocation());

			for (MovementTimes mt : latestFlight.getMovementTimes()) {
				if (mt.getTimeType().equalsIgnoreCase(ShipmentMapperUtils.SCHEDULED)
						&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.ARRIVAL)) {
					shipmentInfo.setSta(mt.getMovementTimeStamp());
				}
				if (mt.getTimeType().equalsIgnoreCase(ESTIMATED)
						&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.ARRIVAL)) {
					shipmentInfo.setEta(mt.getMovementTimeStamp());
				}
			}

			// validate shipment status and set to the shipment status info
			ShipmentDetails shipmentDetails = mapShipmentToShipmentDetails(wrapper);
			shipmentInfo.setShipmentStatus(shipmentDetails.getShipmentStatus());

			shipmentInfos.add(shipmentInfo);
		}
		return shipmentInfos;
	}

	/**
	 * Maps the {@link ShipmentWrapper} from the database to the
	 * {@link ShipmentDetails} of the api.
	 *
	 * @param wrapper the {@link ShipmentWrapper} from the database
	 * @return the {@link ShipmentDetails} from the api
	 */
	public ShipmentDetails mapShipmentToShipmentDetails(ShipmentWrapper wrapper) {
		Shipment shipment = wrapper.getData();
		Piece firstPiece = pieceService.findOne(shipment.getPieces().get(0));
		setLastUld(firstPiece);

		ShipmentDetails shipmentDetails = new ShipmentDetails();

		shipmentDetails.setId(wrapper.getId());
		shipmentDetails.setGoodType(shipment.getGoodsDescription());
		shipmentDetails.setActualWeight(shipment.getTotalGrossWeight());

		TransportMovement latestFlight = shipmentMapperUtils.getLastFlight(firstPiece, this.lastUldNumber);

		shipmentDetails.setFlightNo(latestFlight.getTransportIdentifier());
		shipmentDetails.setOrg(latestFlight.getDepartureLocation());

		for (MovementTimes mt : latestFlight.getMovementTimes()) {
			if (mt.getTimeType().equalsIgnoreCase(ShipmentMapperUtils.SCHEDULED)
					&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.ARRIVAL)) {
				shipmentDetails.setSta(mt.getMovementTimeStamp());
			}
			if (mt.getTimeType().equalsIgnoreCase(ESTIMATED)
					&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.ARRIVAL)) {
				shipmentDetails.setEta(mt.getMovementTimeStamp());
			}
			if (mt.getTimeType().equalsIgnoreCase(ShipmentMapperUtils.SCHEDULED)
					&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.DEPARTURE)) {
				shipmentDetails.setStd(mt.getMovementTimeStamp());
			}
			if (mt.getTimeType().equalsIgnoreCase(ESTIMATED)
					&& mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.DEPARTURE)) {
				shipmentDetails.setEtd(mt.getMovementTimeStamp());
			}
		}

		List<Piece> pieces = pieceService.findAllInIds(shipment.getPieces());

		shipmentDetails.setPositions(shipmentMapperUtils.createPosTableFromShipment(pieces, this.lastUldNumber));

		shipmentDetails.setShipmentAgents(getShipmentAgents(wrapper));

		shipmentDetails.setDocuments(getPieceDocuments(pieces));

		// Create the time sequences (blue bar)
		List<TimeSequence> sequences = Stream
				.concat(shipmentMapperUtils.getTimeSequencesFromTransportMovements(firstPiece, this.lastUldNumber)
						.stream(),
						shipmentMapperUtils.getTimeSequencesFromEvents(firstPiece, this.lastUldNumber).stream())
				.collect(Collectors.toList());

		sequences = Stream
				.concat(sequences.stream(),
						shipmentMapperUtils.getTimeSequencesFromAuthoritiesInfos(firstPiece).stream())
				.collect(Collectors.toList());

		shipmentDetails.setTimeSequences(getTimeSequencesInRightOrder(sequences));
		// Validate the shipment for the shipment details view
		shipmentDetails.setValidationResult(validateShipmentDetails(shipmentDetails));

		// validate and set the shipment status
		shipmentDetails.setShipmentStatus(shipmentStatusDeterminer
				.determineShipmentStatus(shipmentDetails.getValidationResult(), wrapper.getMetainformation()));

		return shipmentDetails;
	}

	/**
	 * Sets the lastUld to the index of the last uld from the first piece of
	 * shipment.
	 *
	 * @param firstPiece the first {@link Piece} of the {@link Shipment.pieces} list
	 */
	public void setLastUld(Piece firstPiece) {
		Integer uldSize = firstPiece.getUlds().size();
		this.lastUldNumber = 0;
		if (uldSize >= 1) {
			this.lastUldNumber = uldSize - 1;
		}
	}

	/**
	 * Validates all mandatory fields of a {@link ShipmentDetails} and returns the
	 * {@link ShipmentDetailsValidationResult}.
	 *
	 * @param shipmentDetails the {@link ShipmentDetails}
	 * @return the updated validation result
	 */
	public ShipmentDetailsValidationResult validateShipmentDetails(ShipmentDetails shipmentDetails) {
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult = validateShipmentAgents(shipmentDetails, validResult);
		return validateDocuments(shipmentDetails, validResult);
	}

	/**
	 * Validates the mandatory shipment agents of the shipment and returns the
	 * updated {@link ShipmentDetailsValidationResult}.
	 *
	 * @param shipmentDetails the {link ShipmentDetails}
	 * @return the {@link ShipmentDetailsValidationResult}
	 */
	private ShipmentDetailsValidationResult validateShipmentAgents(ShipmentDetails shipmentDetails,
			ShipmentDetailsValidationResult validResult) {
		for (ShipmentAgent agent : shipmentDetails.getShipmentAgents()) {
			if (agent.getRole().equalsIgnoreCase(CONSIGNOR)) {
				validResult.setConsignor(ValidationState.VALID);
			}
			if (agent.getRole().equalsIgnoreCase("Place Of Destination")) {
				validResult.setPlaceOfDestination(ValidationState.VALID);
			}
			if (agent.getRole().equalsIgnoreCase("Consignee")) {
				validResult.setConsignee(ValidationState.VALID);
			}
		}

		if (validResult.getConsignor() == null) {
			validResult.setConsignor(ValidationState.MISSING);
		}
		if (validResult.getConsignee() == null) {
			validResult.setConsignee(ValidationState.MISSING);
		}
		if (validResult.getPlaceOfDestination() == null) {
			validResult.setPlaceOfDestination(ValidationState.MISSING);
		}
		return validResult;
	}

	/**
	 * Validates the mandatory piece documents of the shipment and returns the
	 * updated {@link ShipmentDetailsValidationResult}.
	 *
	 * @param shipmentDetails the {link ShipmentDetails}
	 * @param validResult     the {link ShipmentDetailsValidationResult}
	 * @return the updated {@link ShipmentDetailsValidationResult}
	 */
	private ShipmentDetailsValidationResult validateDocuments(ShipmentDetails shipmentDetails,
			ShipmentDetailsValidationResult validResult) {
		for (PieceDocument doc : shipmentDetails.getDocuments()) {
			validatePhytoCertificate(doc, validResult);
			validateMawb(doc, validResult);
		}
		if (validResult.getPhytoCertificate() == null) {
			validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		}
		if (validResult.getMawb() == null) {
			validResult.setMawb(PieceDocumentValidationState.MISSING);
		}
		return validResult;
	}

	/**
	 * Validates the phyto certificate of the shipment.
	 *
	 * @param doc         the {@link PieceDocument}
	 * @param validResult the {link ShipmentDetailsValidationResult}
	 */
	private void validatePhytoCertificate(PieceDocument doc, ShipmentDetailsValidationResult validResult) {
		if (doc.getDocumentType() == PieceDocumentType.PHYTOSANITARY_CERTIFICATE) {
			if (doc.getState() == PieceDocumentState.APPROVED) {
				validResult.setPhytoCertificate(PieceDocumentValidationState.VALID);
			} else if (doc.getState() == PieceDocumentState.INCORRECT) {
				validResult.setPhytoCertificate(PieceDocumentValidationState.INCORRECT);
			} else if (doc.getState() == PieceDocumentState.MISSING) {
				validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
			} else {
				validResult.setPhytoCertificate(PieceDocumentValidationState.UNCHECKED);
			}
		}
	}

	/**
	 * Validates the mawb of the shipment.
	 *
	 * @param doc         the {@link PieceDocument}
	 * @param validResult the {link ShipmentDetailsValidationResult}
	 */
	private void validateMawb(PieceDocument doc, ShipmentDetailsValidationResult validResult) {
		if (doc.getDocumentType() == PieceDocumentType.MAWB) {
			if (doc.getState() == PieceDocumentState.APPROVED) {
				validResult.setMawb(PieceDocumentValidationState.VALID);
			} else if (doc.getState() == PieceDocumentState.INCORRECT) {
				validResult.setMawb(PieceDocumentValidationState.INCORRECT);
			} else if (doc.getState() == PieceDocumentState.MISSING) {
				validResult.setMawb(PieceDocumentValidationState.MISSING);
			} else {
				validResult.setMawb(PieceDocumentValidationState.UNCHECKED);
			}
		}
	}

	/**
	 * Delivers the pick up date of the shipment.
	 *
	 * @param firstPiece the first {@link Piece} of the {@link Shipment.pieces} list
	 * @return the pick up date
	 */
	private Date getPickUpDate(Piece firstPiece) {
		Date pickUpDate = null;
		for (TransportMovement tm : firstPiece.getTransportMovements()) {
			if (tm.getTransportMeans().getVehicleType().equalsIgnoreCase("truck")) {
				for (MovementTimes mt : tm.getMovementTimes()) {
					if (mt.getDirection().equalsIgnoreCase(ShipmentMapperUtils.DEPARTURE)) {
						pickUpDate = mt.getMovementTimeStamp();
					}
				}
			}
		}
		return pickUpDate;
	}

	/**
	 * Delivers the carrier, consignee, consignor, export agent and delivery
	 * location for the shipment details view.
	 *
	 * @param shipment the {@link ShipmentWrapper}
	 * @return the {@link ShipmentAgent}s
	 */
	private List<ShipmentAgent> getShipmentAgents(ShipmentWrapper wrapper) {
		List<ShipmentAgent> agents = new ArrayList<>();
		final Shipment shipment = wrapper.getData();
		final String revId = wrapper.getRev();

		for (BookingOptionRequest request : shipment.getBookingOptionRequests()) {
			for (BookingOption option : request.getBookingOptions()) {
				ShipmentAgent carrier = new ShipmentAgent();
				carrier.setRev(revId);
				carrier.setAddress(option.getCompany().getCompanyBranch().getLocation().getAddress());
				carrier.setName(option.getCompany().getCompanyName());
				carrier.setRole("Carrier");
				agents.add(carrier);
			}
		}

		for (Party party : shipment.getParties()) {
			ShipmentAgent consignee = new ShipmentAgent();
			consignee.setRev(revId);
			consignee.setAddress(party.getPartyDetails().getCompanyBranch().getLocation().getAddress());
			consignee.setName(party.getPartyDetails().getCompanyName());
			consignee.setRole(party.getPartyRole());
			agents.add(consignee);
		}

		ShipmentAgent consignor = new ShipmentAgent();
		consignor.setRev(revId);
		consignor.setAddress(shipment.getShipper().getCompanyBranch().getLocation().getAddress());
		consignor.setName(shipment.getShipper().getCompanyName());
		consignor.setRole(CONSIGNOR);
		agents.add(consignor);

		ShipmentAgent exportAgent = new ShipmentAgent();
		exportAgent.setRev(revId);
		exportAgent.setAddress(shipment.getForwarder().getCompanyBranch().getLocation().getAddress());
		exportAgent.setName(shipment.getForwarder().getCompanyName());
		exportAgent.setRole("ExportAgent");
		agents.add(exportAgent);

		ShipmentAgent deliveryLocation = new ShipmentAgent();
		deliveryLocation.setRev(revId);
		deliveryLocation.setAddress(shipment.getDeliveryLocation().getAddress());
		deliveryLocation.setName(shipment.getDeliveryLocation().getName());
		deliveryLocation.setRole("Place of Destination");
		agents.add(deliveryLocation);

		return agents;
	}

	/**
	 * Delivers the document list for the document table of the shipment details
	 * view.
	 *
	 * @param pieces the pieces belonging to a shipment
	 * @return the {@link PieceDocument}s to a shipment
	 */
	private List<PieceDocument> getPieceDocuments(List<Piece> pieces) {
		List<PieceDocument> documents = new ArrayList<>();

		for (Piece piece : pieces) {
			if (piece.getExternalReferences() != null) {
				for (ExternalReference extRef : piece.getExternalReferences()) {
					// Get the {@link PieceDocument} from the database
					PieceDocument doc = pieceDocumentsService.findOne(extRef.getDocumentLink());
					documents.add(doc);
				}
			}
		}
		return documents;
	}

	/**
	 * Delivers the time sequences in the right order.
	 *
	 * @param sequences the time sequences
	 * @return the time sequences in the right order
	 */
	private List<TimeSequence> getTimeSequencesInRightOrder(List<TimeSequence> sequences) {
		List<TimeSequence> firstTs = new ArrayList<>();
		List<TimeSequence> lastTs = new ArrayList<>();

		for (TimeSequence ts : sequences) {
			if (ts.getSequenceDescription() == SequenceDescription.PICK_UP
					|| ts.getSequenceDescription() == SequenceDescription.DELIVERY_ADDRESS) {
				lastTs.add(ts);
			} else {
				firstTs.add(ts);
			}
		}
		return Stream.concat(firstTs.stream(), lastTs.stream()).collect(Collectors.toList());
	}
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.services.PieceDocumentsRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentMetaInfoUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller for {@link PieceDocument}s.
 * 
 * @author Sascha Wüster
 */
@RestController
@RequestMapping(path = "/api/v1/piece-document")
public class PieceDocumentsController {
	@Autowired
	private PieceDocumentsRepositoryService pieceDocumentsService;

	@Autowired
	private ShipmentMetaInfoUpdater shipmentMetaInfoUpdater;

	/**
	 * Over this endpoint, a document of an piece can be updated. The document is
	 * identified by the "documentLink" inside the "ref".
	 * 
	 * @param doc        the {@link PieceDocument}
	 * @param shipmentId the shipment where this piece document belongs to
	 * @return the updated pieceDocument entity
	 */
	@PatchMapping
	public PieceDocument updatePieceDocument(@RequestBody PieceDocument doc,
			@RequestParam("shipmentId") final String shipmentId) {

		shipmentMetaInfoUpdater.updateExportFlag(shipmentId, false);

		return pieceDocumentsService.update(doc);
	}
}

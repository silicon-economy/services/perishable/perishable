// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller for {@link Piece}s.
 * 
 * @author Sascha Wüster
 */
@RestController
@RequestMapping(path = "/api/v1/piece")
public class PiecesController {
    @Autowired
    private PiecesRepositoryService piecesRepositoryService;
    
    @GetMapping(path = "/{id}")
    public Piece getPieceForId(@PathVariable String id) {
        return piecesRepositoryService.findOne(id);
    }
}

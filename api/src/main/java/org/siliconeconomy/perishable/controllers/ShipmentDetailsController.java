// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.mapper.ShipmentMapper;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentMetaInfoUpdater;
import org.siliconeconomy.perishable.services.ShipmentsWrapperRepositoryService;
import org.siliconeconomy.perishable.word.WordDocumentGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller for {@link ShipmentDetails}.
 *
 * @author Sascha Wüster
 */
@RestController
@RequestMapping(path = "/api/v1/shipmentDetails")
public class ShipmentDetailsController {

    @Autowired
    private WordDocumentGenerator documentGenerator;

    @Autowired
    private ShipmentsWrapperRepositoryService shipmentsService;
    @Autowired
    private PiecesRepositoryService piecesService;
    @Autowired
    private ShipmentMapper shipmentMapper;
    
    @Autowired
    private ShipmentMetaInfoUpdater shipmentMetaInfoUpdater;

    @Autowired
    private ShipmentsController shipmentController;
    
    @GetMapping(path = "/{id}")
    public ShipmentDetails getShipmentDetailsForId(@PathVariable String id) {
        return shipmentMapper.mapShipmentToShipmentDetails(shipmentsService.findOne(id));
    }

    @PutMapping(path = "/{id}/shipmentAgent")
    public ResponseEntity<ShipmentAgent> updateShipmentAgent(@PathVariable String id, @RequestBody ShipmentAgent agent) {
        ShipmentAgent result = shipmentsService.updateShipmentAgent(id, agent);
        ResponseEntity<ShipmentAgent> resp = null;
        if (result == null) {
            resp = ResponseEntity.status(HttpStatus.CONFLICT).build();
        } else {
            resp = ResponseEntity.ok(result);
        }
        
        shipmentMetaInfoUpdater.updateExportFlag(id, false);
        
        return resp;
    }

    @PutMapping(path = "/{id}/{revId}/pieceDetails")
    public ResponseEntity<PieceDetails> updatePieceDetails(@PathVariable String id, @PathVariable String revId, @RequestBody PieceDetails pieceDetails) {
        PieceDetails result = piecesService.updatePiece(pieceDetails, id, revId);
        ResponseEntity<PieceDetails> resp = null;
        if (result == null) {
            resp = ResponseEntity.status(HttpStatus.CONFLICT).build();
        } else {
            resp = ResponseEntity.ok(result);
        }
        
        shipmentMetaInfoUpdater.updateExportFlag(id, false);
        
        return resp;
    }

    private static final String MEDIA_TYPE_ZIP = "application/zip";

    @GetMapping(path = "/{id}/predeclaration", produces = MEDIA_TYPE_ZIP)
    public ResponseEntity<byte[]> getPredeclarationForShipment(@PathVariable String id) throws IOException {
        ShipmentDetails shpDetail = shipmentMapper.mapShipmentToShipmentDetails(shipmentsService.findOne(id));

        String predeclarationFile = String.format("%s#Predeclaration.docx", shpDetail.getId());
        String predeclarationZipfile = String.format("%s#Predeclaration.zip", shpDetail.getId());

        String awb = String.format(("%s"), shpDetail.getId());

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        try ( ZipOutputStream zipStream = new ZipOutputStream(ostream)) {
            zipStream.putNextEntry(new ZipEntry(predeclarationFile));
            zipStream.write(documentGenerator.generateDocument(shpDetail));
            for (PieceDocument doc : shpDetail.getDocuments()) {
                String docFilename = new File(doc.getRef().getDocumentLink()).getName();
                zipStream.putNextEntry(new ZipEntry(docFilename));
                zipStream.write(shipmentController.getDocumentData(awb, docFilename));
            }
        }

        ContentDisposition disposition = ContentDisposition
                .builder("attachment")
                .filename(predeclarationZipfile)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(disposition);
        headers.setContentType(MediaType.valueOf(MEDIA_TYPE_ZIP));
        
        // update the export flag for the given shipment id
        shipmentMetaInfoUpdater.updateExportFlag(id, true);
        
        return new ResponseEntity<>(ostream.toByteArray(), headers, HttpStatus.OK);
    }
}

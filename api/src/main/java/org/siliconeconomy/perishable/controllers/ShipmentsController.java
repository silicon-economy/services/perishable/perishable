// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import java.util.List;

import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.mapper.ShipmentMapper;
import org.siliconeconomy.perishable.models.ShipmentInfo;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentsWrapperRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

/**
 * Rest Controller for shipments.
 *
 * @author swuester
 */
@RestController
@RequestMapping(path = "/api/v1/shipment")
public class ShipmentsController {

    @Autowired
    private ShipmentsWrapperRepositoryService shipmentsService;
    @Autowired
    private PiecesRepositoryService piecesService;

    @Value("${couchdb.client.url}")
    private String dbUrl;

    @Value("${couchdb.client.username}")
    private String username;

    @Value("${couchdb.client.password}")
    private String password;

    @Value("${perishable.couchdb.dbname}")
    private String dbname;

    @Autowired
    private RestTemplate template;
    @Autowired
    private ShipmentMapper shipmentMapper;

    @GetMapping
    public List<ShipmentInfo> getAll() {
        return shipmentMapper.mapShipmentsToShipmentInfos(shipmentsService.findAll());
    }

    @GetMapping(path = "/{id}")
    public ShipmentWrapper getShipmentForId(@PathVariable String id) {
        return shipmentsService.findOne(id);
    }

    @GetMapping(path = "/{id}/piece")
    public List<Piece> getAllPiecesForCertainShipment(@PathVariable String id) {
        return piecesService.findAllInIds(shipmentsService.findOne(id).getData().getPieces());
    }

    private String getUrlForDocument(String awb, String filename) {
        return String.format("%s%s/%s/%s", dbUrl, dbname, awb, filename);
    }

    public byte[] getDocumentData(String awb, String filename) {
        String url = getUrlForDocument(awb, filename);
        if (url.startsWith(dbUrl)) {
            byte[] credsBytes = Base64Utils.encode(String.format("%s:%s", username, password).getBytes());

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + new String(credsBytes));
            HttpEntity<String> request = new HttpEntity<>(headers);
            ResponseEntity<byte[]> responseEntity = template.exchange(url, HttpMethod.GET, request, byte[].class);
            return responseEntity.getBody();
        }
        return new byte[]{};
    }

    @GetMapping(path = "/{awb}/{filename}", produces = {"application/pdf"})
    public ResponseEntity<byte[]> getDocumentForShipment(@PathVariable String awb, @PathVariable String filename) {
        byte[] data = getDocumentData(awb, filename);

        if (data.length >0) {
            return ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=\"" + filename + "\"")
                    .body(data);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid awb or filename");
        }

    }
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.groocraft.couchdb.slacker.annotation.EnableCouchDbRepositories;

@SpringBootApplication
@EnableCouchDbRepositories("org.siliconeconomy.perishable.repositories")
public class PerishableApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerishableApplication.class, args);
	}
}

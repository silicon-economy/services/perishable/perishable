// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.configuration;

import org.springframework.context.annotation.Configuration;

import com.groocraft.couchdb.slacker.configuration.CouchSlackerConfiguration;

@Configuration
public class AppConfig extends CouchSlackerConfiguration {

}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Information for a country.
 *
 * @author swuester, Marc Dickmann
 */
@Data
public class Country {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;

    /**
     * The code of the country.
     */
    @NotNull
    private String countryCode;

    /**
     * The name of the country.
     */
    @NotNull
    private String countryName;

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        return strBuilder
                .append("countryCode: ")
                .append(this.countryCode)
                .append("\ncountryName: ")
                .append(this.countryName)
                .toString();
    }
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

/**
 * The state of a PieceDocument validation. Extends the enum ValidationState. 
 * Its unchecked whenever the document state is unset.
 * 
 * @author Sascha Wüster
 */
public enum PieceDocumentValidationState {
    VALID, INCORRECT, MISSING, UNCHECKED;
}

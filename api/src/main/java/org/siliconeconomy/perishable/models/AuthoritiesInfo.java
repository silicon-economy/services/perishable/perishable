// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Contains information for authorities information of a certain type.
 * 
 * @author Sascha Wüster
 */
@Data
public class AuthoritiesInfo implements Comparable<AuthoritiesInfo> {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The type of the authorities information.
     */
    @NotNull
    private String type;
    
    /**
     * The time on which the information has been released.
     */
    @NotNull
    private Date releaseTime;
    
    /**
     * The status of the approval.
     */
    @NotNull
    private ApprovalStatus approvalStatus;
    
    /**
     * This object represents a piece of a shipment part.
     */
    private String piece;
    
    @Override
    public int compareTo(AuthoritiesInfo o) {
      return getReleaseTime().compareTo(o.getReleaseTime());
    }
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

/**
 * The different sequences occuring during a timesequence:
 * 
 * EXPORT_WAREHOUSE - exporting to warehouse
 * OFF_BLOCKS - leaving the blocks
 * ON_BLOCKS - arriving at blocks
 * GROUND_HANDLER - at ground handler
 * IMPORT_WAREHOUSE - at import warehouse
 * RELEASES - released documents
 * CUSTOMS - custom documents
 * PICK_UP - at pick up
 * DELIVERY_ADDRESS - at delivery address
 * 
 * @author swuester
 */
public enum SequenceDescription {
    EXPORT_WAREHOUSE, OFF_BLOCKS, ON_BLOCKS, GROUND_HANDLER, IMPORT_WAREHOUSE, RELEASES, CUSTOMS, PICK_UP, DELIVERY_ADDRESS
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import lombok.Data;

/**
 * A company which has a certain role during a shipment. Used for {@link ShipmentInfo} and {@link PieceDetails}.
 *
 * @author swuester, Marc Dickmann
 */
@Data
public class ShipmentAgent {
    /**
     * The revision id of the shipment
     */
    private String rev;

    /**
     * The role of the company during the shipment.
     */
    private String role;

    /**
     * The name of the shipment agent.
     */
    private String name;

    /**
     * The address of the shipment agent.
     */
    private Address address;

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        return strBuilder
                .append("role: ")
                .append(this.role)
                .append("\nname: ")
                .append(this.name)
                .append('\n')
                .append(this.address)
                .toString();
    }
}

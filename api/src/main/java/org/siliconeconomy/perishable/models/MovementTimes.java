// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Containing the movement times of a single transport movement
 * 
 * @author swuester
 */
@Data
public class MovementTimes implements Comparable<MovementTimes> {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri; 
    
    /**
     * Information on how the time has been calculated.
     */
    @NotNull
    private String timeType;
    
    /**
     * The direction of the movement.
     */
    @NotNull
    private String direction;
    
    /**
     * The time of the movement.
     */
    private Date movementTimeStamp;
    
    /**
     * Describing a special handling action for a piece.
     */
    private TransportMovement transportMovement;
    
    @Override
    public int compareTo(MovementTimes o) {
      return getMovementTimeStamp().compareTo(o.getMovementTimeStamp());
    }
}

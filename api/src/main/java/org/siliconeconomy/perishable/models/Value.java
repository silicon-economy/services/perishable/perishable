// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * A value for a certain measurement.
 * 
 * @author swuester
 */
@Data
public class Value {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The unit of the value.
     */
    @NotNull
    private String unit;
    
    /**
     * The amount of units.
     */
    @NotNull
    private Float amount;
}

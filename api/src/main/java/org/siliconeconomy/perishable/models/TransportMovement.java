// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.List;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Describing a special handling action for a piece.
 * 
 * @author swuester
 */
@Data
public class TransportMovement implements Comparable<TransportMovement> {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The location of the departure.
     */
    private String departureLocation;
    
    /**
     * The location of the arrival.
     */
    private String arrivalLocation;
    
    /**
     * The MovementTimes belonging to the transport movement.
     */
    private List<MovementTimes> movementTimes;
    
    /**
     * The means of a certain transport movement.
     */
    private TransportMeans transportMeans;
    
    /**
     * The official identifier for the transport movement.
     */
    private String transportIdentifier;
    
    /**
     * The ULDs.
     */
    private List<ULD> ulds;
    
    /**
     * The pieces belonging to the transport movement.
     */
    private List<String> pieces;
    
    @Override
    public int compareTo(TransportMovement o) {
        return getMovementTimes().get(0).compareTo(o.getMovementTimes().get(0));
    }
}

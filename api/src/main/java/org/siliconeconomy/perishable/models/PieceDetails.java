// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import lombok.Data;

/**
 * A position of goods in a shipment. Used for ShipmentDetails.
 * 
 * @author swuester
 */
@Data
public class PieceDetails {
    
    /**
     * The identifier of the piece.
     */
    private String id;
    
    /**
     * ULD identification code consisting of 10 digits (combination of letters and numbers). Not editable.
     */
    private String uldId;
    
    /**
     * The hs code. Taken from product.
     */
    private String hsCode;
    
    /**
     * The eppo code. Taken from product.
     */
    private String eppoCode;
    
    /**
     * The type of the goods. Taken from goodDescription. When they are more than one, use MIXED.
     */
    private String goodType;
    
    /**
     * The production country. Taken from piece.
     */
    private String country;
    
    /**
     * The packing which with the goods have been packed. Taken from PackagingType.
     */
    private String packaging;
    
    /**
     * The packaging code for the packaging. Taken from PackagingType.
     */
    private String packagingCode;
    
    /**
     * Temperature handling the shipment is supposed to keep. Taken from SpecialHandling. Not editable.
     */
    private TemperatureHandling plannedTemp;
    
    /**
     * Code for the type of cargo. Taken from SpecialHandling.
     */
    private String group;
    
    /**
     * The weight of a piece.
     */
    private Value weight;
    
    /**
     * The quantity of a piece.
     */
    private Value quantity;
    
    /**
     * Container for a multitude of validation results of an PieceDetails object (aka position).
     */
    private PieceDetailsValidationResult validationResult;
}

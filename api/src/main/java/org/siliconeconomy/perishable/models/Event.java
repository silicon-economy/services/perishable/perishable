// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Describing an event during the transport of a piece.
 * 
 * @author swuester
 */
@Data
public class Event implements Comparable<Event> {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * Describes the type of the event.
     */
    @NotNull
    private String eventTypeIndicator;
    
    /**
     * The date and time of the certain event.
     */
    @NotNull
    private Date dateTime;
    
    /**
     * Contains information about a company.
     */
    private Company performedBy;
    
    @Override
    public int compareTo(Event o) {
      return getDateTime().compareTo(o.getDateTime());
    }
}

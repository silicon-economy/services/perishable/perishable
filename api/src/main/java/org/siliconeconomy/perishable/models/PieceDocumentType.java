// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

/**
 * The internal type of an PieceDocument.
 * 
 * @author Sascha Wüster
 */
public enum PieceDocumentType {
    OTHER, HEALTH_CERTIFICATE_VET, PHYTOSANITARY_CERTIFICATE, PREFERENTIAL_DOCUMENTS_EUR1, MAWB, HAWB, AIRLINE_MANIFEST, 
    RELEASE_DECLARATION_BY_BLE, DECLARATION_OF_CONFORMITY_BY_BLE, CERTIFICATE_OF_INSPECTION, CATCH_CERTIFICATE
}

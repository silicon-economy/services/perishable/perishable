// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.List;


import lombok.Data;
import org.siliconeconomy.perishable.deserializer.URIDeserializer;

/**
 * Represents a shipment in the database.   
 * A shipment transporting certain goods.
 *
 * @author swuester
 */
@Data
public class Shipment {   
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * Describes the goods transported by the shipment.
     */
    private String goodsDescription;
    
    /**
     * Declared weight of the shipment.
     */
    private Value totalGrossWeight;
    
    /**
     * Date of delivery.
     */
    private Date deliveryDate;
    
    /**
     * Location of delivery.
     */
    private Location deliveryLocation;
    
    /**
     * The shipper company of the shipment.
     */
    private Company shipper;
    
    /**
     * The forwarder company of the shipment.
     */
    private Company forwarder;
    
    /**
     * Further companies along with their roles during the shipment.
     */
    private List<Party> parties;
    
    /**
     * The booking option requests for the shipment leading to the airwaybills.
     */
    private List<BookingOptionRequest> bookingOptionRequests;
    
    /**
     * The pieces belonging to the shipment.
     */
    private List<String> pieces;
    
    
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;
import java.util.List;

import org.siliconeconomy.perishable.entities.PieceDocument;

import lombok.Data;

/**
 * This object represents a shipment as used for the detail view for the frontend.
 *
 * @author swuester
 */
@Data
public class ShipmentDetails {
    /**
     * (Master) Air Waybill. Originally taken from the master Airwaybill. + (House) Air Waybill. Originally taken from the house Airwaybill.
     */
    private String id;

    /**
     * Good type of this shipment.
     */
    private String goodType;

    /**
     * Origin Airport in three letter IATA Code.
     */
    private String org;

    /**
     * Flight number. Taken from the last flight.
     */
    private String flightNo;

    /**
     * Scheduled Time of Departure of flight. Times shall be ordered.
     */
    private Date std;

    /**
     * Estimated Time of Departure of flight. Times shall be ordered.
     */
    private Date etd;

    /**
     * Scheduled Time of arrival of flight.
     */
    private Date sta;

    /**
     * Estimated Time of arrival of flight.
     */
    private Date eta;

    /**
     * Times for the blue list in the frontend, namely for export warehouse, offblocks, onblocks, groundhandler, importwarehouse,
     * releases, customs, pickup and delivery address. Sometimes there are multiple times per list (e.g. in and out of warehouse).
     */
    private List<TimeSequence> timeSequences;

    /**
     * Contains the information for consignor, consignee, carrier, exportAgent and place of destination.
     */
    private List<ShipmentAgent> shipmentAgents;

    /**
     * The pieces from the positions table in the shipment detail view.
     */
    private List<PieceDetails> positions;

    /**
     * The total weight of the shipment.
     */
    private Value actualWeight;

    /**
     * The documents.
     */
    private List<PieceDocument> documents;

    /**
     * Container for a multitude of validation results of an ShipmentDetails object.
     */
    private ShipmentDetailsValidationResult validationResult;

    /**
     *  The status which is changed by clicking on the "export data for pre-delaration" button. It appears as a traffic light in the phyto column.
     */
    private ShipmentStatus shipmentStatus;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;

import lombok.Data;

/**
 * An overview for a shipment transporting certain goods.
 *
 * @author swuester
 */
@Data
public class ShipmentInfo {
    /**
     * (Master) Air Waybill. Originally taken from the master Airwaybill. + (House) Air Waybill. Originally taken from the house Airwaybill.
     */
    private String id;

    /**
     * Good type of this shipment.
     */
    private String goodType;

    /**
     * The total gross weight of the goods transported by the shipment.
     */
    private Value actualWeight;

    /**
     * Number of ulds in the shipment. Has to be counted.
     */
    private Integer ulds;

    /**
     * Origin Airport in three letter IATA Code.
     */
    private String org;

    /**
     * The pick up date of the shipment.
     */
    private Date pickUpDate;

    /**
     * The delivery date of the shipment.
     */
    private Date deliveryDate;

    /**
     * The shipper of the shipment.
     */
    private ShipmentAgent shipper;

    /**
     * The location of the shipment.
     */
    private ShipmentAgent deliveryLocation;

    /**
     * Flight number. Taken from the last flight.
     */
    private String flightNo;

    /**
     * Scheduled Time of arrival of flight.
     */
    private Date sta;

    /**
     * Estimated Time of arrival of flight.
     */
    private Date eta;

    /**
     * Describes the status of the shipment.
     */
    private ShipmentStatus shipmentStatus;
}

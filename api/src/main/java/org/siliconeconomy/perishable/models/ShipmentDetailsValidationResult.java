// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Container for a multitude of validation results of an ShipmentDetails object.
 * 
 * @author Sascha Wüster
 */
@Data
public class ShipmentDetailsValidationResult {
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState consignor;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState consignee;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState placeOfDestination;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private PieceDocumentValidationState phytoCertificate;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private PieceDocumentValidationState mawb;
}

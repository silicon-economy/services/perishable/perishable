// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Containing information for the airwaybill. There can be master- and house airwaybills. a houseairwaybill can belong 
 * to a single masterairwaybill. A masterairwaybill can contain multiple houseairwaybills.
 * 
 * @author swuester
 *
 */
@Data
public class Waybill {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The number for the waybill. (Previously awb[3:])
     */
    @NotNull
    private String waybillNumber;
    
    /**
     * The prefix of the waybill. (Previously awb[:3])
     */
    @NotNull
    private String waybillPrefix;
    
    /**
     * The type of the waybill.
     */
    @NotNull
    private WaybillType waybillType;
    
    /**
     * Links to the Master airwaybill (for house airwaybills).
     */
    private String linkToMaster;
    
    /**
     * Links to the house airwaybills (for master airwaybills).
     */
    private List<String> linksToHouses;
}

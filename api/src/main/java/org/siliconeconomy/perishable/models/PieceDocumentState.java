// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

/**
 * The internal state of an PieceDocument.
 * 
 * @author Sascha Wüster
 */
public enum PieceDocumentState {
    MISSING, INCORRECT, APPROVED
}

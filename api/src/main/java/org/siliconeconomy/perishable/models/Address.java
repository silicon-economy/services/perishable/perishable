// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Describing a certain address with the attributes.
 *
 * @author swuester, Marc Dickmann
 */
@Data
public class Address {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;

    /**
     * The street of the address.
     */
    private String street;

    /**
     * The postal box of the address.
     */
    private String poBox;

    /**
     * The postal code of the address.
     */
    private String postalCode;

    /**
     * The city code of the address.
     */
    private String cityCode;

    /**
     * The city name.
     */
    private String cityName;

    /**
     * The region code of the address.
     */
    private String regionCode;

    /**
     * The region name.
     */
    private String regionName;

    /**
     * The address code type.
     */
    private String addressCodeType;

    /**
     * The address code.
     */
    private String addressCode;

    /**
     * Information for a country.
     */
    @NotNull
    private Country country;

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        return strBuilder
                .append("street: ")
                .append(this.street)
                .append("\npoBox: ")
                .append(this.poBox)
                .append("\npostalCode: ")
                .append(this.postalCode)
                .append("\ncityCode: ")
                .append(this.cityCode)
                .append("\nregionCode: ")
                .append(this.regionCode)
                .append("\nregionName: ")
                .append(this.regionName)
                .append("\naddressCodeType: ")
                .append(this.addressCodeType)
                .append("\naddressCode: ")
                .append(this.addressCode)
                .append('\n')
                .append(this.country)
                .toString();
    }
}

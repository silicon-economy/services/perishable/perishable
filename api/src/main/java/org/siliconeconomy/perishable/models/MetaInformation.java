// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import lombok.Data;


/**
 * Holding the meta information of a shipment.
 * 
 * @author bernd
 */
@Data
public class MetaInformation {
	/**
	 * This flag represents the state if the shipment
	 * was exported.
	 */
	private boolean exported;
}

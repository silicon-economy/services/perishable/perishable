// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Contains the type of the packaging of a piece.
 * 
 * @author swuester
 */
@Data
public class PackagingType {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The code of the packaging type.
     */
    @NotNull
    private String typeCode;
    
    /**
     * The description of the packaging type.
     */
    @NotNull
    private String packagingTypeDescription;
    
    /**
     * Links to the piece to which the packaging type belongs.
     */
    private String piece;
}

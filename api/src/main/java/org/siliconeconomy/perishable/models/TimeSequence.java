// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.List;

import lombok.Data;

/**
 * A time sequence during a shipment. It contains a list of times with their descriptions (the sequence) and the belonging description.
 * 
 * @author swuester
 */
@Data
public class TimeSequence {
    /**
     * A time stamp during a shipment with a belonging description.
     */
    private List<TimeWithDescription> timeWithDescriptions; 
    
    /**
     * The description for the meaning of the time sequence.
     */
    private SequenceDescription sequenceDescription;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * A booking option consisting of a company and an airwaybill.
 * 
 * @author swuester
 */
@Data
public class BookingOption {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * Contains information about a company.
     */
    @NotNull
    private Company company;
    
    /**
     * Containing information for the airwaybill. There can be master- and house airwaybills. 
     * A houseairwaybill can belong to a single masterairwaybill. A masterairwaybill can contain multiple houseairwaybills.
     */
    @NotNull 
    private Waybill waybill;
    
    /**
     * Linking to a booking option.
     */
    private BookingOptionRequest bookingOptionRequest;
}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.Date;

import lombok.Data;

/**
 * A time stamp during a shipment with a belonging description.
 * 
 * @author swuester
 */
@Data
public class TimeWithDescription {
    /**
     * A time stamp during a shipment with a belonging description.
     */
    private Date time;
    
    /**
     * The description for the meaning of the time.
     */
    private String timeDescription;
}

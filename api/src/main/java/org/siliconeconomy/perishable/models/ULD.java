// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.siliconeconomy.perishable.deserializer.URIDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

/**
 * Containing the ULD id.
 * 
 * @author swuester
 */
@Data
public class ULD {
    /**
     * The Uniform Resource Identifier. This can be used for linking other objects to this one.
     */
    @JsonIgnore
    @JsonDeserialize(using = URIDeserializer.class)
    private String uri;
    
    /**
     * The serial ULD id.
     */
    @NotNull
    private String serialNumber;
    
    /**
     * Links to a TransportMovement which can contain the movement times for std, etd, offblocks, onblocks, sta, eta.
     */
    private List<TransportMovement> transportMovements;
    
    /**
     * The events belonging to the ULD.
     */
    private List<Event> events;
    
    /**
     * The pieces belonging to the ULD.
     */
    private List<String> pieces;
}

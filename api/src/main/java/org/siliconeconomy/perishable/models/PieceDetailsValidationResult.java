// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.models;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Container for a multitude of validation results of an PieceDetails object (aka position).
 * 
 * @author Sascha Wüster
 */
@Data
public class PieceDetailsValidationResult {
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState hsCode;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState eppoCode;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState countryOfOrigin;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState quantity;
    
    /**
     * The state of a validation. Its incorrect whenever the value is given but its wrong on a technical level. If the value is missing its declared as missing.
     */
    @NotNull
    private ValidationState weight;
}

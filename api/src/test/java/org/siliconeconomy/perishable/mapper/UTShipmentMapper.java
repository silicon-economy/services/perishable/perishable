// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.Address;
import org.siliconeconomy.perishable.models.Company;
import org.siliconeconomy.perishable.models.CompanyBranch;
import org.siliconeconomy.perishable.models.ExternalReference;
import org.siliconeconomy.perishable.models.Location;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.MovementTimes;
import org.siliconeconomy.perishable.models.PieceDocumentState;
import org.siliconeconomy.perishable.models.PieceDocumentType;
import org.siliconeconomy.perishable.models.PieceDocumentValidationState;
import org.siliconeconomy.perishable.models.Shipment;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.siliconeconomy.perishable.models.ShipmentDetailsValidationResult;
import org.siliconeconomy.perishable.models.ShipmentInfo;
import org.siliconeconomy.perishable.models.ShipmentStatus;
import org.siliconeconomy.perishable.models.TimeSequence;
import org.siliconeconomy.perishable.models.TransportMeans;
import org.siliconeconomy.perishable.models.TransportMovement;
import org.siliconeconomy.perishable.models.ULD;
import org.siliconeconomy.perishable.models.ValidationState;
import org.siliconeconomy.perishable.models.Value;
import org.siliconeconomy.perishable.services.PieceDocumentsService;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentStatusDeterminer;

@ExtendWith(MockitoExtension.class)
class UTShipmentMapper {
	/* class under test */
	@InjectMocks
	private ShipmentMapper shipmentMapper;

	/* dependencies */
	@Mock
	private PiecesRepositoryService pieceService;
	@Mock
	private ShipmentMapperUtils shipmentMapperUtils;
	@Mock
	private PieceDocumentsService pieceDocumentsService;
	@Mock
	private ShipmentStatusDeterminer shipmentStatusDeterminer;

	/* parameters */
	@Mock
	private ShipmentWrapper wrapper;
	@Mock
	private Shipment shipment;
	@Mock
	private Location location;
	@Mock
	private Company company;
	@Mock
	private Value value;
	SimpleDateFormat formatter;
	@Mock
	private Piece piece;
	@Mock
	private List<ULD> ulds;
	@Mock
	private List<TransportMovement> tms;
	@Mock
	private List<TransportMovement> tms2;
	@Mock
	private TransportMovement tm;
	@Mock
	private TransportMovement tm2;
	@Mock
	private CompanyBranch companyBranch;
	@Mock
	private Address address;
	@Mock
	private TransportMeans transportMeans;
	@Mock
	private List<String> piecesIds;
	@Mock
	private ExternalReference extReference;
	@Mock
	private PieceDocument doc;
	@Mock
	private TimeSequence timeSequence;
	private List<ShipmentWrapper> shipments;
	private ShipmentInfo shipmentInfo;
	private ShipmentAgent consignor;
	private ShipmentAgent deliveryLocation;
	private ShipmentAgent exportAgent;
	private List<Piece> pieces;
	private List<ExternalReference> refs;
	private List<MovementTimes> times;
	private ShipmentDetails shipmentDetails;
	private MetaInformation information;
	private static final String REV_ID = "4-b77609412b5390412d020ad52c8cfd9a";

	@BeforeEach
	void setUp() throws ParseException {
		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
		pieces = Arrays.asList(piece, piece);
		tms = Arrays.asList(tm, tm, tm2);
		tms2 = Arrays.asList(tm2, tm2);

		MovementTimes time = new MovementTimes();
		time.setTimeType("scheduled");
		time.setDirection("departure");
		time.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
		MovementTimes time2 = new MovementTimes();
		time2.setTimeType("scheduled");
		time2.setDirection("departure");
		time2.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
		MovementTimes time3 = new MovementTimes();
		time3.setTimeType("scheduled");
		time3.setDirection("arrival");
		time3.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
		MovementTimes time4 = new MovementTimes();
		time4.setTimeType("estimated");
		time4.setDirection("arrival");
		time4.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
		MovementTimes time5 = new MovementTimes();
		time5.setTimeType("estimated");
		time5.setDirection("departure");
		time5.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
		times = Arrays.asList(time, time2, time3, time4, time5);

		refs = Arrays.asList(extReference, extReference);

		consignor = new ShipmentAgent();
		consignor.setRev(REV_ID);
		consignor.setAddress(address);
		consignor.setName("Blumen Werner");
		consignor.setRole("Consignor");

		deliveryLocation = new ShipmentAgent();
		deliveryLocation.setRev(REV_ID);
		deliveryLocation.setAddress(address);
		deliveryLocation.setName("Blumen Risse");
		deliveryLocation.setRole("Place of Destination");

		exportAgent = new ShipmentAgent();
		exportAgent.setRev(REV_ID);
		exportAgent.setAddress(address);
		exportAgent.setName("Blumen Werner");
		exportAgent.setRole("ExportAgent");

		List<TimeSequence> tsList = Arrays.asList(timeSequence, timeSequence, timeSequence);
		List<ShipmentAgent> agents = Arrays.asList(consignor, exportAgent, deliveryLocation);
		shipmentDetails = new ShipmentDetails();
		shipmentDetails.setId("123-456");
		shipmentDetails.setGoodType("Roses");
		shipmentDetails.setActualWeight(value);
		shipmentDetails.setFlightNo("LH4278");
		shipmentDetails.setOrg("MUN");
		shipmentDetails.setSta(formatter.parse("2018-10-20T01:00Z"));
		shipmentDetails.setEta(formatter.parse("2018-10-20T01:00Z"));
		shipmentDetails.setStd(formatter.parse("2018-10-20T01:00Z"));
		shipmentDetails.setEtd(formatter.parse("2018-10-20T01:00Z"));
		shipmentDetails.setDocuments(Arrays.asList(doc, doc, doc, doc));
		shipmentDetails.setPositions(Arrays.asList());
		shipmentDetails.setTimeSequences(tsList);
		shipmentDetails.setShipmentAgents(agents);

		information = new MetaInformation();
		information.setExported(false);
	}

	@Test
	void mapShipmentsToShipmentInfos() throws ParseException {
		// arrange
		wrapper.setMetainformation(information);
		when(wrapper.getData()).thenReturn(shipment);
		when(wrapper.getId()).thenReturn("123-456");
		when(shipment.getGoodsDescription()).thenReturn("Roses");
		when(shipment.getTotalGrossWeight()).thenReturn(value);
		when(shipment.getPieces()).thenReturn(piecesIds);
		when(shipment.getDeliveryLocation()).thenReturn(location);
		when(shipment.getShipper()).thenReturn(company);
		when(shipment.getForwarder()).thenReturn(company);
		when(wrapper.getMetainformation()).thenReturn(information);
		when(piecesIds.get(0)).thenReturn("");
		when(pieceService.findOne("")).thenReturn(piece);
		when(company.getCompanyBranch()).thenReturn(companyBranch);
		when(companyBranch.getLocation()).thenReturn(location);
		when(location.getAddress()).thenReturn(address);
		when(location.getName()).thenReturn("Blumen Risse");
		when(company.getCompanyName()).thenReturn("Blumen Werner");
		when(piece.getUlds()).thenReturn(ulds);
		when(ulds.size()).thenReturn(3);
		when(tm.getMovementTimes()).thenReturn(times);
		when(tm.getTransportIdentifier()).thenReturn("LH4278");
		when(tm.getDepartureLocation()).thenReturn("MUN");
		when(shipmentMapperUtils.getLastFlight(piece, 2)).thenReturn(tm);

		when(shipment.getDeliveryDate()).thenReturn(formatter.parse("2018-10-20T01:00Z"));
		when(tm.getTransportMeans()).thenReturn(transportMeans);
		when(tm2.getTransportMeans()).thenReturn(transportMeans);
		when(transportMeans.getVehicleType()).thenReturn("truck");
		when(piece.getTransportMovements()).thenReturn(tms);

		shipmentInfo = new ShipmentInfo();
		shipmentInfo.setId("123-456");
		shipmentInfo.setGoodType("Roses");
		shipmentInfo.setActualWeight(value);
		shipmentInfo.setUlds(3);
		shipmentInfo.setOrg("MUN");
		shipmentInfo.setPickUpDate(formatter.parse("2018-10-20T01:00Z"));
		shipmentInfo.setDeliveryDate(formatter.parse("2018-10-20T01:00Z"));
		consignor.setRev(null);
		shipmentInfo.setShipper(consignor);
		deliveryLocation.setRev(null);
		shipmentInfo.setDeliveryLocation(deliveryLocation);
		shipmentInfo.setFlightNo("LH4278");
		shipmentInfo.setSta(formatter.parse("2018-10-20T01:00Z"));
		shipmentInfo.setEta(formatter.parse("2018-10-20T01:00Z"));
		shipmentInfo.setShipmentStatus(ShipmentStatus.INCOMPLETE);
		shipments = Arrays.asList(wrapper);
		
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);
		
		when(shipmentStatusDeterminer.determineShipmentStatus(validResult, information))
		.thenReturn(ShipmentStatus.INCOMPLETE);

		// act
		List<ShipmentInfo> returnedList = shipmentMapper.mapShipmentsToShipmentInfos(shipments);

		// assert
		assertThat(returnedList).contains(shipmentInfo);
	}

	@Test
	void mapShipmentToShipmentDetails() throws ParseException {
		// arrange
		when(wrapper.getData()).thenReturn(shipment);
		when(wrapper.getId()).thenReturn("123-456");
		when(wrapper.getRev()).thenReturn(REV_ID);
		when(shipment.getGoodsDescription()).thenReturn("Roses");
		when(shipment.getTotalGrossWeight()).thenReturn(value);
		when(shipment.getPieces()).thenReturn(piecesIds);
		when(shipment.getDeliveryLocation()).thenReturn(location);
		when(shipment.getShipper()).thenReturn(company);
		when(wrapper.getMetainformation()).thenReturn(information);
		when(piecesIds.get(0)).thenReturn("");
		when(pieceService.findOne("")).thenReturn(piece);
		when(pieceService.findAllInIds(piecesIds)).thenReturn(pieces);
		when(company.getCompanyBranch()).thenReturn(companyBranch);
		when(companyBranch.getLocation()).thenReturn(location);
		when(location.getAddress()).thenReturn(address);
		when(location.getName()).thenReturn("Blumen Risse");
		when(company.getCompanyName()).thenReturn("Blumen Werner");
		when(piece.getUlds()).thenReturn(ulds);
		when(ulds.size()).thenReturn(3);
		when(tm.getMovementTimes()).thenReturn(times);
		when(tm.getTransportIdentifier()).thenReturn("LH4278");
		when(tm.getDepartureLocation()).thenReturn("MUN");
		when(shipmentMapperUtils.getLastFlight(piece, 2)).thenReturn(tm);

		List<TimeSequence> tsList = Arrays.asList(timeSequence, timeSequence, timeSequence);
		List<ShipmentAgent> agents = Arrays.asList(consignor, exportAgent, deliveryLocation);

		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.UNCHECKED);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		ShipmentDetails expectedValue = new ShipmentDetails();
		expectedValue.setId("123-456");
		expectedValue.setGoodType("Roses");
		expectedValue.setActualWeight(value);
		expectedValue.setFlightNo("LH4278");
		expectedValue.setOrg("MUN");
		expectedValue.setSta(formatter.parse("2018-10-20T01:00Z"));
		expectedValue.setEta(formatter.parse("2018-10-20T01:00Z"));
		expectedValue.setStd(formatter.parse("2018-10-20T01:00Z"));
		expectedValue.setEtd(formatter.parse("2018-10-20T01:00Z"));
		expectedValue.setShipmentAgents(agents);
		expectedValue.setDocuments(Arrays.asList(doc, doc, doc, doc));
		expectedValue.setTimeSequences(tsList);
		expectedValue.setPositions(Arrays.asList());
		expectedValue.setValidationResult(validResult);
		expectedValue.setShipmentStatus(ShipmentStatus.INCOMPLETE);

		when(shipment.getForwarder()).thenReturn(company);
		when(piece.getExternalReferences()).thenReturn(refs);
		when(shipmentMapperUtils.createPosTableFromShipment(pieces, 2)).thenReturn(Arrays.asList());
		when(shipmentMapperUtils.getTimeSequencesFromTransportMovements(piece, 2))
				.thenReturn(Arrays.asList(timeSequence));
		when(shipmentMapperUtils.getTimeSequencesFromAuthoritiesInfos(piece)).thenReturn(Arrays.asList(timeSequence));
		when(shipmentMapperUtils.getTimeSequencesFromEvents(piece, 2)).thenReturn(Arrays.asList(timeSequence));
		when(extReference.getDocumentLink()).thenReturn("123-456");
		when(pieceDocumentsService.findOne("123-456")).thenReturn(doc);
		when(doc.getDocumentType()).thenReturn(PieceDocumentType.MAWB);

		when(shipmentStatusDeterminer.determineShipmentStatus(validResult, information))
				.thenReturn(ShipmentStatus.INCOMPLETE);

		// act
		ShipmentDetails returnedValue = shipmentMapper.mapShipmentToShipmentDetails(wrapper);

		// assert
		assertThat(returnedValue).isEqualTo(expectedValue);
	}

	@Test
	void validateShipmentDetails_phyto() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.UNCHECKED);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.PHYTOSANITARY_CERTIFICATE);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_mawb() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.UNCHECKED);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.MAWB);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_mawb_valid() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.VALID);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.MAWB);
		when(doc.getState()).thenReturn(PieceDocumentState.APPROVED);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_phyto_valid() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.VALID);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.PHYTOSANITARY_CERTIFICATE);
		when(doc.getState()).thenReturn(PieceDocumentState.APPROVED);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_phyto_missing() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.PHYTOSANITARY_CERTIFICATE);
		when(doc.getState()).thenReturn(PieceDocumentState.MISSING);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_mawb_missing() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.MAWB);
		when(doc.getState()).thenReturn(PieceDocumentState.MISSING);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_phyto_incorrect() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.MISSING);
		validResult.setPhytoCertificate(PieceDocumentValidationState.INCORRECT);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.PHYTOSANITARY_CERTIFICATE);
		when(doc.getState()).thenReturn(PieceDocumentState.INCORRECT);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}

	@Test
	void validateShipmentDetails_mawb_incorrect() throws ParseException {
		// arrange
		ShipmentDetailsValidationResult validResult = new ShipmentDetailsValidationResult();
		validResult.setConsignee(ValidationState.MISSING);
		validResult.setConsignor(ValidationState.VALID);
		validResult.setMawb(PieceDocumentValidationState.INCORRECT);
		validResult.setPhytoCertificate(PieceDocumentValidationState.MISSING);
		validResult.setPlaceOfDestination(ValidationState.VALID);

		when(doc.getDocumentType()).thenReturn(PieceDocumentType.MAWB);
		when(doc.getState()).thenReturn(PieceDocumentState.INCORRECT);

		// act
		ShipmentDetailsValidationResult returnedValue = shipmentMapper.validateShipmentDetails(shipmentDetails);

		// assert
		assertThat(returnedValue).isEqualTo(validResult);
	}
}

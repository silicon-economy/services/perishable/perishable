// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.AuthoritiesInfo;
import org.siliconeconomy.perishable.models.Country;
import org.siliconeconomy.perishable.models.Event;
import org.siliconeconomy.perishable.models.Item;
import org.siliconeconomy.perishable.models.MovementTimes;
import org.siliconeconomy.perishable.models.PackagingType;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.PieceDetailsValidationResult;
import org.siliconeconomy.perishable.models.Product;
import org.siliconeconomy.perishable.models.SequenceDescription;
import org.siliconeconomy.perishable.models.Shipment;
import org.siliconeconomy.perishable.models.SpecialHandling;
import org.siliconeconomy.perishable.models.TemperatureHandling;
import org.siliconeconomy.perishable.models.TimeSequence;
import org.siliconeconomy.perishable.models.TimeWithDescription;
import org.siliconeconomy.perishable.models.TransportMeans;
import org.siliconeconomy.perishable.models.TransportMovement;
import org.siliconeconomy.perishable.models.ULD;
import org.siliconeconomy.perishable.models.ValidationState;
import org.siliconeconomy.perishable.models.Value;

@ExtendWith(MockitoExtension.class)
class UTShipmentMapperUtils {
    /* class under test */
    @InjectMocks
    private ShipmentMapperUtils shipmentMapperUtils;
    
    /* parameters */
    private static final String ULD_ID = "AKE1234ZZ";
    private static final String GROUP_ID = "PER";
    private static final String WEIGHT_UNIT = "kg";
    private SimpleDateFormat formatter;
    private ShipmentWrapper wrapper;
    private Shipment shipment;
    private TransportMovement tm, tm2, tm3, tm4, tm5;
    private Integer lastUldCount;
    private Piece firstPiece;
    private List<Piece> pieces;
    private List<Piece> piecesWithEmpty;
    private Country kenia;
    private PackagingType packagingType;
    
    @BeforeEach
    void setUp() throws ParseException {
        formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
        
        AuthoritiesInfo authInfo = new AuthoritiesInfo();
        authInfo.setType("phytosanitary");
        authInfo.setReleaseTime(formatter.parse("2018-10-12T01:00Z"));
        
        AuthoritiesInfo authInfo2 = new AuthoritiesInfo();
        authInfo2.setType("customs");
        authInfo2.setReleaseTime(formatter.parse("2018-10-21T02:00Z"));
        
        AuthoritiesInfo authInfo3 = new AuthoritiesInfo();
        authInfo3.setType("veterinarian");
        authInfo3.setReleaseTime(formatter.parse("2018-10-20T01:00Z"));
        
        AuthoritiesInfo authInfo4 = new AuthoritiesInfo();
        authInfo4.setType("ble");
        authInfo4.setReleaseTime(formatter.parse("2018-10-21T01:00Z"));
        
        List<AuthoritiesInfo> authInfos = Arrays.asList(authInfo, authInfo2, authInfo3, authInfo4);
        
        Event event = new Event();
        event.setDateTime(formatter.parse("2018-10-19T01:00Z"));
        event.setEventTypeIndicator("Received By Ground Handler");
        
        Event event2 = new Event();
        event2.setDateTime(formatter.parse("2018-10-20T01:00Z"));
        event2.setEventTypeIndicator("Delivered By Ground Handler");
        
        Event event3 = new Event();
        event3.setDateTime(formatter.parse("2018-10-10T01:00Z"));
        event3.setEventTypeIndicator("Accepted By PCF");
        
        Event event4 = new Event();
        event4.setDateTime(formatter.parse("2018-10-12T01:00Z"));
        event4.setEventTypeIndicator("Check time PCF");
        
        Event event5 = new Event();
        event5.setDateTime(formatter.parse("2018-10-19T01:00Z"));
        event5.setEventTypeIndicator("Ready for Inspection PCF");
        
        Event event6 = new Event();
        event6.setDateTime(formatter.parse("2018-10-12T01:04Z"));
        event6.setEventTypeIndicator("Ready for Pick Up");
        
        Event event7 = new Event();
        event7.setDateTime(formatter.parse("2018-10-12T01:05Z"));
        event7.setEventTypeIndicator("Arrival at PCF");
        
        List<Event> events = Arrays.asList(event, event2, event3, event4, event5, event6, event7);
        
        TransportMeans tMeans = new TransportMeans();
        tMeans.setVehicleType("Aircraft");
        
        TransportMeans tMeans2 = new TransportMeans();
        tMeans2.setVehicleType("Warehouse");
        
        TransportMeans tMeans3 = new TransportMeans();
        tMeans3.setVehicleType("Truck");
        
        MovementTimes mt = new MovementTimes();
        mt.setDirection("departure");
        mt.setTimeType("scheduled");
        mt.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
        
        MovementTimes mt2 = new MovementTimes();
        mt2.setDirection("departure");
        mt2.setTimeType("estimated");
        mt2.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
        
        MovementTimes mt4 = new MovementTimes();
        mt4.setDirection("arrival");
        mt4.setTimeType("estimated");
        mt4.setMovementTimeStamp(formatter.parse("2018-10-20T01:00Z"));
        
        MovementTimes mt5 = new MovementTimes();
        mt5.setDirection("departure");
        mt5.setTimeType("scheduled");
        mt5.setMovementTimeStamp(formatter.parse("2018-10-22T01:00Z"));
        
        MovementTimes mt6 = new MovementTimes();
        mt6.setDirection("departure");
        mt6.setTimeType("scheduled");
        mt6.setMovementTimeStamp(formatter.parse("2020-10-20T01:00Z"));
        
        MovementTimes mt8 = new MovementTimes();
        mt8.setDirection("arrival");
        mt8.setTimeType("actual");
        mt8.setMovementTimeStamp(formatter.parse("2018-10-12T01:01Z"));
        
        MovementTimes mt9 = new MovementTimes();
        mt9.setDirection("departure");
        mt9.setTimeType("actual");
        mt9.setMovementTimeStamp(formatter.parse("2018-10-12T01:00Z"));
        
        MovementTimes mt10 = new MovementTimes();
        mt10.setDirection("arrival");
        mt10.setTimeType("actual");
        mt10.setMovementTimeStamp(formatter.parse("2018-10-12T01:03Z"));
        
        MovementTimes mt11 = new MovementTimes();
        mt11.setDirection("departure");
        mt11.setTimeType("actual");
        mt11.setMovementTimeStamp(formatter.parse("2018-10-12T01:02Z"));
        
        MovementTimes mt12 = new MovementTimes();
        mt12.setDirection("arrival");
        mt12.setTimeType("actual");
        mt12.setMovementTimeStamp(formatter.parse("2018-10-12T01:07Z"));
        
        MovementTimes mt13 = new MovementTimes();
        mt13.setDirection("departure");
        mt13.setTimeType("actual");
        mt13.setMovementTimeStamp(formatter.parse("2018-10-12T01:06Z"));
        
        List<MovementTimes> mtList = Arrays.asList(mt, mt2, mt4, mt10, mt11);
        List<MovementTimes> mtList2 = Arrays.asList(mt5, mt2, mt4, mt5, mt6);
        List<MovementTimes> mtList3 = Arrays.asList(mt6, mt2, mt4);
        List<MovementTimes> mtList4 = Arrays.asList(mt8, mt9);
        List<MovementTimes> mtList5 = Arrays.asList(mt8, mt12, mt13);
        
        tm = new TransportMovement();
        tm.setArrivalLocation("FRA");
        tm.setDepartureLocation("NBO");
        tm.setMovementTimes(mtList);
        tm.setTransportIdentifier("LH4278");
        tm.setTransportMeans(tMeans);
        
        tm2 = new TransportMovement();
        tm2.setArrivalLocation("MUN");
        tm2.setDepartureLocation("NYC");
        tm2.setMovementTimes(mtList2);
        tm2.setTransportIdentifier("LH4279");
        tm2.setTransportMeans(tMeans);
        
        tm3 = new TransportMovement();
        tm3.setArrivalLocation("FRA");
        tm3.setDepartureLocation("MUN");
        tm3.setMovementTimes(mtList3);
        tm3.setTransportIdentifier("LH4300");
        tm3.setTransportMeans(tMeans);
        
        tm4 = new TransportMovement();
        tm4.setArrivalLocation("NBO_PCN");
        tm4.setDepartureLocation("NBO_PCN");
        tm4.setMovementTimes(mtList4);
        tm4.setTransportMeans(tMeans2);
        
        tm5 = new TransportMovement();
        tm5.setArrivalLocation("NBO_PCN");
        tm5.setDepartureLocation("FRA_PCF");
        tm5.setMovementTimes(mtList5);
        tm5.setTransportMeans(tMeans3);
        
        List<TransportMovement> tms = Arrays.asList(tm, tm2, tm3, tm4, tm5);
        
        ULD lastUld = new ULD();
        lastUld.setTransportMovements(tms);
        lastUld.setSerialNumber(ULD_ID);
        lastUld.setEvents(events);

        List<ULD> ulds = Arrays.asList(lastUld);
        
        SpecialHandling sh = new SpecialHandling();
        sh.setCode(GROUP_ID);
        sh.setPiece(null);
        
        SpecialHandling sh2 = new SpecialHandling();
        sh2.setCode("PEF");
        sh2.setPiece(null);
        
        SpecialHandling sh3 = new SpecialHandling();
        sh3.setCode("COL");
        sh3.setPiece(null);
        
        List<SpecialHandling> shList = Arrays.asList(sh, sh2, sh3);
        
        Value weight = new Value();
        weight.setUnit(WEIGHT_UNIT);
        weight.setAmount(1200f);
        
        Value quantity = new Value();
        quantity.setUnit("stems");
        quantity.setAmount(10000f);
        
        kenia = new Country();
        kenia.setCountryCode("KE");
        kenia.setCountryName("Kenia");
        
        packagingType = new PackagingType();
        packagingType.setPackagingTypeDescription("Palets");
        packagingType.setPiece(null);
        packagingType.setTypeCode("85341");
        
        Product product = new Product();
        product.setCommodityName("Roses");
        product.setEppoCode("ROSCH");
        product.setHsCode("06031100");
        
        Item item = new Item();
        item.setProduct(product);
        item.setProductionCountry(kenia);
        item.setQuantity(quantity);
        item.setWeight(weight);
        
        Item item2 = new Item();
        item2.setProduct(product);
        item2.setProductionCountry(kenia);
        item2.setQuantity(quantity);
        item2.setWeight(weight);
        
        Item item3 = new Item();
        item3.setProduct(product);
        item3.setProductionCountry(kenia);
        item3.setQuantity(quantity);
        item3.setWeight(weight);
        
        List<Item> items = Arrays.asList(item, item2);
        
        firstPiece = new Piece();
        firstPiece.setId("123");
        firstPiece.setAuthoritiesInfos(authInfos);
        firstPiece.setEvents(events);
        firstPiece.setContainedItems(items);
        firstPiece.setPackagingType(packagingType);
        firstPiece.setSpecialHandlings(shList);
        firstPiece.setTransportMovements(tms);
        firstPiece.setUlds(ulds);
        
        Piece piece2 = new Piece();
        piece2.setId("123");
        piece2.setAuthoritiesInfos(authInfos);
        piece2.setEvents(events);
        piece2.setContainedItems(items);
        piece2.setPackagingType(packagingType);
        piece2.setSpecialHandlings(shList);
        piece2.setTransportMovements(tms);
        piece2.setUlds(ulds);
        
        pieces = Arrays.asList(firstPiece, piece2);
        piecesWithEmpty = Arrays.asList(firstPiece, piece2, new Piece());
        wrapper = new ShipmentWrapper();
        shipment = new Shipment();
        wrapper.setData(shipment);
        wrapper.getData().setGoodsDescription("Roses");
        wrapper.getData().setPieces(Arrays.asList("123","456"));
        
        lastUldCount = 0;
    }
    
    @Test
    void getLastFlight() throws ParseException {
        // act
        TransportMovement returnedValue = shipmentMapperUtils.getLastFlight(firstPiece, lastUldCount);
        
        // assert
        assertThat(returnedValue).isEqualTo(tm3);
    }
    
    @Test
    void getTimeSequencesFromEvent() throws ParseException {
        // arrange
        TimeWithDescription td3 = new TimeWithDescription();
        td3.setTime(formatter.parse("2018-10-10T01:00Z"));
        td3.setTimeDescription("Acceptance");
        
        TimeWithDescription td4 = new TimeWithDescription();
        td4.setTime(formatter.parse("2018-10-12T01:00Z"));
        td4.setTimeDescription("Check time");
        
        TimeWithDescription td5 = new TimeWithDescription();
        td5.setTime(formatter.parse("2018-10-19T01:00Z"));
        td5.setTimeDescription("Ready for inspection");
        
        TimeWithDescription td = new TimeWithDescription();
        td.setTime(formatter.parse("2018-10-19T01:00Z"));
        td.setTimeDescription("Received");
        
        TimeWithDescription td2 = new TimeWithDescription();
        td2.setTime(formatter.parse("2018-10-20T01:00Z"));
        td2.setTimeDescription("Delivered");
        
        List<TimeWithDescription> tdList = Arrays.asList(td, td2);
        List<TimeWithDescription> tdList2 = Arrays.asList(td3, td4, td5);
        
        TimeSequence groundHandler = new TimeSequence();
        groundHandler.setSequenceDescription(SequenceDescription.GROUND_HANDLER);
        groundHandler.setTimeWithDescriptions(tdList);
        
        TimeSequence importWarehouse = new TimeSequence();
        importWarehouse.setSequenceDescription(SequenceDescription.IMPORT_WAREHOUSE);
        importWarehouse.setTimeWithDescriptions(tdList2);
        
        // act
        List<TimeSequence> resultList = shipmentMapperUtils.getTimeSequencesFromEvents(firstPiece, lastUldCount);
        
        // assert
        assertThat(resultList).containsExactlyInAnyOrder(groundHandler, importWarehouse);
    }
    
    @Test
    void getTimeSequencesFromAuthoritiesInfos() throws ParseException {
        // arrange
        TimeWithDescription td = new TimeWithDescription();
        td.setTime(formatter.parse("2018-10-20T01:00Z"));
        td.setTimeDescription("Veterinarian release");

        TimeWithDescription td2 = new TimeWithDescription();
        td2.setTime(formatter.parse("2018-10-21T01:00Z"));
        td2.setTimeDescription("BLE release");

        TimeWithDescription td3 = new TimeWithDescription();
        td3.setTime(formatter.parse("2018-10-21T02:00Z"));
        td3.setTimeDescription("Release");
        
        TimeWithDescription td4 = new TimeWithDescription();
        td4.setTime(formatter.parse("2018-10-12T01:00Z"));
        td4.setTimeDescription("Phytosanitarian release");
        
        List<TimeWithDescription> tdList = Arrays.asList(td3);
        List<TimeWithDescription> tdList2 = Arrays.asList(td4, td, td2);
        
        TimeSequence customs = new TimeSequence();
        customs.setSequenceDescription(SequenceDescription.CUSTOMS);
        customs.setTimeWithDescriptions(tdList);
        
        TimeSequence release = new TimeSequence();
        release.setSequenceDescription(SequenceDescription.RELEASES);
        release.setTimeWithDescriptions(tdList2);
        
        // act
        List<TimeSequence> resultList = shipmentMapperUtils.getTimeSequencesFromAuthoritiesInfos(firstPiece);
        
        // assert
        assertThat(resultList).containsExactlyInAnyOrder(customs, release);
    }
    
    @Test
    void getTimeSequencesFromTransportMovements() throws ParseException {
        // arrange
        TimeWithDescription td = new TimeWithDescription();
        td.setTime(formatter.parse("2018-10-12T01:00Z"));
        td.setTimeDescription("In");

        TimeWithDescription td2 = new TimeWithDescription();
        td2.setTime(formatter.parse("2018-10-12T01:01Z"));
        td2.setTimeDescription("Out");

        TimeWithDescription td3 = new TimeWithDescription();
        td3.setTime(formatter.parse("2018-10-12T01:06Z"));
        td3.setTimeDescription("Departure");
        
        TimeWithDescription td4 = new TimeWithDescription();
        td4.setTime(formatter.parse("2018-10-12T01:05Z"));
        td4.setTimeDescription("Arrival");
        
        TimeWithDescription td5 = new TimeWithDescription();
        td5.setTime(formatter.parse("2018-10-12T01:04Z"));
        td5.setTimeDescription("Ready");
        
        TimeWithDescription td6 = new TimeWithDescription();
        td6.setTime(formatter.parse("2018-10-12T01:00Z"));
        td6.setTimeDescription("");
        
        TimeWithDescription td7 = new TimeWithDescription();
        td7.setTime(formatter.parse("2018-10-12T01:02Z"));
        td7.setTimeDescription("");
        
        TimeWithDescription td8 = new TimeWithDescription();
        td8.setTime(formatter.parse("2018-10-12T01:03Z"));
        td8.setTimeDescription("");
        
        TimeWithDescription td9 = new TimeWithDescription();
        td9.setTime(formatter.parse("2018-10-12T01:07Z"));
        td9.setTimeDescription("Arrival");
        
        List<TimeWithDescription> tdList = Arrays.asList(td, td2);
        List<TimeWithDescription> tdList2 = Arrays.asList(td9);
        List<TimeWithDescription> tdList3 = Arrays.asList(td5, td4, td3);
        List<TimeWithDescription> tdList4 = Arrays.asList(td7);
        List<TimeWithDescription> tdList5 = Arrays.asList(td8);
        
        TimeSequence exportWarehouse = new TimeSequence();
        exportWarehouse.setSequenceDescription(SequenceDescription.EXPORT_WAREHOUSE);
        exportWarehouse.setTimeWithDescriptions(tdList);
        
        TimeSequence pickup = new TimeSequence();
        pickup.setSequenceDescription(SequenceDescription.PICK_UP);
        pickup.setTimeWithDescriptions(tdList3);
        
        TimeSequence deliveryAddress = new TimeSequence();
        deliveryAddress.setSequenceDescription(SequenceDescription.DELIVERY_ADDRESS);
        deliveryAddress.setTimeWithDescriptions(tdList2);
        
        TimeSequence offBlocks = new TimeSequence();
        offBlocks.setSequenceDescription(SequenceDescription.OFF_BLOCKS);
        offBlocks.setTimeWithDescriptions(tdList4);
        
        TimeSequence onBlocks = new TimeSequence();
        onBlocks.setSequenceDescription(SequenceDescription.ON_BLOCKS);
        onBlocks.setTimeWithDescriptions(tdList5);
        
        // act
        List<TimeSequence> resultList = shipmentMapperUtils.getTimeSequencesFromTransportMovements(firstPiece, lastUldCount);
        
        // assert
        assertThat(resultList).containsExactlyInAnyOrder(exportWarehouse, pickup, deliveryAddress, offBlocks, onBlocks);
    }
    
    @Test
    void createPosTableFromShipment() {
        // arrange
        Value weight = new Value();
        weight.setAmount(2400f);
        weight.setUnit(WEIGHT_UNIT);
        
        Value quantity = new Value();
        quantity.setUnit("stems");
        quantity.setAmount(20000f);
        
        PieceDetailsValidationResult validResult = new PieceDetailsValidationResult();
        validResult.setCountryOfOrigin(ValidationState.VALID);
        validResult.setEppoCode(ValidationState.VALID);
        validResult.setHsCode(ValidationState.VALID);
        validResult.setQuantity(ValidationState.VALID);
        validResult.setWeight(ValidationState.VALID);
        
        PieceDetails expectedPiece = new PieceDetails();
        expectedPiece.setId("123");
        expectedPiece.setHsCode("06031100");
        expectedPiece.setEppoCode("ROSCH");
        expectedPiece.setGoodType("Roses");
        expectedPiece.setCountry(kenia.getCountryName());
        expectedPiece.setPackaging(packagingType.getPackagingTypeDescription());
        expectedPiece.setPackagingCode(packagingType.getTypeCode());
        expectedPiece.setUldId(ULD_ID);
        expectedPiece.setPlannedTemp(TemperatureHandling.COL);
        expectedPiece.setGroup(GROUP_ID + ", PEF");
        expectedPiece.setWeight(weight);
        expectedPiece.setQuantity(quantity);
        expectedPiece.setValidationResult(validResult);
        
        // act
        List<PieceDetails> resultList = shipmentMapperUtils.createPosTableFromShipment(pieces, lastUldCount);
        
        // assert
        assertThat(resultList).containsExactlyInAnyOrder(expectedPiece, expectedPiece);
    }
    
    @Test
    void createPosTableFromShipment_invalid_piece() {
        // arrange
        Value weight = new Value();
        weight.setAmount(2400f);
        weight.setUnit(WEIGHT_UNIT);
        
        Value quantity = new Value();
        quantity.setUnit("stems");
        quantity.setAmount(20000f);
        
        PieceDetailsValidationResult validResult = new PieceDetailsValidationResult();
        validResult.setCountryOfOrigin(ValidationState.VALID);
        validResult.setEppoCode(ValidationState.VALID);
        validResult.setHsCode(ValidationState.VALID);
        validResult.setQuantity(ValidationState.VALID);
        validResult.setWeight(ValidationState.VALID);
        
        PieceDetailsValidationResult validResultMissing = new PieceDetailsValidationResult();
        validResultMissing.setCountryOfOrigin(ValidationState.MISSING);
        validResultMissing.setEppoCode(ValidationState.MISSING);
        validResultMissing.setHsCode(ValidationState.MISSING);
        validResultMissing.setQuantity(ValidationState.MISSING);
        validResultMissing.setWeight(ValidationState.MISSING);
        
        PieceDetails expectedPiece = new PieceDetails();
        expectedPiece.setId("123");
        expectedPiece.setHsCode("06031100");
        expectedPiece.setEppoCode("ROSCH");
        expectedPiece.setGoodType("Roses");
        expectedPiece.setCountry(kenia.getCountryName());
        expectedPiece.setPackaging(packagingType.getPackagingTypeDescription());
        expectedPiece.setPackagingCode(packagingType.getTypeCode());
        expectedPiece.setUldId(ULD_ID);
        expectedPiece.setPlannedTemp(TemperatureHandling.COL);
        expectedPiece.setGroup(GROUP_ID + ", PEF");
        expectedPiece.setWeight(weight);
        expectedPiece.setQuantity(quantity);
        expectedPiece.setValidationResult(validResult);
        
        PieceDetails expectedPieceEmpty = new PieceDetails();
        expectedPieceEmpty.setValidationResult(validResultMissing);
        
        // act
        List<PieceDetails> resultList = shipmentMapperUtils.createPosTableFromShipment(piecesWithEmpty, lastUldCount);
        
        // assert
        assertThat(resultList).containsExactlyInAnyOrder(expectedPiece, expectedPiece, expectedPieceEmpty);
    }
}

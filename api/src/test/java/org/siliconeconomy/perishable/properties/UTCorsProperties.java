// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.properties;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UTCorsProperties {
    /* class under test */
    @InjectMocks private CorsProperties corsProperties;
    
    /* dependencies */
    @Mock private List<String> allowedOriginPatterns;
    
    @Test
    void getAllowedOriginPatterns() {
        // act
        List<String> resultList = corsProperties.getAllowedOriginPatterns();
        
        // assert
        assertThat(resultList).isEqualTo(allowedOriginPatterns);
    }
    
    @Test
    void setAllowedOriginPatterns() {
        // act
        corsProperties.setAllowedOriginPatterns(null);
        
        // assert
        assertThat(corsProperties.getAllowedOriginPatterns()).isNull();
    }
}

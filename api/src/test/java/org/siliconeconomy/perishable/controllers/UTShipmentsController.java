// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.Base64;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.mapper.ShipmentMapper;
import org.siliconeconomy.perishable.models.Shipment;
import org.siliconeconomy.perishable.models.ShipmentInfo;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentsWrapperRepositoryService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(locations = "classpath:test.properties")
class UTShipmentsController {

    /* class under test */
    @InjectMocks
    private ShipmentsController shipmentsController;

    /* dependencies */
    @Mock
    private ShipmentsWrapperRepositoryService shipmentsService;
    @Mock
    private ShipmentMapper shipmentMapper;
    @Mock
    private RestTemplate template;
    @Mock
    private PiecesRepositoryService piecesService;

    /* parameters */
    @Mock
    private List<ShipmentWrapper> shipmentEntities;
    @Mock
    private List<ShipmentInfo> shipmentInfos;
    @Mock
    private ShipmentWrapper wrapperEntity;
    @Mock
    private Shipment shipment;    
    @Mock
    private List<String> pieceIds;
    @Mock
    private List<Piece> pieces;

    @Test
    void getAll() {
        // arrange
        when(shipmentMapper.mapShipmentsToShipmentInfos(shipmentEntities)).thenReturn(shipmentInfos);
        when(shipmentsService.findAll()).thenReturn(shipmentEntities);

        // act
        List<ShipmentInfo> returnedList = shipmentsController.getAll();

        // assert
        assertThat(returnedList).isEqualTo(shipmentInfos);
    }

    @Test
    void getShipmentForId() {
        // arrange
        when(shipmentsService.findOne("067-82506471")).thenReturn(wrapperEntity);

        // act
        ShipmentWrapper returnedShipment = shipmentsController.getShipmentForId("067-82506471");

        // assert
        assertThat(returnedShipment).isEqualTo(wrapperEntity);
    }
    
    @Test
    void getAllPiecesForCertainShipment() {
        // arrange
        when(shipmentsService.findOne("067-82506471")).thenReturn(wrapperEntity);
        when(wrapperEntity.getData()).thenReturn(shipment);
        when(shipment.getPieces()).thenReturn(pieceIds);
        when(piecesService.findAllInIds(pieceIds)).thenReturn(pieces);

        // act
        List<Piece> returnedList = shipmentsController.getAllPiecesForCertainShipment("067-82506471");

        // assert
        assertThat(returnedList).isEqualTo(pieces);
    }

    /**
     * Test of getDocumentForShipment method, of class ShipmentsController.
     */
    @Test
    void testGetDocumentForShipment() {
        System.out.println("getDocumentForShipment");

        String empty_pdf = "JVBERi0xLjAKMSAwIG9iajw8L1R5cGUvQ2F0YWxvZy9QYWdlcyAyIDAgUj4+ZW5kb2JqIDIgMCBv"
                + "Ymo8PC9UeXBlL1BhZ2VzL0tpZHNbMyAwIFJdL0NvdW50IDE+PmVuZG9iaiAzIDAgb2JqPDwvVHlw"
                + "ZS9QYWdlL01lZGlhQm94WzAgMCAzIDNdPj5lbmRvYmoKeHJlZgowIDQKMDAwMDAwMDAwMCA2NTUz"
                + "NSBmCjAwMDAwMDAwMTAgMDAwMDAgbgowMDAwMDAwMDUzIDAwMDAwIG4KMDAwMDAwMDEwMiAwMDAw"
                + "MCBuCnRyYWlsZXI8PC9TaXplIDQvUm9vdCAxIDAgUj4+CnN0YXJ0eHJlZgoxNDkKJUVPRgo=";

        byte binBody[]=Base64.getDecoder().decode(empty_pdf);
        String awb = "0815";
        String filename = "empty.pdf";
        ReflectionTestUtils.setField(shipmentsController, "username", "testuser");
        ReflectionTestUtils.setField(shipmentsController, "password", "secret");
        ReflectionTestUtils.setField(shipmentsController, "dbUrl", "http://localhost:5984/");
        ReflectionTestUtils.setField(shipmentsController, "dbname", "testdb");
        when(template.exchange(eq("http://localhost:5984/testdb/0815/empty.pdf"), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class)))
                .thenReturn(ResponseEntity.ok().body(binBody));

        ResponseEntity<byte[]> result = shipmentsController.getDocumentForShipment(awb, filename);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        byte body[]=result.getBody();
        
        assertArrayEquals(binBody,body);

    }
}

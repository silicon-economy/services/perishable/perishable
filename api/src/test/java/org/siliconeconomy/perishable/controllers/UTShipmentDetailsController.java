// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.ZipInputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.mapper.ShipmentMapper;
import org.siliconeconomy.perishable.models.ExternalReference;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentMetaInfoUpdater;
import org.siliconeconomy.perishable.services.ShipmentsWrapperRepositoryService;
import org.siliconeconomy.perishable.word.WordDocumentGenerator;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class UTShipmentDetailsController {
    
    /* class under test */
    @InjectMocks
    private ShipmentDetailsController shipmentDetailsController;
    
    @Mock
    private WordDocumentGenerator documentGenerator;
    
    /* dependencies */
    @Mock
    private ShipmentsWrapperRepositoryService shipmentsRepositoryService;
    @Mock
    private PiecesRepositoryService piecesService;
    @Mock
    private ShipmentMapper shipmentMapper;
    
    /* parameters */
    @Mock
    private ShipmentWrapper shipment;
    @Mock
    private ShipmentAgent agent;
    @Mock

    private ShipmentAgent agentConflict;

    private PieceDetails pieceDetails;

    @Mock
    private ShipmentDetails shipmentDetails;
    @Mock
    private ShipmentsController controller;
    
    @Mock
    private ShipmentMetaInfoUpdater shipmentMetaInfoUpdater;
    
    private static final String id = "AWB11223344-x";
    
    private static final String revId = "rev-789123";
    
    @Test
    void getShipmentDetailsForId() {
        // arrange
        when(shipmentsRepositoryService.findOne(id)).thenReturn(shipment);
        when(shipmentMapper.mapShipmentToShipmentDetails(shipment)).thenReturn(shipmentDetails);
        
        // act
        ShipmentDetails returnedValue = shipmentDetailsController.getShipmentDetailsForId(id);
        
        // assert
        assertThat(returnedValue).isEqualTo(shipmentDetails);
    }
    
    @Test
    void updateShipmentAgent() {
        // arrange
        when(shipmentsRepositoryService.updateShipmentAgent(id, agent)).thenReturn(agent);
        when(shipmentsRepositoryService.updateShipmentAgent(id, agentConflict)).thenReturn(null);
        
        // act
        ResponseEntity<ShipmentAgent> returnedValue = shipmentDetailsController.updateShipmentAgent(id, agent);
        
        // assert
        assertThat(returnedValue.getBody()).isEqualTo(agent);// act
        
        returnedValue = shipmentDetailsController.updateShipmentAgent(id, agentConflict);
        
        // assert
        verify(shipmentMetaInfoUpdater, times(2)).updateExportFlag(id, false);
        assertThat(returnedValue.getBody()).isNull();
    }
    
    @Test
    void updatePieceDetails() {
        // arrange
        when(piecesService.updatePiece(pieceDetails, id,revId)).thenReturn(pieceDetails);
        
        // act
        ResponseEntity<PieceDetails> returnedValue = shipmentDetailsController.updatePieceDetails(id, revId, pieceDetails);
        
        // assert
        verify(shipmentMetaInfoUpdater).updateExportFlag(id, false);
        assertThat(returnedValue.getBody()).isEqualTo(pieceDetails);
    }
    
    @Test
    void getPredeclarationForShipment() throws IOException {
        // arrange
        PieceDocument doc = new PieceDocument();
        String filename = "mock.pdf";
        
        doc.setRef(new ExternalReference());
        doc.getRef().setDocumentLink(String.format("/mock-server/%s",filename));
        
        when(shipmentsRepositoryService.findOne(id)).thenReturn(shipment);
        when(shipmentMapper.mapShipmentToShipmentDetails(shipment)).thenReturn(shipmentDetails);
        when(shipmentDetails.getId()).thenReturn(id);
        when(shipmentDetails.getDocuments()).thenReturn(Arrays.asList(doc));
        when(documentGenerator.generateDocument(any())).thenReturn("intentionally left invalid".getBytes());
        when(controller.getDocumentData(String.format("%s",id), filename)).thenReturn("kein inhalt".getBytes());
        
        // act
        ResponseEntity<byte[]> res= shipmentDetailsController.getPredeclarationForShipment(id);
        
        // assert
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        ContentDisposition disposition=res.getHeaders().getContentDisposition();
        assertThat(disposition.isAttachment()).isTrue();
        assertThat(disposition.getFilename()).contains("AWB11223344");
        assertThat(res.getHeaders().getContentType().toString())
                .hasToString("application/zip");
        ByteArrayInputStream content=new ByteArrayInputStream(res.getBody());
        ZipInputStream zipStream=new ZipInputStream(content);
        assertThat(zipStream.getNextEntry().getName()).hasToString("AWB11223344-x#Predeclaration.docx");
        assertThat(zipStream.getNextEntry().getName()).hasToString(filename);
        zipStream.close();
        
        verify(shipmentMetaInfoUpdater).updateExportFlag(id, true);
    }
}

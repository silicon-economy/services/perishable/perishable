// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.services.PieceDocumentsRepositoryService;
import org.siliconeconomy.perishable.services.ShipmentMetaInfoUpdater;

@ExtendWith(MockitoExtension.class)
class UTPieceDocumentsController {
    
    /* class under test */
    @InjectMocks
    private PieceDocumentsController pieceDocumentsController;
    
    /* dependencies */
    @Mock
    private PieceDocumentsRepositoryService pieceDocumentsService;
    
    @Mock
    private ShipmentMetaInfoUpdater shipmentMetaInfoUpdater;
    
    /* parameters */
    @Mock
    private PieceDocument doc;
    
    @Test
    void updatePieceDocument() {
        // arrange
    	String shipmentId = "SHIPMENT_ID";
        when(pieceDocumentsService.update(doc)).thenReturn(doc);
        
        // act
        PieceDocument returnedValue = pieceDocumentsController.updatePieceDocument(doc, shipmentId);
        
        // assert
        verify(shipmentMetaInfoUpdater).updateExportFlag(shipmentId, false);
        assertThat(returnedValue).isEqualTo(doc);
    }

}

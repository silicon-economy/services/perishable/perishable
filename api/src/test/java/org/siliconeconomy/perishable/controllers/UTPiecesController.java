// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.services.PiecesRepositoryService;

@ExtendWith(MockitoExtension.class)
class UTPiecesController {
    /* class under test */
    @InjectMocks
    private PiecesController piecesController;
    
    /* dependencies */
    @Mock
    private PiecesRepositoryService piecesRepositoryService;
    
    /* parameters */
    @Mock
    private Piece piece;
    
    @Test
    void getPieceForId() {
         // arrange
        when(piecesRepositoryService.findOne("123")).thenReturn(piece);
        
        // act
        Piece returnedValue = piecesController.getPieceForId("123");
        
        // assert
        assertThat(returnedValue).isEqualTo(piece);
    }

}

// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.MetaInformation;

@ExtendWith(MockitoExtension.class)
class UTShipmentMetaInfoUpdater {
	/* class under test */
	@InjectMocks
	private ShipmentMetaInfoUpdater updater;

	/* dependencies */
	@Mock
	private ShipmentsWrapperService shipmentsWrapperService;

	/* test parameters */
	private ShipmentWrapper shipmentWrapper;
	private final String shipmentId = "SHIPMENT_ID";
	private MetaInformation metaInfo;

	@BeforeEach
	void setUp() {
		metaInfo = new MetaInformation();
		shipmentWrapper = new ShipmentWrapper();
		shipmentWrapper.setMetainformation(metaInfo);
		
		when(shipmentsWrapperService.findOne(shipmentId)).thenReturn(shipmentWrapper);
	}

	@Test
	void updateExportFlagToTrue() {
		// act
		updater.updateExportFlag("SHIPMENT_ID", true);

		// assert
		verify(shipmentsWrapperService).update(shipmentWrapper);
	}

	@Test
	void updateExportFlagToFalse() {
		// act
		updater.updateExportFlag("SHIPMENT_ID", false);

		// assert
		verify(shipmentsWrapperService).update(shipmentWrapper);
	}

}

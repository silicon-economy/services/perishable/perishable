// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.any;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.Piece;
import org.siliconeconomy.perishable.exceptions.CouchDbClientException;
import org.siliconeconomy.perishable.models.Country;
import org.siliconeconomy.perishable.models.Item;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.Product;
import org.siliconeconomy.perishable.models.Value;

import com.groocraft.couchdb.slacker.CouchDbClient;

@ExtendWith(MockitoExtension.class)
class UTPiecesRepositoryService {

    /* class under test */
    @InjectMocks
    private PiecesRepositoryService piecesRepositoryService;

    /* dependencies */
    @Mock
    private CouchDbClient client;
    @Mock
    private ShipmentsWrapperRepositoryService shipmentsService;

    /* parameters */
    @Mock
    private Piece piece;
    private List<String> ids = new ArrayList<>();
    private static final String ID = "123";

    private static final String VALID_REF = "rev-111111";

    private static final String CONFLICT_REF = "rev-99999";

    @Test
    void findOne() throws IOException {
        // arrange
        when(client.read(ID, Piece.class)).thenReturn(piece);

        // act
        Piece returnedValue = piecesRepositoryService.findOne(ID);

        // assert
        assertThat(returnedValue).isEqualTo(piece);
    }

    @Test
    void findOne_error() throws IOException {
        // arrange
        when(client.read(ID, Piece.class)).thenThrow(IOException.class);

        // act + assert
        assertThatThrownBy(() -> piecesRepositoryService.findOne(ID))
                .isInstanceOf(CouchDbClientException.class);
    }

    @Test
    void findAllInIds() throws IOException {
        // arrange
        List<Piece> pieces = Arrays.asList(piece, piece);

        when(client.readAll(ids, Piece.class)).thenReturn(pieces);

        // act
        List<Piece> returnedList = piecesRepositoryService.findAllInIds(ids);

        // assert
        assertThat(returnedList).isEqualTo(pieces);
    }

    @Test
    void update() throws IOException {
        // arrange
        when(piece.getId()).thenReturn(ID);
        when(client.read(ID, Piece.class)).thenReturn(piece);
        when(piece.getRev()).thenReturn("132647246");
        when(client.save(piece)).thenReturn(piece);

        // act
        Piece returnedValue = piecesRepositoryService.update(piece);

        // assert
        assertThat(returnedValue).isEqualTo(piece);
    }

    @Test
    void updatePiece() throws IOException {
        // arrange
        Country country = new Country();
        Product product = new Product();

        PieceDetails pieceDetails = new PieceDetails();
        pieceDetails.setId(ID);
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        pieceDetails.setWeight(value);
        pieceDetails.setGoodType("Roses");
        pieceDetails.setHsCode("ABC");
        pieceDetails.setEppoCode("CDE");
        pieceDetails.setCountry("DEU");
        pieceDetails.setQuantity(value);
        pieceDetails.setPackaging("Palets");
        pieceDetails.setPackagingCode("1234");

        Item item = new Item();
        item.setProduct(product);
        item.setProductionCountry(country);
        Value value2 = new Value();
        value2.setUnit("kg");
        value2.setAmount(5f);
        item.setWeight(value2);
        List<Item> items = Arrays.asList(item, item);

        when(piece.getId()).thenReturn(ID);
        when(client.read(ID, Piece.class)).thenReturn(piece);
        when(piece.getContainedItems()).thenReturn(items);
        when(shipmentsService.updateAllShipmentsContainingPiece(ids, 90f)).thenReturn(true);
        when(piece.getRev()).thenReturn("132647246");
        when(client.save(piece)).thenReturn(piece);

        // act
        PieceDetails returnedValue = piecesRepositoryService.updatePiece(pieceDetails, ID, VALID_REF);

        // assert
        assertThat(returnedValue).isEqualTo(pieceDetails);
    }
    
    @Test
    void updatePiece_amount_null() throws IOException {
        // arrange
        Country country = new Country();
        Product product = new Product();

        PieceDetails pieceDetails = new PieceDetails();
        pieceDetails.setId(ID);
        Value value = new Value();
        pieceDetails.setWeight(value);
        pieceDetails.setGoodType("Roses");
        pieceDetails.setHsCode("ABC");
        pieceDetails.setEppoCode("CDE");
        pieceDetails.setCountry("DEU");
        pieceDetails.setPackaging("Palets");
        pieceDetails.setPackagingCode("1234");

        Item item = new Item();
        item.setProduct(product);
        item.setProductionCountry(country);
        Value value2 = new Value();
        item.setWeight(value2);
        List<Item> items = Arrays.asList(item, item);

        when(piece.getId()).thenReturn(ID);
        when(client.read(ID, Piece.class)).thenReturn(piece);
        when(piece.getContainedItems()).thenReturn(items);
        when(shipmentsService.updateAllShipmentsContainingPiece(ids, 0f)).thenReturn(true);
        when(piece.getRev()).thenReturn("132647246");
        when(client.save(piece)).thenReturn(piece);

        // act
        PieceDetails returnedValue = piecesRepositoryService.updatePiece(pieceDetails, ID, VALID_REF);

        // assert
        assertThat(returnedValue).isEqualTo(pieceDetails);
    }

    @Test
    void updatePiece_add() throws IOException {
        // arrange
        PieceDetails pieceDetails = new PieceDetails();
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        pieceDetails.setWeight(value);
        pieceDetails.setGoodType("Roses");
        pieceDetails.setHsCode("ABC");
        pieceDetails.setEppoCode("CDE");
        pieceDetails.setCountry("DEU");
        pieceDetails.setQuantity(value);
        pieceDetails.setPackaging("Palets");
        pieceDetails.setPackagingCode("1234");

        when(shipmentsService.addPieceToShipment(eq(pieceDetails), eq(ID), eq(VALID_REF), any())).thenReturn(true);
        // act
        PieceDetails returnedValue = piecesRepositoryService.updatePiece(pieceDetails, ID, VALID_REF);

        // assert
        assertThat(returnedValue).isEqualTo(pieceDetails);
    }

    @Test
    void updatePiece_add_conflict() throws IOException {
        // arrange
        PieceDetails pieceDetails = new PieceDetails();
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        pieceDetails.setWeight(value);
        pieceDetails.setGoodType("Roses");
        pieceDetails.setHsCode("ABC");
        pieceDetails.setEppoCode("CDE");
        pieceDetails.setCountry("DEU");
        pieceDetails.setQuantity(value);
        pieceDetails.setPackaging("Palets");
        pieceDetails.setPackagingCode("1234");

        when(shipmentsService.addPieceToShipment(eq(pieceDetails), eq(ID), any(), any())).thenReturn(false);
        // act
        PieceDetails returnedValue = piecesRepositoryService.updatePiece(pieceDetails, ID, CONFLICT_REF);

        // assert
        assertThat(returnedValue).isNull();
    }
}

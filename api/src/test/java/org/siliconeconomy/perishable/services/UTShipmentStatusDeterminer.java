// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.PieceDocumentValidationState;
import org.siliconeconomy.perishable.models.ShipmentDetailsValidationResult;
import org.siliconeconomy.perishable.models.ShipmentStatus;
import org.siliconeconomy.perishable.models.ValidationState;

@ExtendWith(MockitoExtension.class)
class UTShipmentStatusDeterminer {
	/* class under test */
	@InjectMocks
	private ShipmentStatusDeterminer statusDeterminer;

	/* test parameters */
	private ShipmentDetailsValidationResult result;
	private MetaInformation info;

	@BeforeEach
	void setUp() {
		result = new ShipmentDetailsValidationResult();
		result.setConsignee(ValidationState.VALID);
		result.setConsignor(ValidationState.VALID);
		result.setMawb(PieceDocumentValidationState.VALID);
		result.setPhytoCertificate(PieceDocumentValidationState.VALID);
		result.setPlaceOfDestination(ValidationState.VALID);

		info = new MetaInformation();
	}

	@Test
	void determineShipmentStatus_exported() {
		// arrange
		info.setExported(true);

		// act
		ShipmentStatus shipmentStatus = statusDeterminer.determineShipmentStatus(result, info);

		// assert
		assertThat(shipmentStatus).isEqualTo(ShipmentStatus.EXPORTED);
	}

	@Test
	void determineShipmentStatus_completed() {
		// arrange
		info.setExported(false);

		// act
		ShipmentStatus shipmentStatus = statusDeterminer.determineShipmentStatus(result, info);

		// assert
		assertThat(shipmentStatus).isEqualTo(ShipmentStatus.COMPLETED);
	}

	@Test
	void determineShipmentStatus_incomplete() {
		// arrange
		info.setExported(false);
		// at least one state is not valid
		result.setPlaceOfDestination(ValidationState.MISSING);

		// act
		ShipmentStatus shipmentStatus = statusDeterminer.determineShipmentStatus(result, info);

		// assert
		assertThat(shipmentStatus).isEqualTo(ShipmentStatus.INCOMPLETE);
	}

}

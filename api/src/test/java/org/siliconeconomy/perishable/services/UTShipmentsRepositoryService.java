// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.ShipmentWrapper;
import org.siliconeconomy.perishable.models.Address;
import org.siliconeconomy.perishable.models.BookingOption;
import org.siliconeconomy.perishable.models.BookingOptionRequest;
import org.siliconeconomy.perishable.models.Company;
import org.siliconeconomy.perishable.models.Party;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.Value;

import com.groocraft.couchdb.slacker.CouchDbClient;
import com.groocraft.couchdb.slacker.exception.CouchDbException;
import org.siliconeconomy.perishable.models.MetaInformation;
import org.siliconeconomy.perishable.models.Shipment;

@ExtendWith(MockitoExtension.class)
class UTShipmentsRepositoryService {

    /* class under test */
    @InjectMocks
    private ShipmentsWrapperRepositoryService shipmentsRepoService;

    /* dependencies */
    @Mock
    private ShipmentsWrapperRepositoryService shipmentsRepo;
    @Mock
    private CouchDbClient client;

    /* parameters */
    @Mock
    private List<ShipmentWrapper> mockedShipments;
    @Mock
    private Iterator<ShipmentWrapper> mockedIter;
    @Mock
    private ShipmentWrapper mockedShipment;
    @Mock
    private Address address;
    @Mock
    private ShipmentAgent agent;
    @Mock
    private Iterable<ShipmentWrapper> mockedShipmentIter;
    @Mock
    private PieceDetails pieceDetails;
    @Mock
    private ShipmentWrapper shipment;
    private List<String> ids = new ArrayList<String>();
    private static final String ID = "testId";
    private static final String VALID_REF = "rev1";
    private static final String CONFLICT_REF = "rev2";
    private static final String companyName = "Default Ltd.";
    private static final String role_carrier = "Carrier";
    private static final String role_consignee = "Consignee";

    @Test
    void findAll() throws IOException {
        // arrange
        when(client.readAll(ShipmentWrapper.class)).thenReturn(ids);
        when(client.readAll(ids, ShipmentWrapper.class)).thenReturn(mockedShipments);

        // act
        List<ShipmentWrapper> returnedList = shipmentsRepoService.findAll();

        // assert
        assertThat(returnedList).isEqualTo(mockedShipments);
    }

    @Test
    void findAll_error() throws IOException {
        // arrange
        when(client.readAll(ShipmentWrapper.class)).thenThrow(IOException.class);

        // act + assert
        assertThatThrownBy(() -> shipmentsRepoService.findAll())
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void findAllInIds() throws IOException {
        // arrange
        when(client.readAll(ids, ShipmentWrapper.class)).thenReturn(mockedShipments);

        // act
        List<ShipmentWrapper> returnedList = shipmentsRepoService.findAllInIds(ids);

        // assert
        assertThat(returnedList).isEqualTo(mockedShipments);
    }

    @Test
    void findOne() throws IOException {
        // arrange
        when(client.read(ID, ShipmentWrapper.class)).thenReturn(mockedShipment);

        // act
        ShipmentWrapper returnedValue = shipmentsRepoService.findOne(ID);

        // assert
        assertThat(returnedValue).isEqualTo(mockedShipment);
    }

    @Test
    void getCompanyByAgent() {
        // arrange
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);

        // act
        Company returnedValue = shipmentsRepoService.getCompanyByAgent(agent);

        // assert
        assertThat(returnedValue).isNotNull();
        assertThat(returnedValue.getCompanyBranch().getLocation().getAddress()).isEqualTo(address);
        assertThat(returnedValue.getCompanyName()).isEqualTo(companyName);
    }

    @Test
    void update() throws IOException {
        // arrange
        when(client.save(shipment)).thenReturn(shipment);

        // act
        ShipmentWrapper returnedValue = shipmentsRepoService.update(shipment);

        // assert
        assertThat(returnedValue).isEqualTo(shipment);
    }

    @Test
    void update_conflict() throws IOException {
        // arrange
        when(client.save(shipment)).thenThrow(new CouchDbException(409, "unknown", "http://invalid.de", "unknown"));

        // act
        ShipmentWrapper returnedValue = shipmentsRepoService.update(shipment);

        // assert
        assertThat(returnedValue).isNull();
    }

    @Test
    void updateShipmentAgent_consignor() throws IOException {
        // arrange
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn("Consignor");
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_exportAgent() throws IOException {
        // arrange
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn("ExportAgent");
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_placeOfDestination() throws IOException {
        // arrange
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn("PlaceofDestination");
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_consignee() throws IOException {
        // arrange
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setParties(new ArrayList<>());

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn(role_consignee);
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_existing_consignee() throws IOException {
        // arrange
        Company company = new Company();
        company.setCompanyName(companyName);

        Party consignee = new Party();
        consignee.setPartyRole(role_consignee);
        consignee.setPartyDetails(company);

        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setParties(Arrays.asList(consignee));

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn("Consignee");
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_conflict() throws IOException {
        // arrange
        Company company = new Company();
        company.setCompanyName(companyName);

        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRev()).thenReturn(CONFLICT_REF);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isNull();
    }

    @Test
    void updateShipmentAgent_carrier() throws IOException {
        // arrange
        BookingOptionRequest request = new BookingOptionRequest();
        request.setBookingOptions(new ArrayList<>());

        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setBookingOptionRequests(Arrays.asList(request));

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn(role_carrier);
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateShipmentAgent_existing_carrier() throws IOException {
        // arrange
        Company company = new Company();
        company.setCompanyName(companyName);

        BookingOption carrier = new BookingOption();
        carrier.setCompany(company);

        BookingOptionRequest request = new BookingOptionRequest();
        request.setBookingOptions(Arrays.asList(carrier));

        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setBookingOptionRequests(Arrays.asList(request));

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(agent.getRole()).thenReturn(role_carrier);
        when(agent.getAddress()).thenReturn(address);
        when(agent.getName()).thenReturn(companyName);
        when(agent.getRev()).thenReturn(VALID_REF);
        when(client.save(wrapper)).thenReturn(wrapper);

        // act
        ShipmentAgent returnedValue = shipmentsRepoService.updateShipmentAgent(ID, agent);

        // assert
        assertThat(returnedValue).isEqualTo(agent);
    }

    @Test
    void updateAll() throws IOException {
        // arrange
        ShipmentWrapper shipment = new ShipmentWrapper();
        shipment.setId(ID);
        List<ShipmentWrapper> shipments = Arrays.asList(shipment, shipment);

        when(client.saveAll(mockedShipments, ShipmentWrapper.class)).thenReturn(shipments);

        // act
        List<ShipmentWrapper> returnedValue = shipmentsRepoService.updateAll(mockedShipments);

        // assert
        assertThat(returnedValue).isEqualTo(shipments);
    }

    @Test
    void updateAllShipmentsContainingPiece() throws IOException {
        // arrange
        ids.add("123");
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        wrapper.getData().setTotalGrossWeight(value);
        List<ShipmentWrapper> shipments = Arrays.asList(wrapper, wrapper);

        when(client.readAll(ids, ShipmentWrapper.class)).thenReturn(shipments);
        when(client.saveAll(shipments, ShipmentWrapper.class)).thenReturn(shipments);

        // act
        Boolean returnedValue = shipmentsRepoService.updateAllShipmentsContainingPiece(ids, 10f);

        // assert
        assertThat(returnedValue).isTrue();
    }

    @Test
    void addPieceToShipment() throws IOException {
        // arrange
        ids.add("123");
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setPieces(ids);
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        wrapper.getData().setTotalGrossWeight(value);

        when(client.save(wrapper)).thenReturn(wrapper);
        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);
        when(pieceDetails.getId()).thenReturn(ID);

        // act
        boolean res = shipmentsRepoService.addPieceToShipment(pieceDetails, ID, VALID_REF, 10f);

        // assert
        assertThat(res).isTrue();  
        verify(client, times(1)).read(ID, ShipmentWrapper.class);
        verify(client).save(wrapper);
    }

    @Test
    void addPieceToShipment_conflict() throws IOException {
        // arrange
        ids.add("123");
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setData(new Shipment());
        wrapper.setId(ID);
        wrapper.setRev(VALID_REF);
        wrapper.getData().setPieces(ids);
        Value value = new Value();
        value.setAmount(100f);
        value.setUnit("kg");
        wrapper.getData().setTotalGrossWeight(value);

        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);

        // act
        boolean res = shipmentsRepoService.addPieceToShipment(pieceDetails, ID, CONFLICT_REF, 10f);

        // assert
        assertThat(res).isFalse();
    }
    
    @Test
    void getMetaInformation() throws IOException {
        // arrange
        ShipmentWrapper wrapper = new ShipmentWrapper();
        wrapper.setId(ID);
        MetaInformation info = new MetaInformation();
        wrapper.setMetainformation(info);
        when(client.read(ID, ShipmentWrapper.class)).thenReturn(wrapper);

        // act
        MetaInformation res=shipmentsRepoService.getMetaInformation(ID);
        
        // assert
        assertThat(res).isEqualTo(info);
    }
}

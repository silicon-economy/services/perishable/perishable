// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.exceptions.CouchDbClientException;

import com.groocraft.couchdb.slacker.CouchDbClient;
import com.groocraft.couchdb.slacker.exception.CouchDbException;

@ExtendWith(MockitoExtension.class)
class UTPieceDocumentsRepositoryService {

    /* class under test */
    @InjectMocks
    private PieceDocumentsRepositoryService pieceDocumentsRepositoryService;

    /* dependencies */
    @Mock
    private CouchDbClient client;

    /* parameters */
    @Mock
    private PieceDocument pieceDocument;

    @Test
    void findOne() throws IOException {
        // arrange
        when(client.read("123", PieceDocument.class)).thenReturn(pieceDocument);

        // act
        PieceDocument returnedValue = pieceDocumentsRepositoryService.findOne("123");

        // assert
        assertThat(returnedValue).isEqualTo(pieceDocument);
    }

    @Test
    void findOne_error() throws IOException {
        when(client.read("123", PieceDocument.class)).thenThrow(IOException.class);

        // assert
        assertThatThrownBy(() -> pieceDocumentsRepositoryService.findOne("123"))
        .isInstanceOf(CouchDbClientException.class);
    }

    @Test
    void update() throws IOException {
        // arrange
        when(client.save(pieceDocument)).thenReturn(pieceDocument);
        // act
        PieceDocument returnedValue = pieceDocumentsRepositoryService.update(pieceDocument);

        // assert
        assertThat(returnedValue).isEqualTo(pieceDocument);
    }

    @Test
    void update_wrong_revision() throws IOException {
        when(client.save(pieceDocument)).thenThrow(new CouchDbException(409,"","",""));

        // assert
        PieceDocument res=pieceDocumentsRepositoryService.update(pieceDocument);
        assertThat(res).isNull();
    }

    @Test
    void update_failure() throws IOException {
        when(client.save(pieceDocument)).thenThrow(new CouchDbException(500,"","",""));

        // assert
        assertThatThrownBy(() -> pieceDocumentsRepositoryService.update(pieceDocument))
        .isInstanceOf(CouchDbClientException.class);
    }

    @Test
    void update_error() throws IOException {
        when(client.save(pieceDocument)).thenThrow(IOException.class);

        // assert
        assertThatThrownBy(() -> pieceDocumentsRepositoryService.update(pieceDocument))
        .isInstanceOf(CouchDbClientException.class);
    }
}

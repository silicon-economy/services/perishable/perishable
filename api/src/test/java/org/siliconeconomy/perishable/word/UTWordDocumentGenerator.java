// Copyright 2021 Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//

package org.siliconeconomy.perishable.word;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.perishable.entities.PieceDocument;
import org.siliconeconomy.perishable.models.PieceDetails;
import org.siliconeconomy.perishable.models.PieceDocumentState;
import org.siliconeconomy.perishable.models.PieceDocumentType;
import org.siliconeconomy.perishable.models.ShipmentAgent;
import org.siliconeconomy.perishable.models.ShipmentDetails;
import org.siliconeconomy.perishable.models.Value;

/**
 *
 * @author bernd
 */
class UTWordDocumentGenerator {
  
  public UTWordDocumentGenerator() {
  }
  
  @BeforeAll
  public static void setUpClass() {
  }
  
  @AfterAll
  public static void tearDownClass() {
  }
  
  @BeforeEach
  public void setUp() {
  }
  
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of generateDocument method, of class WordDocumentGenerator.
   */
  @Test
  void testGenerateDocument() throws Exception {
    ShipmentDetails data=new ShipmentDetails();
    data.setId("123-494949_595959");
    Value weight=new Value();
    weight.setAmount(1201F);
    weight.setUnit("kg");
    data.setActualWeight(weight);
    data.setGoodType("Electronics");
    data.setOrg("DUS");
    data.setFlightNo("LH1149");
    data.setStd(new Date(1634608395730L));
    data.setEtd(new Date(1634618395730L));    
    data.setSta(new Date(1634628395730L));
    data.setEta(new Date(1634638395730L));
    
    List<PieceDetails> positions =new ArrayList<>();
    PieceDetails piece1=new PieceDetails();
    piece1.setCountry("US");
    piece1.setEppoCode("eppocode1");
    piece1.setGoodType("misc1");
    piece1.setGroup("group1");
    piece1.setHsCode("hscode1");
    piece1.setId("piece1");
    piece1.setPackaging("package1");
    piece1.setPackagingCode("pcode1");
    Value quant1=new Value();
    quant1.setAmount(10f);
    quant1.setUnit("u1");
    piece1.setQuantity(quant1);
    piece1.setUldId("uld1");
    Value w1=new Value();
    w1.setAmount(101f);
    w1.setUnit("kg");
    piece1.setWeight(w1);
    PieceDetails piece2=new PieceDetails();
    piece2.setCountry("CAN");
    piece2.setEppoCode("eppocode2");
    piece2.setGoodType("misc2");
    piece2.setGroup("group2");
    piece2.setHsCode("hscode2");
    piece2.setId("piece2");
    piece2.setPackaging("package2");
    piece2.setPackagingCode("pcode2");
    Value quant2=new Value();
    quant2.setAmount(12f);
    quant2.setUnit("u2");
    piece2.setQuantity(quant2);
    piece2.setUldId("uld2");
    Value w2=new Value();
    w2.setAmount(0.2f);
    w2.setUnit("t");
    piece2.setWeight(w2);    
    positions.add(piece1);
    positions.add(piece2);
    data.setPositions(positions);
    
    List<ShipmentAgent> agents=new ArrayList<>();
    ShipmentAgent consigner=new ShipmentAgent();
    consigner.setRole("Consignor");
    consigner.setName("abc");
    
    agents.add(consigner); 
    data.setShipmentAgents(agents);
    
    List<PieceDocument> docs=new ArrayList<>();
    PieceDocument doc=new PieceDocument();
    doc.setDocumentType(PieceDocumentType.OTHER);
    doc.setComment("no comment");
    doc.setId("doc1");
    doc.setState(PieceDocumentState.APPROVED);
    
    docs.add(doc);
    data.setDocuments(docs);
    
    System.out.println("generateDocument");
    
    WordDocumentGenerator instance = new WordDocumentGenerator();
    
    byte[] result = instance.generateDocument(data);
    ByteArrayInputStream is=new ByteArrayInputStream(result);
    XWPFDocument genDoc   = new XWPFDocument(OPCPackage.open(is));  
    assertEquals(1, genDoc.getTables().size());
    assertEquals(54,genDoc.getTables().get(0).getRows().size());
  }
    
}
  

